# BVATools -- Bam and Variant Analysis Tools

## Installation

Installation is managed by maven:

```bash
mvn clean install
```


## Documentation and help

Documentation for the BVATools suite is available in the [wiki](https://bitbucket.org/mugqic/bvatools/wiki/Home).

For bugs and ticket please send an email to bioinformatics.service@mail.mcgill.ca
