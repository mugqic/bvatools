#!/usr/bin/env perl

use Text::LevenshteinXS qw(distance);
use strict;

my $manifest = $ARGV[0];
my $fasta = $ARGV[1];

main();

sub main {

  my $rH_sequences = parseFasta($fasta);

  my $line;
  open(FILE, $manifest) or die "Can't open $manifest\n";
  while($line = <FILE>) {
    if($line =~ /^\[Assay\]/) {
      last;
    }
  }
  # read header
  $line = <FILE>;

  while($line = <FILE>) {
    my @values = split(',', $line);
    my $ilmnID = $values[0];
    my $name = $values[1];
    my $ilmnStrand = $values[2];
    my $snp = $values[3];
    my $addressA = $values[5];
    my $chr = $values[9];
    my $pos = $values[10];
    my $refStrand = substr($values[20],0,1); # safe against dos and linux \r\n

    $snp =~ s/\[//g;
    $snp =~ s/\]//g;
    if($chr eq '0' || $chr eq 'XY') {
      next;
    }

    if(length($addressA) == 0) {
      die "Too short: $ilmnID\n";
    }

    my $seq = $rH_sequences->{$chr};
    my $flank;
    my $ref = substr($seq, $pos-length($addressA), 1);
    if($refStrand eq '-') {
      $addressA = revcomp($addressA);
      $snp = revcomp($snp);
      $flank = substr($seq, $pos+1, length($addressA));
    }
    else {
      $flank = substr($seq, $pos-length($addressA)-1, length($addressA));
    }

    if($flank ne $addressA) {
      my $dist = distance($flank, $addressA);
      if($dist > 4) {
        print STDERR "Bad flank: $ilmnID\nProbe:  ".$addressA."\nFlank:  ".$flank."\nDistance: ".distance($flank, $addressA)."\n".$line."\n";
      }
    }

    print $chr.','.$pos.','.$name,','.$ref.','.substr($snp,0,1).','.substr($snp,2,1)."\n";
  }
  close(FILE);
}

sub parseFasta {
  my $file = shift;

  my %retVal;
  my $line;
  my $id = undef;
  my $seq;
  open(FILE, $file) or die "Can't open $file\n";
  while($line = <FILE>) {
    chomp($line);
    if($line =~ /^>([^\s]+)\s.*/) {
      if(defined($id)) {
        $retVal{$id} = $seq;
        $id = undef;
        $seq = undef;
      }
      $id = $1;
    }
    elsif(defined($id)) {
      $seq .= $line;
    }
  }
  if(defined($id)) {
    $retVal{$id} = $seq;
  }
  close(FILE);

  return \%retVal;
}

sub revcomp {
  my $dna = shift;

  # reverse the DNA sequence
  my $revcomp = reverse($dna);

  # complement the reversed DNA sequence
  $revcomp =~ tr/ACGTacgt/TGCAtgca/;
  return $revcomp;
}

