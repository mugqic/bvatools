/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package htsjdk.samtools;

/**
 * Hack to gain access to package protected BAMFileSpan
 * 
 * @author lletourn
 *
 */
public class BAMFileSpanUtils {
  /**
   * Make a BAMFileSpan that spans the position or the closest read following the position up to the end of the BAM
   * 
   * @param reader
   * @param chrIndex
   * @param position
   * @return
   */
  public static BAMFileSpan makeSpanFollowing(SamReader reader, int chrIndex, int position) {
    BrowseableBAMIndex bamIndex = reader.indexing().getBrowseableIndex();
    BAMFileSpan fileSpan = bamIndex.getSpanOverlapping(chrIndex, position, Integer.MAX_VALUE);
    
    if(fileSpan == null) {
      for(int idx=chrIndex+1; idx < reader.getFileHeader().getSequenceDictionary().size(); idx++) {
        fileSpan = bamIndex.getSpanOverlapping(idx, 1, Integer.MAX_VALUE);
        if(fileSpan != null) {
          break;
        }
      }
    }

    if(fileSpan != null) {
      return new BAMFileSpan(new Chunk(fileSpan.getChunks().get(0).getChunkStart(),Long.MAX_VALUE));
    }
    return null;
  }
}
