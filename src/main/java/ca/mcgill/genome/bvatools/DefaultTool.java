/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools;

import java.util.Deque;

public abstract class DefaultTool {
  public abstract String getCmdName();

  public String getCmdUsage() {
    return "";
  }

  public abstract int parseArgs(Deque<String> arguments, Deque<String> unusedArgs);

  public abstract int run();

  public abstract void printUsage(String errMsg);

  protected final void printUsageHeader() {
    System.out.println("Usage: BVATools " + getCmdName());
  }
}
