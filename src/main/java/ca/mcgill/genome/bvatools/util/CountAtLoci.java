/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.util.CoordMath;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class CountAtLoci {
  private final SAMRecordIterator recIter;
  private final int minMappingQuality;
  private final int minBaseQuality;
  private final boolean countDuplicates;
  private SAMSequenceDictionary dictionary;
  private Queue<SAMRecord> stackedRecords = new LinkedList<SAMRecord>();
  
  public CountAtLoci(SamReader reader, int minMappingQuality, int minBaseQuality, boolean countDuplicates) {
    this.minMappingQuality = minMappingQuality;
    this.minBaseQuality = minBaseQuality;
    this.countDuplicates = countDuplicates;
    recIter = reader.iterator();
    dictionary = reader.getFileHeader().getSequenceDictionary();
  }

  public int[] count(String chr, long pos, String alleles[]) {
    int retVal[] = new int [alleles.length];

    cleanStack(chr, pos);
    SAMSequenceRecord chrSequence = dictionary.getSequence(chr);
    int seqIdx = chrSequence.getSequenceIndex();
    for (SAMRecord record : stackedRecords) {
      if (seqIdx != record.getReferenceIndex() || record.getAlignmentEnd() < pos) {
        continue;
      }
      processRecord(record, chr, pos, alleles, retVal);
    }
    
    while (recIter.hasNext()) {
      SAMRecord record = recIter.next();
      if (record.getReadUnmappedFlag() || record.getMappingQuality() < minMappingQuality)
        continue;
      if(!countDuplicates && record.getDuplicateReadFlag())
        continue;

      if (record.getReferenceIndex() < seqIdx) {
        continue;
      } else if (record.getReferenceIndex() > seqIdx || (record.getReferenceIndex() == seqIdx && record.getAlignmentStart() > pos)) {
        stackedRecords.add(record);
        break;
      }

      if (record.getAlignmentEnd() < pos) {
        continue;
      }

      stackedRecords.add(record);

      processRecord(record, chr, pos, alleles, retVal);
    }
    return retVal;
  }

  private void processRecord(SAMRecord record, String chr, long pos, String alleles[], int counts[]) {
    for (AlignmentBlock block : record.getAlignmentBlocks()) {
      if (block.getReferenceStart() <= pos && pos <= CoordMath.getEnd(block.getReferenceStart(), block.getLength())) {
        int refOffset = (int)pos - block.getReferenceStart();
        int readBaseOffset = block.getReadStart() - 1 + refOffset;
        if (record.getBaseQualities()[readBaseOffset] >= minBaseQuality) {
          byte bases[] = record.getReadBases();
          String baseStr = String.valueOf((char) bases[readBaseOffset]);
          
          for(int i=0; i < alleles.length; i++) {
            if(alleles[i].equals(baseStr)) {
              counts[i]++;
            }
          }
        }
        break;
      }
    }
  }
  
  private void cleanStack(String chr, long pos) {
    Iterator<SAMRecord> iter = stackedRecords.iterator();
    SAMSequenceRecord seqRec = dictionary.getSequence(chr);
    if (stackedRecords.size() > 0) {
      if (seqRec.getSequenceIndex() > stackedRecords.peek().getReferenceIndex()) {
        stackedRecords.clear();
      } else {
        while (iter.hasNext()) {
          SAMRecord record = iter.next();

          if (record.getAlignmentEnd() >= pos) {
            break;
          } else {
            iter.remove();
          }
        }
      }
    }
  }
}
