/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

public class Bin {
  private final String chromosome;
  private final int start;
  private final int stop;
  private int count = 0;

  public Bin(String chromosome, int start, int stop) {
    super();
    this.chromosome = chromosome;
    this.start = start;
    this.stop = stop;
  }

  public void add() {
    count++;
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getStart() {
    return start;
  }

  public int getStop() {
    return stop;
  }

  public int getCount() {
    return count;
  }

}
