/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;

public class StrandBamFile {

  final private String strand;
  final private SamReader samReader;
  final private String order;
  final private String fileName;

  public StrandBamFile(String strand, File fileReader, String order) {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    this.samReader = samReaderFactory.open(fileReader);
    this.strand = strand;
    this.order = order;
    this.fileName = fileReader.toString();
  }

  public String getStrand() {
    return this.strand;
  }

  public SamReader getFileReader() {
    return this.samReader;
  }

  public String getOrder() {
    return this.order;
  }

  public String getFileName() {
    return this.fileName;
  }
}
