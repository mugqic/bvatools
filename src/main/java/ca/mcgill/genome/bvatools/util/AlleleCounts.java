/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.util;

import gnu.trove.impl.Constants;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

import ca.mcgill.mcb.pcingola.util.Gpr;

public class AlleleCounts {
  private final static DecimalFormat doubleFormatter = new DecimalFormat("#0.00");

  private final String chromosome;
  private final int position;
  // Official alleles
  private String alleleA;
  private String alleleB;
  private TObjectIntHashMap<String> bamCounts;
  private TObjectIntHashMap<String> referenceBAMCounts;

  public AlleleCounts(String chromosome, int position) {
    super();
    this.chromosome = chromosome;
    this.position = position;
  }

  public int getRefDepth() {
    int depth = 0;
    for (String base : referenceBAMCounts.keySet()) {
      depth += referenceBAMCounts.get(base);
    }
    return depth;
  }

  public Set<String> getBasesAboveFraction(double fraction) {
    TObjectDoubleHashMap<String> alleleFractions = AlleleCounts.computeAlleleFractions(getBamCounts());
    Set<String> keptBases = new HashSet<String>();
    for (String base : alleleFractions.keySet()) {
      if (alleleFractions.get(base) > fraction) {
        keptBases.add(base);
      }
    }
    return keptBases;
  }

  public int getDepth() {
    int depth = 0;
    for (String base : bamCounts.keySet()) {
      depth += bamCounts.get(base);
    }
    return depth;
  }

  public StringBuilder format(boolean formatRef) {
    if (formatRef) {
      return formatCounts(referenceBAMCounts);
    } else {
      return formatCounts(bamCounts);
    }
  }

  public String getAlleleA() {
    return alleleA;
  }

  public void setAlleleA(String alleleA) {
    this.alleleA = alleleA;
  }

  public String getAlleleB() {
    return alleleB;
  }

  public void setAlleleB(String alleleB) {
    this.alleleB = alleleB;
  }

  private StringBuilder formatCounts(TObjectIntHashMap<String> counts) {
    StringBuilder sb = new StringBuilder();
    boolean first = true;
    for (String seq : counts.keySet()) {
      if (first) {
        first = false;
      } else {
        sb.append(' ');
      }
      sb.append(seq);
      sb.append(':');
      sb.append(counts.get(seq));
    }
    return sb;
  }

  public static StringBuilder formatFractionCounts(TObjectDoubleHashMap<String> baseCounts) {
    StringBuilder sb = new StringBuilder();
    boolean first = true;
    for (String base : baseCounts.keySet()) {
      if (first) {
        first = false;
      } else {
        sb.append(' ');
      }
      sb.append(base.toString());
      sb.append(':');
      sb.append(doubleFormatter.format(baseCounts.get(base)));
    }
    return sb;
  }

  public static TObjectDoubleHashMap<String> computeAlleleFractions(TObjectIntHashMap<String> baseCounts) {
    TObjectDoubleHashMap<String> retVal = new TObjectDoubleHashMap<String>();

    double totalBaseCount = 0;
    for (String base : baseCounts.keySet()) {
      totalBaseCount += baseCounts.get(base);
    }

    for (String base : baseCounts.keySet()) {
      if (baseCounts.get(base) == 0) {
        retVal.put(base, 0);
      } else {
        retVal.put(base, baseCounts.get(base) / totalBaseCount);
      }
    }
    return retVal;
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getPosition() {
    return position;
  }

  public TObjectIntHashMap<String> getBamCounts() {
    return bamCounts;
  }

  public void setBamCounts(TObjectIntHashMap<String> bamCounts) {
    this.bamCounts = bamCounts;
  }

  public TObjectIntHashMap<String> getReferenceBAMCounts() {
    return referenceBAMCounts;
  }

  public void setReferenceBAMCounts(TObjectIntHashMap<String> referenceBAMCounts) {
    this.referenceBAMCounts = referenceBAMCounts;
  }

  public static AlleleCounts valueOf(String chr, int pos, String alleles) {
    AlleleCounts retVal = new AlleleCounts(chr, pos);
    retVal.setBamCounts(new TObjectIntHashMap<String>(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0));
    String values[] = Gpr.split(alleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String seq = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      retVal.getBamCounts().put(seq, Integer.valueOf(cntStr));
    }
    return retVal;
  }

  public static AlleleCounts valueOf(String chr, int pos, String refAlleles, String alleles) {
    AlleleCounts retVal = new AlleleCounts(chr, pos);
    retVal.setBamCounts(new TObjectIntHashMap<String>(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0));
    retVal.setReferenceBAMCounts(new TObjectIntHashMap<String>(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0));

    String values[] = Gpr.split(alleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String seq = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      retVal.getBamCounts().put(seq, Integer.valueOf(cntStr));
    }

    values = Gpr.split(refAlleles, ' ');
    for (String value : values) {
      int idx = value.indexOf(':');
      String seq = value.substring(0, idx);
      String cntStr = value.substring(idx + 1);

      retVal.getReferenceBAMCounts().put(seq, Integer.valueOf(cntStr));
    }
    return retVal;
  }
}
