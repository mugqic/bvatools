/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.filterdups;

import gnu.trove.map.hash.TObjectIntHashMap;

import org.obiba.bitwise.BitVector;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

public class DuplicateKmerBuilder {
  public static int DEFAULT_OFFSET = 15;
  public static int DEFAULT_KMER_SIZE = 20;
  
  private int estimatedMaxItems = 200000000;
  private static final int DUPLICATE_OFFSET = 1024;
  private transient TObjectIntHashMap<BitVector> nbKnownSequencesFound;
  private int kmerSize = DEFAULT_KMER_SIZE;
  private int offset = DEFAULT_OFFSET;
  private long totalNbReads = 0;

  public boolean longEnoughReads(Fragment fragment) {
    int kmerBasesPerRead = kmerSize / fragment.size();
    // All reads need to be > than kmer + offset
    for(int idx=0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if(read != null) {
        if(read.getBaseQualities().length < (kmerBasesPerRead+offset)) {
          return false;
        }
      }
    }
    return true;
  }

  public void processSequence(Fragment fragment) {
    if(!longEnoughReads(fragment)) {
      return;
    }
    
    if (nbKnownSequencesFound == null)
      nbKnownSequencesFound = new TObjectIntHashMap<BitVector>((int) (estimatedMaxItems / 0.75 + 1.0), 0.75f);

    totalNbReads++;
    BitVector twoBitSequence;
    try {
      twoBitSequence = computeKmer(offset, kmerSize, fragment);
    } catch (SequenceLengthException e) {
      throw new RuntimeException("Kmer too long, or offset to big: " + fragment.getName());
    }
    // Keep this for debugging purposes
    // if(pair1.toString().equals(pair2.toString())) {
    // System.err.println("They are equal: " + sequence.getHeader());
    // }
    if (!nbKnownSequencesFound.containsKey(twoBitSequence)) {
      nbKnownSequencesFound.put(twoBitSequence, fragment.getAvgQuality());
    } else {
      int value = nbKnownSequencesFound.get(twoBitSequence);
      if (value >= DUPLICATE_OFFSET) {
        value = value - DUPLICATE_OFFSET;
      }

      if (value < fragment.getAvgQuality()) {
        nbKnownSequencesFound.put(twoBitSequence, DUPLICATE_OFFSET + fragment.getAvgQuality());
      }
    }
  }

  public static BitVector computeKmer(int offset, int kmerSize, Fragment fragment) throws SequenceLengthException {
    int bitIdx = 0;
    BitVector twoBitSequence = new BitVector(kmerSize * 3);
    int kmerSizePerRead = kmerSize / fragment.size();
    
    for(int idx=0; idx < fragment.size(); idx++) {
      bitIdx = idx*kmerSizePerRead*3;
      Read read = fragment.getRead(idx);
      if(read == null) {
        continue;
      }
      String sequence = read.getSequence();
      for (int kmerIdx = offset; kmerIdx < kmerSizePerRead+offset; kmerIdx++) {
        char nucleotide = sequence.charAt(kmerIdx);
        switch (nucleotide) {
        case 'A':
          // 000
          bitIdx += 3;
          break;
        case 'C':
          // 001
          bitIdx += 2;
          twoBitSequence.set(bitIdx);
          bitIdx++;
          break;
        case 'G':
          // 010
          bitIdx++;
          twoBitSequence.set(bitIdx);
          bitIdx += 2;
          break;
        case 'T':
          // 011
          bitIdx++;
          twoBitSequence.set(bitIdx);
          bitIdx++;
          twoBitSequence.set(bitIdx);
          bitIdx++;
          break;
        case 'N':
        case '-':
        case '*':
          // 100
          twoBitSequence.set(bitIdx);
          bitIdx += 3;
          break;
        default:
          throw new RuntimeException("Unknown base: " + nucleotide);
        }
      }
    }

    return twoBitSequence;
  }

  public boolean getKmerCountAndMark(Fragment fragment) throws SequenceLengthException {
    BitVector twoBitSequence = computeKmer(offset, kmerSize, fragment);
    int value = nbKnownSequencesFound.get(twoBitSequence);
    if (value - DUPLICATE_OFFSET <= fragment.getAvgQuality()) {
      nbKnownSequencesFound.put(twoBitSequence, Integer.MAX_VALUE);
      return true;
    }

    return false;
  }

  public TObjectIntHashMap<BitVector> getNbKnownSequencesFound() {
    return nbKnownSequencesFound;
  }

  public long getTotalNbReads() {
    return totalNbReads;
  }

  public void setTotalNbReads(long totalNbReads) {
    this.totalNbReads = totalNbReads;
  }

  public int getEstimatedMaxItems() {
    return estimatedMaxItems;
  }

  public void setEstimatedMaxItems(int estimatedMaxItems) {
    this.estimatedMaxItems = estimatedMaxItems;
  }

  public int getKmerSize() {
    return kmerSize;
  }

  public void setKmerSize(int kmerSize) {
    this.kmerSize = kmerSize;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  public static class KmerCount {
    private int count;
    private byte maxQuality;

    public int getCount() {
      return count;
    }

    public void setCount(int count) {
      this.count = count;
    }

    public byte getMaxQuality() {
      return maxQuality;
    }

    public void setMaxQuality(byte maxQuality) {
      this.maxQuality = maxQuality;
    }
  }
}
