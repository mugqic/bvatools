/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.content;

import htsjdk.samtools.util.IOUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.util.Gpr;

/**
 * 
 * References: http://mblab.wustl.edu/GTF22.html
 *             http://www.ensembl.org/info/website/upload/gff.html
 * 
 */
public class EnsemblGTFParser {

  private static final String ATTRIBUTE_PATTERN_REGEX = "\\s*(\\S+)\\s+\"(.*?)\"\\s*;";
  private static final Pattern ATTRIBUTE_PATTERN = Pattern.compile(ATTRIBUTE_PATTERN_REGEX);
  private final Map<String, Map<String, CountingTranscript>> transcriptsById;
  private final Map<String, Map<String, Markers>> type2marker;
  private final Genome genome;

  public EnsemblGTFParser() {
    genome = new Genome();
    transcriptsById = new HashMap<String, Map<String, CountingTranscript>>();
    type2marker = new HashMap<String, Map<String, Markers>>();
  }
  
  public Map<String, Map<String, CountingTranscript>> getTranscriptsById() {
    return transcriptsById;
  }

  public Map<String, Map<String, Markers>> getType2marker() {
    return type2marker;
  }

  public Genome getGenome() {
    return genome;
  }

  public void parse(File gtf) {
    BufferedReader reader = null;
    
    try {
      reader = IOUtil.openFileForBufferedReading(gtf);
      
      while (true) {
        String line = reader.readLine();
        if (line == null)
          break;
        parse(line);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  /**
   * Read and parse GTF file
   */
  protected void parse(String line) {
    String fields[] = line.split("\t");

    // Omit headers
    if (fields.length <= 6)
      return;

    String type = fields[2];

    // Parse fields
    String chromo = fields[0];
    String source = fields[1];
    int start = Gpr.parseIntSafe(fields[3]) - 1;
    int end = Gpr.parseIntSafe(fields[4]) - 1;
    boolean strand = (fields[6].equals("-") ? true : false);
    // int frame = (fields[7].equals(".") ? -1 :
    // Gpr.parseIntSafe(fields[7]));
    if(!type2marker.containsKey(chromo)) {
      type2marker.put(chromo, new HashMap<String, Markers>());
      transcriptsById.put(chromo, new HashMap<String, CountingTranscript>());
    }
    
    Markers typeMarkers = type2marker.get(chromo).get(source);
    if(typeMarkers == null) {
      typeMarkers = new Markers(source);
      type2marker.get(chromo).put(source, typeMarkers);
    }
    Chromosome chr = genome.getOrCreateChromosome(chromo);
    Marker marker = new Marker(chr, start, end, strand, source);
    typeMarkers.add(marker);

    String geneId = "", transcriptId = "";
    String geneName = null;
    
    // Is it protein coding?
    boolean proteinCoding = source.equals("protein_coding");
    if(proteinCoding) {
      //String geneBioType = "", trBioType = "";
  
      // Parse attributes
      if (fields.length >= 8) {
        HashMap<String, String> attrMap = parseAttributes(fields[8]);

        // Get gene and transcript ID
        geneId = attrMap.get("gene_id");
        transcriptId = attrMap.get("transcript_id");
        geneName = attrMap.get("gene_name");
        
        if(type.equals("transcript")) {
          CountingTranscript transcript = transcriptsById.get(chromo).get(transcriptId);
          if(transcript == null) {
            transcript = new CountingTranscript(chr, geneId, geneName, start, end, strand, transcriptId);
          }
        }
        else if(type.equals("exon")) {
          CountingTranscript transcript = transcriptsById.get(chromo).get(transcriptId);
          if(transcript != null) {
            String exonId = attrMap.get("exon_id");
            CountingExon exon = new CountingExon(transcript, start, end, strand, exonId);
            transcript.addExon(exon);
          }
          else {
            throw new RuntimeException("Transcript for exon isn't created: " + transcriptId);
          }
        }
  
      // geneBioType = attrMap.get("gene_biotype"); // Note: This is ENSEMBL
      // // specific
      // if (geneBioType == null)
      // geneBioType = attrMap.get("gene_type"); // Note: This is GENCODE
      // // specific
      //
      // trBioType = attrMap.get("transcript_type"); // Note: This is GENCODE
      // // specific
      }

      // // Use 'source' as bioType (ENSEMBL uses this field)
      // if ((trBioType == null) || trBioType.isEmpty())
      // trBioType = source;
      //
      // // Transform null to empty
      // if (geneId == null)
      // geneId = "";
      // if (transcriptId == null)
      // transcriptId = "";
      //
      // String id = type + "_" + chromo + "_" + (start + 1) + "_" + (end +
      // 1); // Create
      // // ID
      // if (geneId.isEmpty())
      // System.err
      // .println("WARNING: Empty gene_id. This should never happen");
      // else
      // addInterval(id, type, chromo, start, end, strand, geneId, geneName,
      // transcriptId, proteinCoding, geneBioType, trBioType, frame); // Add
      // // interval
    }
  }

  /**
   * Parse attributes
   * 
   * @param attrs
   * @return
   */
  HashMap<String, String> parseAttributes(String attrStr) {
    HashMap<String, String> keyValues = new HashMap<String, String>();

    if (attrStr.length() > 0) {
      Matcher matcher = ATTRIBUTE_PATTERN.matcher(attrStr);
      while (matcher.find()) {
        if (matcher.groupCount() >= 2) {
          String key = matcher.group(1).toLowerCase();
          String value = matcher.group(2);
          keyValues.put(key, value);
        }
      }
    }

    return keyValues;
  }
}
