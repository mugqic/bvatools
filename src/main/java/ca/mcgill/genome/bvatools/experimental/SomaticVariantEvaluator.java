/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.experimental;

import htsjdk.samtools.util.IOUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;

public class SomaticVariantEvaluator {
  private final String adKey;
  private final File vcf;
  private final int minCLR;
  private final String outPrefix;
  private final int normalIdx;
  private final int tumorIdx;
  
  public SomaticVariantEvaluator(String adKey, File vcf, String outPrefix, int minDP, float minQual, int minCLR, boolean switchNormTum) {
    this.adKey = adKey;
    this.vcf = vcf;
    this.minCLR = minCLR;
    this.outPrefix = outPrefix;
    if(switchNormTum) {
      normalIdx = 1;
      tumorIdx = 0;
    } else {
      normalIdx = 0;
      tumorIdx = 1;
    }
    
  }
  
  public void run() {
    VcfFileIterator vcfParser = new VcfFileIterator(IOUtil.openFileForBufferedReading(vcf));
    PrintWriter allWriter;
    PrintWriter overCLRWriter;
    PrintWriter byCLRWriter;
    PrintWriter byCallWriter;
    try {
      allWriter = new PrintWriter(outPrefix + ".all.csv");
      overCLRWriter = new PrintWriter(outPrefix + ".overCLR.csv");
      byCLRWriter = new PrintWriter(outPrefix + ".byCLR.csv");
      byCallWriter = new PrintWriter(outPrefix + ".byCall.csv");
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }

    try {
      printHeader(allWriter);
      printHeader(overCLRWriter);
      printHeader(byCLRWriter);
      printHeader(byCallWriter);
      int nbLOH=0;
      for (VcfEntry vcfEntry : vcfParser) {
        if(!vcfEntry.isSnp()) {
          continue;
        }
        
        printEntry(allWriter, vcfEntry);

        int clr = (int)vcfEntry.getInfoInt("CLR");
        if(clr > minCLR) {
          if(vcfEntry.getVcfGenotype(normalIdx).getGenotypeCode() != vcfEntry.getVcfGenotype(tumorIdx).getGenotypeCode()) {
            printEntry(overCLRWriter, vcfEntry);
          }
        }
        if(clr < minCLR) {
          if(vcfEntry.getVcfGenotype(normalIdx).getGenotypeCode() != vcfEntry.getVcfGenotype(tumorIdx).getGenotypeCode()) {
            printEntry(byCLRWriter, vcfEntry);
          }
          
          if(vcfEntry.getVcfGenotype(normalIdx).isVariant() && !vcfEntry.getVcfGenotype(tumorIdx).isVariant()) {
            nbLOH++;
          }
        }

        if(vcfEntry.getVcfGenotype(normalIdx).getGenotypeCode() != vcfEntry.getVcfGenotype(tumorIdx).getGenotypeCode()
            && vcfEntry.getVcfGenotype(tumorIdx).getGenotypeCode() != 0) {
          printEntry(byCallWriter, vcfEntry);
        }
      }
      byCLRWriter.println("Nb REF LOH: " + nbLOH);
    }
    finally {
      overCLRWriter.close();
      byCLRWriter.close();
      byCallWriter.close();
    }
  }
  
  private void printHeader(PrintWriter writer) {
    writer.println("chr,pos,key,ref,nrd,nad,trd,tad,NormDP,TumDP,clr,qual,NormGT,TumGT,NormRatio,TumRatio,SampleRatio,logSampleRatio,NormCall,TumCall");
  }
  
  private void printEntry(PrintWriter writer, VcfEntry vcfEntry) {
    int clr = (int)vcfEntry.getInfoInt("CLR");
    String normDepthField[] = vcfEntry.getVcfGenotype(normalIdx).get(adKey).split(","); 
    int normRefDP = Gpr.parseIntSafe(normDepthField[0]);
    int normAltDP = Gpr.parseIntSafe(normDepthField[1]);
    int normDP = normRefDP+normAltDP;
    String tumDepthField[] = vcfEntry.getVcfGenotype(tumorIdx).get(adKey).split(","); 
    int tumRefDP = Gpr.parseIntSafe(tumDepthField[0]);
    int tumAltDP = Gpr.parseIntSafe(tumDepthField[1]);
    int tumDP = tumRefDP+tumAltDP;
    double ratio = ((double)normAltDP/(double)normDP) / ((double)tumAltDP/(double)tumDP);

    writer.print(vcfEntry.getChromosomeName());
    writer.print(',');
    writer.print(vcfEntry.getStart() + 1);
    writer.print(',');
    writer.print(vcfEntry.getChromosomeName()+'_'+(vcfEntry.getStart() + 1));
    writer.print(',');
    writer.print(vcfEntry.getRef());
    writer.print(',');
    writer.print(normRefDP);
    writer.print(',');
    writer.print(normAltDP);
    writer.print(',');
    writer.print(tumRefDP);
    writer.print(',');
    writer.print(tumAltDP);
    writer.print(',');
    writer.print(normDP);
    writer.print(',');
    writer.print(tumDP);
    writer.print(',');
    writer.print(clr);
    writer.print(',');
    writer.print(vcfEntry.getQuality());
    writer.print(',');
    writer.print(vcfEntry.getVcfGenotype(normalIdx).getGenotypeStr());
    writer.print(',');
    writer.print(vcfEntry.getVcfGenotype(tumorIdx).getGenotypeStr());
    writer.print(',');
    writer.print((double)normAltDP/(double)normDP);
    writer.print(',');
    writer.print((double)tumAltDP/(double)tumDP);
    writer.print(',');
    writer.print(ratio);
    writer.print(',');
    writer.print(Math.log(ratio < 0.0000001 ? 0.0000001 : ratio)/Math.log(2));
    writer.print(',');
    writer.print(genotypeCode2string(vcfEntry.getVcfGenotype(normalIdx).getGenotypeCode()));
    writer.print(',');
    writer.print(genotypeCode2string(vcfEntry.getVcfGenotype(tumorIdx).getGenotypeCode()));
    writer.println();
  }

  private String genotypeCode2string(int code) {
    switch(code) {
    case 0:
      return "HOM_REF";
    case 1:
      return "HET";
    case 2:
      return "HOM_ALT";
    default:
      return "UNKNOWN";
    }
  }

  public static void main(String[] args) {
    String key = args[0];
    File vcf = new File(args[1]);
    String outPrefix = args[2];
    int minDP = Integer.parseInt(args[3]);
    float minQual = Float.parseFloat(args[4]);
    int minCLR = Integer.parseInt(args[5]);
    boolean switchNormTum = false;
    if(args.length > 6)
      switchNormTum = Boolean.parseBoolean(args[6]);
    SomaticVariantEvaluator sve = new SomaticVariantEvaluator(key, vcf, outPrefix, minDP, minQual, minCLR, switchNormTum);
    sve.run();
  }
}
