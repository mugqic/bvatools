/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Interval;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.interval.tree.IntervalForest;
import ca.mcgill.mcb.pcingola.util.Gpr;

public class SmallestInterval extends DefaultTool {
  private List<File> beds = new ArrayList<File>();
  private List<String> sampleNames = new ArrayList<String>();
  private File geneBed = null;

  @Override
  public String getCmdName() {
    return "smallestinterval";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bed   <SAMPLE> <FILE> Sample name and Bed file to parse. Can be given multiple times");
    System.out.println("\t--genes <FILE           Genes to check intervals on.");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bed")) {
        sampleNames.add(arguments.pop());
        beds.add(new File(arguments.pop()));
      } else if (arg.equals("--genes")) {
        geneBed = new File(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (beds.size() == 0) {
      printUsage("You need to pass at least one bed");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {

    try {
      findIntervals();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public void findIntervals() throws IOException, UnsupportedEncodingException {
    Map<String, List<Interval>> chrIntervals = new LinkedHashMap<String, List<Interval>>();
    for (int idx = 0; idx < beds.size(); idx++) {
      parseIntervals(sampleNames.get(idx), beds.get(idx), chrIntervals);
    }

    Map<String, List<Interval>> chrSmallIntervals = new HashMap<String, List<Interval>>();
    for (String chr : chrIntervals.keySet()) {
      List<Interval> intervals = chrIntervals.get(chr);
      Collections.sort(intervals);
      chrSmallIntervals.put(chr, extractSmallestIntervals(intervals));
    }

    IntervalForest forest = parseGenes(geneBed);
    System.out.println("Chr,Start,End,Genes,NbSamples,Samples");
    for (String chr : chrIntervals.keySet()) {
      for (Interval interval : chrSmallIntervals.get(chr)) {
        System.out.print(chr);
        System.out.print(',');
        System.out.print(interval.getStart() + 1);
        System.out.print(',');
        System.out.print(interval.getEnd() + 1);
        System.out.print(',');
        Markers hits = forest.getTree(chr).query(interval);
        Set<String> foundGene = new HashSet<String>();
        if (hits.size() > 0) {
          StringBuilder sb = new StringBuilder();
          for (Marker gene : hits) {
            if (!foundGene.contains(gene.getId())) {
              sb.append(' ').append(gene.getId());
              foundGene.add(gene.getId());
            }
          }
          sb.deleteCharAt(0);
          System.out.print(sb.toString());
        } else {
          System.out.print("None");
        }
        System.out.print(',');
        System.out.print(interval.getId().split(",").length);
        System.out.print(',');
        System.out.println(interval.getId().replace(',', ' '));
      }
    }
  }

  public IntervalForest parseGenes(File geneBed) throws IOException {
    IntervalForest forest = new IntervalForest();
    Genome genome = new Genome();
    LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(geneBed), "ASCII"));
    while (true) {
      String line = reader.readLine();
      if (line == null)
        break;

      // Ignore empty lines and comment lines
      if ((line.length() > 0) && (!line.startsWith("#"))) {
        // Parse line
        String fields[] = line.split("\t");
        String chromosome = fields[0].trim();
        int start = Gpr.parseIntSafe(fields[1]) - 2000;
        if (start < 0)
          start = 0;
        int end = Gpr.parseIntSafe(fields[2]) - 1;
        end += 2000;
        String id = fields[3];
        Marker newRegion = new Marker(genome.getOrCreateChromosome(chromosome), start, end, false, id);
        forest.add(newRegion);
      }
    }
    reader.close();

    return forest;
  }

  private List<Interval> extractSmallestIntervals(List<Interval> intervals) {
    List<Interval> retVal = new ArrayList<Interval>();

    Interval current = null;
    ArrayList<Interval> buffer = new ArrayList<Interval>();
    while (intervals.size() > 0) {
      if (current == null) {
        Interval next = intervals.remove(0);
        buffer.add(next);
        current = new Interval(next.getParent(), next.getStart(), next.getEnd(), false, "");
      } else {
        Interval next = intervals.remove(0);
        if (next.intersects(current)) {
          if (current.getStart() < next.getStart()) {
            current.setStart(next.getStart());
          }
          if (current.getEnd() > next.getEnd()) {
            current.setEnd(next.getEnd());
          }
        } else {
          StringBuilder sb = new StringBuilder();
          Interval nextCurrent = new Interval(next.getParent(), next.getStart(), next.getEnd(), false, "");
          Iterator<Interval> iter = buffer.iterator();
          while (iter.hasNext()) {
            Interval buffered = iter.next();
            sb.append(',').append(buffered.getId());
            if (!buffered.intersects(next)) {
              iter.remove();
            } else {
              if (nextCurrent.getStart() < buffered.getStart()) {
                nextCurrent.setStart(buffered.getStart());
              }
              if (nextCurrent.getEnd() > buffered.getEnd()) {
                nextCurrent.setEnd(buffered.getEnd());
              }
            }
          }
          sb.deleteCharAt(0);
          current.setId(sb.toString());
          retVal.add(current);
          current = nextCurrent;
        }
        buffer.add(next);
      }
    }
    return retVal;
  }

  private void parseIntervals(String sampleName, File bedFile, Map<String, List<Interval>> chrIntervals) {
    BufferedReader reader = null;
    Genome genome = new Genome();
    try {
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(bedFile), Charset.forName("ASCII")));
      while (true) {
        String line = reader.readLine();
        if (line == null)
          break;

        if (line.length() == 0) {
          continue;
        }

        String values[] = line.split("\t");
        if (!chrIntervals.containsKey(values[0])) {
          chrIntervals.put(values[0], new ArrayList<Interval>());
        }
        chrIntervals.get(values[0]).add(new Interval(genome.getOrCreateChromosome(values[0]), Gpr.parseIntSafe(values[1]), Gpr.parseIntSafe(values[2]) - 1, false, sampleName));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }
}
