/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Deque;
import java.util.List;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class GenderEstimator extends DefaultTool {
  private File vcfFile;
  private String sexChr = "X";
  private double maleHomRatio = 0.8;
  private double femaleHomRatio = 0.5;
  private String sample = null;
  private int sampleIdx = -1;
  private int minGenotypeQual = 0;
  private int minDepth = 0;
  private double minQual = 100;
  private char gender = 'A';

  @Override
  public String getCmdName() {
    return "gender";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf          VCF");
    System.out.println("\t--sexChr       Sex chromosome. (default: " + sexChr + ")");
    System.out.println("\t--femaleRatio  Female Homozygous ratio. Needs to be lower or equal than this. (default: " + femaleHomRatio + ")");
    System.out.println("\t--maleRatio    Male Homozygous ratio. Needs to be higher or equal than this. (default: " + maleHomRatio + ")");
    System.out.println("\t--minDepth     Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minGQ        Minimum tumor genotype quality. (default: " + minGenotypeQual + ")");
    System.out.println("\t--minQual      Minimum variant quality. (default: " + minQual + ")");
    System.out.println("\t--sample       Sample to evaluate in multi-sample vcf. Default is always the first");
    System.out.println("\t--sampleIdx    Index of sample to evaluate in multi-sample vcf. Default is always the first");
    System.out.println("\t--gender       Gender should be this. Values F,M (optional)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcfFile = new File(arguments.pop());
      } else if (arg.equals("--sexChr")) {
        sexChr = Chromosome.simpleName(arguments.pop());
      } else if (arg.equals("--femaleRatio")) {
        femaleHomRatio = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--maleRatio")) {
        maleHomRatio = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--sample")) {
        sampleIdx = -1;
        sample = arguments.pop();
      } else if (arg.equals("--sampleIdx")) {
        sample = null;
        sampleIdx = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minGQ")) {
        minGenotypeQual = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minQual")) {
        minQual = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--gender")) {
        gender = arguments.pop().charAt(0);
      } else {
        unusedArgs.add(arg);
      }
    }

    if (vcfFile == null) {
      printUsage("You need to pass --vcf");
      return 1;
    }
    if (gender != 'A' && gender != 'F' && gender != 'M') {
      printUsage("gender can only be F or M");
      return 1;
    }

    // Default first sample
    if (sampleIdx == -1 && sample == null) {
      sampleIdx = 0;
    }

    return 0;
  }

  @Override
  public int run() {
    estimateGender();
    return 0;
  }

  public void estimateGender() {
    VcfFileIterator vcfParser = null;
    try {
      vcfParser = new VcfFileIterator(vcfFile.toString());
      VcfHeader header = vcfParser.readHeader();
      List<String> sampleNames = header.getSampleNames();
      if (sampleIdx == -1 && sample != null) {
        for (int idx = 0; idx < sampleNames.size(); idx++) {
          if (sampleNames.get(idx).equals(sample)) {
            sampleIdx = idx;
            break;
          }
        }
      }

      long totalVariants = 0;
      long totalTestedVariants = 0;
      long totalHomVariants = 0;
      for (VcfEntry vcfEntry : vcfParser) {
        if (!Chromosome.simpleName(vcfEntry.getChromosomeName()).equals(sexChr))
          continue;

        totalVariants++;
        VcfGenotype genotype = vcfEntry.getVcfGenotype(sampleIdx);
        boolean passedGQ = true;
        if (minGenotypeQual > 0) {
          passedGQ = Gpr.parseIntSafe(genotype.get("GQ")) >= minGenotypeQual;
        }

        if (passedGQ && vcfEntry.getQuality() >= minQual && Gpr.parseIntSafe(genotype.get("DP")) >= minDepth) {
          totalTestedVariants++;
          if (genotype.isHomozygous())
            totalHomVariants++;
        }
      }

      DecimalFormat formatter = new DecimalFormat("#0.00");
      double homRatio = (double) totalHomVariants / (double) totalTestedVariants;
      System.out.println("Nb variants: " + totalVariants);
      System.out.println("Nb tested variants: " + totalTestedVariants);
      System.out.println("Nb hom. variants:   " + totalHomVariants);
      System.out.println("Hom ratio:          " + formatter.format(homRatio));
      System.out.print("Gender Estimate is: ");
      char genderEst = 'A';
      if (homRatio <= femaleHomRatio) {
        System.out.println("Female");
        genderEst = 'F';
      } else if (homRatio >= maleHomRatio) {
        System.out.println("Male");
        genderEst = 'M';
      } else {
        System.out.println("Ambiguous");
      }

      if (gender != 'A') {
        System.out.print("Test outcome:       ");
        if (gender == genderEst) {
          System.out.println("PASSED");
        } else {
          System.out.println("FAILED");
        }
      }
    } finally {
      if (vcfParser != null) {
        vcfParser.close();
      }
    }
  }

}
