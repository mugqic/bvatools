/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Deque;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.graphs.ChromosomeFrequencyPlot;
import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.interval.Genome;

public class FrequencyLOH extends DefaultTool {
  public static final int NORMAL_VCF_IDX = 0;
  public static final int TUMOR_VCF_IDX = 1;

  private SAMSequenceDictionary referenceDictionary;
  private File normal;
  private File tumor;
  private int width = 1280;
  private int height = 768;
  private int minDepth = 10;
  private double hetThreshold = 0.35;
  private String outputPrefix = null;

  @Override
  public String getCmdName() {
    return "freqloh";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--normal      Allele counts for normal");
    System.out.println("\t--tumor       Allele counts for tumor");
    System.out.println("\t--refdict     Reference dictionary (required for --plot only)");
    System.out.println("\t--minDepth    Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--width       Plot width. (default: " + width + ")");
    System.out.println("\t--height      Plot height. (default: " + height + ")");
    System.out.println("\t--het         At which threshold do we call a valid het. (default: " + hetThreshold + ")");
    System.out.println("\t--prefix      Output prefix");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--normal")) {
        normal = new File(arguments.pop());
      } else if (arg.equals("--tumor")) {
        tumor = new File(arguments.pop());
      } else if (arg.equals("--het")) {
        hetThreshold = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--width")) {
        width = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--height")) {
        height = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--prefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--refdict")) {
        referenceDictionary = SAMUtils.getSequenceDictionary(new File(arguments.pop()));
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (normal == null) {
      printUsage("You need to pass --normal");
      return 1;
    }
    if (tumor == null) {
      printUsage("You need to pass --tumor");
      return 1;
    }
    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }
    if (referenceDictionary == null) {
      printUsage("refdict is needed");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    computeLOH(outputPrefix);

    return 0;
  }

  public void computeLOH(String outputPrefix) {
    BufferedReader normalReader = null;
    BufferedReader tumorReader = null;
    PrintWriter out = null;
    try {
      normalReader = new BufferedReader(new InputStreamReader(new FileInputStream(normal), "ASCII"), 100 * 1024);
      tumorReader = new BufferedReader(new InputStreamReader(new FileInputStream(tumor), "ASCII"), 100 * 1024);
      normalReader.readLine(); // skip header
      tumorReader.readLine(); // skip header

      ChromosomeFrequencyPlot chrPlot = null;
      Genome genome = new Genome();

      for (SAMSequenceRecord seq : referenceDictionary.getSequences()) {
        genome.add(new Chromosome(genome, 0, seq.getSequenceLength() - 1, seq.getSequenceName()));
      }
      chrPlot = new ChromosomeFrequencyPlot(genome, width, height);

      out = new PrintWriter(outputPrefix + ".csv");
      while (true) {
        String normalLine = null;
        while (true) {
          normalLine = normalReader.readLine();
          if (normalLine != null && normalLine.length() > 0 && normalLine.charAt(0) == '#')
            continue;
          break;
        }
        String tumorLine = null;
        while (true) {
          tumorLine = tumorReader.readLine();
          if (tumorLine != null && tumorLine.length() > 0 && tumorLine.charAt(0) == '#')
            continue;
          break;
        }

        if (normalLine == null || tumorLine == null)
          break;

        String normFreqValues[] = normalLine.split(",");
        String normFreqChr = normFreqValues[0];
        String normFreqKey = normFreqChr + '#' + normFreqValues[1];

        String tumFreqValues[] = tumorLine.split(",");
        String tumFreqChr = tumFreqValues[0];
        String tumFreqKey = tumFreqChr + '#' + tumFreqValues[1];

        if (!normFreqKey.equals(tumFreqKey)) {
          throw new RuntimeException("BaseFreq files from normal and tumor aren't in sync: " + normalLine + " vs " + tumorLine);
        }

        int freqStart = Integer.parseInt(normFreqValues[1]);
        AlleleCounts alleleCounts = AlleleCounts.valueOf(normFreqChr, freqStart, normFreqValues[2], tumFreqValues[2]);

        String alleleA = null;
        String alleleB = null;
        int alleleACnts = 0;
        int alleleBCnts = 0;
        for (String allele : alleleCounts.getReferenceBAMCounts().keySet()) {
          int cnt = alleleCounts.getReferenceBAMCounts().get(allele);
          if (cnt > alleleBCnts) {
            alleleBCnts = cnt;
            alleleB = allele;
          }
          if (alleleBCnts > alleleACnts) {
            String tmp = alleleA;
            int tmpCnt = alleleACnts;
            alleleACnts = alleleBCnts;
            alleleA = alleleB;
            alleleB = tmp;
            alleleBCnts = tmpCnt;
          }
        }

        alleleCounts.setAlleleA(alleleA);
        alleleCounts.setAlleleB(alleleB);

        int normalDepth = alleleCounts.getReferenceBAMCounts().get(alleleCounts.getAlleleA())
            + alleleCounts.getReferenceBAMCounts().get(alleleCounts.getAlleleB());
        int tumorDepth = alleleCounts.getBamCounts().get(alleleCounts.getAlleleA()) + alleleCounts.getBamCounts().get(alleleCounts.getAlleleB());
        if (normalDepth < minDepth || tumorDepth < minDepth)
          continue;

        double normalBAlleleFreq = (double) alleleCounts.getReferenceBAMCounts().get(alleleCounts.getAlleleB()) / (double) normalDepth;
        double tumorBAlleleFreq = (double) alleleCounts.getBamCounts().get(alleleCounts.getAlleleB()) / (double) tumorDepth;

        if (normalBAlleleFreq < hetThreshold || normalBAlleleFreq > (1 - hetThreshold))
          continue; // Not a het

        out.print(alleleCounts.getChromosome());
        out.print(',');
        out.print(alleleCounts.getPosition());
        out.print(',');
        out.print(alleleCounts.getAlleleA());
        out.print(',');
        out.print(alleleCounts.getAlleleB());
        out.print(',');
        out.print(normalDepth);
        out.print(',');
        out.print(normalBAlleleFreq);
        out.print(',');
        out.print(tumorDepth);
        out.print(',');
        out.print(tumorBAlleleFreq);
        out.print(',');
        out.print(tumorBAlleleFreq / normalBAlleleFreq);
        out.print(',');
        // double logRatio = Math.log(tumorBAlleleFreq / normalBAlleleFreq)/Math.log(2);
        double logRatio = Math.abs(tumorBAlleleFreq - normalBAlleleFreq);
        out.print(logRatio);
        out.println();
        chrPlot.addPoint(alleleCounts.getChromosome(), alleleCounts.getPosition(), logRatio);
      }
      out.close();
      out = null;
      chrPlot.write(outputPrefix + ".logRatio", false);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (out != null) {
        out.close();
      }

      if (normalReader != null) {
        try {
          normalReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
      if (tumorReader != null) {
        try {
          tumorReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }
}
