/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.set.TShortSet;
import gnu.trove.set.hash.TShortHashSet;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMTagUtil;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.TextTagCodecBVAWriter;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.fastq.AsyncFastqWriter;
import htsjdk.samtools.fastq.BasicFastqWriter;
import htsjdk.samtools.fastq.FastqRecord;
import htsjdk.samtools.fastq.FastqWriter;
import htsjdk.samtools.util.SequenceUtil;
import htsjdk.samtools.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;

public class Bam2Fastq extends DefaultTool {
  private enum OnlyMapped {NO,PAIRED,ONLY};

  private final TextTagCodecBVAWriter tagCodecs = new TextTagCodecBVAWriter();
  private File bamFile = null;
  private File output = null;
  private OnlyMapped mapped = OnlyMapped.NO;
  private Set<String> tagsName = new HashSet<String>();

  @Override
  public String getCmdName() {
    return "bam2fq";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam     BAM file");
    System.out.println("\t--tags    Attributes to keep in the read header");
    System.out.println("\t--mapped  Output only mapped reads. (default: "+mapped+")");
    System.out.println("\t          "+OnlyMapped.NO+": Output all reads");
    System.out.println("\t          "+OnlyMapped.PAIRED+": Output paired mapped reads");
    System.out.println("\t          "+OnlyMapped.ONLY+": Output only mapped reads. This breaks the shuffled output.");
    System.out.println("\t--out     Output file. (default: STDOUT)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {

    while (!arguments.isEmpty()) {
      String arg = arguments.pop();

      if (arg.equals("--out")) {
        output = new File(arguments.pop());
      } else if (arg.equals("--bam")) {
        bamFile = new File(arguments.pop());
      } else if (arg.equals("--tags")) {
        tagsName.add(arguments.pop());
      } else if (arg.equals("--mapped")) {
        mapped = Enum.valueOf(OnlyMapped.class, arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bamFile == null) {
      printUsage("bam not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    TShortSet tags = new TShortHashSet();
    for(String tagName : tagsName) {
      tags.add(SAMTagUtil.getSingleton().makeBinaryTag(tagName));
    }
    
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);
    PrintStream stream = System.out;
    FastqWriter writer = null;
    try {
      if(output != null) {
        stream = new PrintStream(output, "ASCII");
      }
      
      writer = new BasicFastqWriter(stream);
      if (Defaults.USE_ASYNC_IO) {
        writer = new AsyncFastqWriter(writer, AsyncFastqWriter.DEFAULT_QUEUE_SIZE);
      }

      Map<String,SAMRecord> pairs = new HashMap<String,SAMRecord>();
      
      for (SAMRecord read : samReader) {
        if (read.isSecondaryOrSupplementary())
            continue;

        if(mapped == OnlyMapped.NO || mapped == OnlyMapped.PAIRED) {
          if(!read.getReadPairedFlag()) {
            throw new UnsupportedOperationException("Only mapped filter mode '"+OnlyMapped.ONLY+"' supports unpaired reads");
          }

          String readName = read.getReadName();
          SAMRecord first = pairs.remove(readName);
          if(first == null) {
            pairs.put(readName, read);
          }
          else {
            final SAMRecord read1 = read.getFirstOfPairFlag() ? read : first;
            final SAMRecord read2 = read.getFirstOfPairFlag() ? first : read;
            if(mapped == OnlyMapped.PAIRED && !read1.getReadUnmappedFlag() && !read2.getReadUnmappedFlag()) {
              writeRead(tagsName, read1, writer);
              writeRead(tagsName, read2, writer);
            }
            else {
              writeRead(tagsName, read1, writer);
              writeRead(tagsName, read2, writer);
            }
          }
        } else if(mapped == OnlyMapped.ONLY && !read.getReadUnmappedFlag()) {
          writeRead(tagsName, read, writer);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      try {
        samReader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      if(writer != null)
        writer.close();
    }

    return 0;
  }
  
  private void writeRead(Set<String> tagsName, SAMRecord read, FastqWriter writer) {
    String seqHeader = read.getReadName();
    if(read.getReadPairedFlag()) {
      if(read.getFirstOfPairFlag()) {
        seqHeader += "/1";
      }
      else {
        seqHeader += "/2";
      }
    }
    seqHeader += tagCodecs.format(tagsName, read);
    
    String readString = read.getReadString();
    String readQual = read.getBaseQualityString();
    if(read.getReadNegativeStrandFlag()) {
      readString = SequenceUtil.reverseComplement(readString);
      readQual = StringUtil.reverseString(readQual);
    }

    writer.write(new FastqRecord(seqHeader, readString, null, readQual));
  }
}
