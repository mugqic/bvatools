/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.map.hash.TObjectIntHashMap;
import htsjdk.samtools.util.IOUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.parsers.GeneMapper;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.vcf.EffFormatVersion;
import ca.mcgill.mcb.pcingola.vcf.VcfEffect;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;

public class GenePerPatient extends DefaultTool {
  private int minDepth = 10;
  private double minQual = 0.0;
  private int minCLR = 45;
  private GeneMapper mapper = null;
  private boolean onlyDamaging = false;
  private EffFormatVersion snpEffFormat = EffFormatVersion.FORMAT_EFF_3;
  private List<File> vcfs = new ArrayList<File>();

  @Override
  public String getCmdName() {
    return "genesampleimpact";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf            VCFs to load");
    System.out.println("\t--minDepth       Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minQual      Minimum variant quality. (Default: " + minQual + ")");
    System.out.println("\t--minCLR       Minimum CLR to call somatic. (Default: " + minCLR + ")");
    System.out.println("\t--mapper         Genes to keep file. Use to filter for refseq");
    System.out.println("\t--onlyDamaging   Use to only output damaging snps.");
    System.out.println("\t--snpEffFormat   VCF snpEff format. (default: " + snpEffFormat + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcfs.add(new File(arguments.pop()));
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minQual")) {
        minQual = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--minCLR")) {
        minCLR = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--snpEffFormat")) {
        snpEffFormat = Enum.valueOf(EffFormatVersion.class, arguments.pop());
      } else if (arg.equals("--mapper")) {
        mapper = new GeneMapper(new File(arguments.pop()));
      } else if (arg.equals("--onlyDamaging")) {
        onlyDamaging = true;
      } else {
        unusedArgs.add(arg);
      }
    }

    if (vcfs.size() == 0) {
      printUsage("vcf not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    try {
      getImpacts(vcfs);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public Map<String, Map<String, Boolean>> testVCF(VcfFileIterator vcfParser) {
    Map<String, Map<String, Boolean>> gene2Type = new HashMap<String, Map<String, Boolean>>();

    for (VcfEntry vcfEntry : vcfParser) {
      List<VcfEffect> allEffects = vcfEntry.parseEffects(snpEffFormat);
      List<VcfEffect> effects = new ArrayList<VcfEffect>();

      for (VcfEffect effect : allEffects) {
        if (vcfEntry.getQuality() < minQual || vcfEntry.getInfoInt("CLR") < minCLR) {
          continue;
        }

        if (mapper == null || mapper.getTranscriptId(effect.getTranscriptId()) != null) {
          if (onlyDamaging) {
            String polyPhen = vcfEntry.getInfo("dbnsfpPolyphen2_HVAR_pred");
            boolean isDamaging = false;
            if (vcfEntry.getInfoFloat("dbnsfpSIFT_score") <= 0.05f) {
              isDamaging = true;
            }

            if (polyPhen != null && !isDamaging && (polyPhen.indexOf("D") != -1 || polyPhen.indexOf("P") != -1)) {
              isDamaging = true;
            }

            if (isDamaging) {
              effects.add(effect);
            }
          } else {
            effects.add(effect);
          }
        }
      }

      for (VcfEffect effect : effects) {
        if (!gene2Type.containsKey(effect.getGeneName())) {
          gene2Type.put(effect.getGeneName(), new HashMap<String, Boolean>());
        }
        gene2Type.get(effect.getGeneName()).put(effect.getEffectType().toString(), Boolean.TRUE);
      }
    }

    return gene2Type;
  }

  public void getImpacts(List<File> vcfs) throws IOException {
    Map<String, TObjectIntHashMap<String>> gene2NbTypes = new HashMap<String, TObjectIntHashMap<String>>();
    TObjectIntHashMap<String> gene2NbPatients = new TObjectIntHashMap<String>();
    Set<String> types = new HashSet<String>();
    for (File vcf : vcfs) {
      VcfFileIterator vcfParser = new VcfFileIterator(IOUtil.openFileForBufferedReading(vcf));
      Map<String, Map<String, Boolean>> gene2Type = testVCF(vcfParser);
      for (String gene : gene2Type.keySet()) {
        gene2NbPatients.adjustOrPutValue(gene, 1, 1);
        if (!gene2NbTypes.containsKey(gene)) {
          gene2NbTypes.put(gene, new TObjectIntHashMap<String>());
        }
        Map<String, Boolean> impacts = gene2Type.get(gene);
        for (String impact : impacts.keySet()) {
          types.add(impact);
          gene2NbTypes.get(gene).adjustOrPutValue(impact, 1, 1);
        }
      }
    }

    System.out.print("GeneName,TotalPatients");
    for (String type : types) {
      System.out.print(',');
      System.out.print(type);
    }
    System.out.print("\n");

    for (String gene : gene2NbTypes.keySet()) {
      System.out.print(gene);
      System.out.print(',');
      System.out.print(gene2NbPatients.get(gene));
      for (String type : types) {
        System.out.print(',');
        System.out.print(gene2NbTypes.get(gene).get(type));
      }
      System.out.print("\n");
    }
  }
}
