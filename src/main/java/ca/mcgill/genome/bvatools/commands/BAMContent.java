/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.content.CountCategories;
import ca.mcgill.genome.bvatools.content.CountingTranscript;
import ca.mcgill.genome.bvatools.content.EnsemblGTFParser;
import ca.mcgill.genome.bvatools.content.ThreadSafeTypeWriter;
import ca.mcgill.genome.bvatools.content.TypeWriter;
import ca.mcgill.genome.bvatools.readsQC.DefaultQCPlugin;
import ca.mcgill.genome.bvatools.util.SAMUtils;
import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.interval.Markers;

public class BAMContent extends DefaultTool {
  private static final int DEFAULT_HEIGHT = 200;

  private String output;
  private File gtf;
  private File bam;
  private byte threads;
  private boolean useDups;
  private boolean bamPerType;

  @Override
  public String getCmdName() {
    return "bamcontent";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--out                  Output prefix");
    System.out.println("\t--bam                  BAM file");
    System.out.println("\t--gtf                  GTF containing types to classify.");
    System.out.println("\t--threads              Threads to use (default: " + threads + ")");
    System.out.println("\t--bamPerType           Output a BAM per type in the GTF");
    System.out.println("\t--countDups            Don't ignore duplicates. (default: false)");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--threads")) {
        threads = Byte.parseByte(arguments.pop());
      } else if (arg.equals("--out")) {
        output = arguments.pop();
      } else if (arg.equals("--bam")) {
        bam = new File(arguments.pop());
      } else if (arg.equals("--countDups")) {
        useDups = true;
      } else if (arg.equals("--bamPerType")) {
        bamPerType = true;
      } else if (arg.equals("--bamPerType")) {
        gtf = new File(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bam == null) {
      printUsage("bam not set");
      return 1;
    }

    if (gtf == null) {
      printUsage("gtf not set");
      return 1;
    }

    if (output == null) {
      printUsage("out not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    ExecutorService bamExecutor = Executors.newFixedThreadPool(threads);
    Map<String, TypeCount> allCounts = new HashMap<String, TypeCount>();
    long nbReads = 0;

    TypeWriter typeWriter = null;
    try {
      EnsemblGTFParser parser = new EnsemblGTFParser();
      parser.parse(gtf);

      List<CountCategories> counters = new ArrayList<CountCategories>();
      List<Future<Map<String, Integer>>> futures = new ArrayList<Future<Map<String, Integer>>>();
      SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
      SamReader reader = samReaderFactory.open(bam);
      SAMFileHeader samHeader = reader.getFileHeader();
      reader.close();

      Markers exons = new Markers();
      for (Map<String, CountingTranscript> id2Tr : parser.getTranscriptsById().values()) {
        for (CountingTranscript transcript : id2Tr.values()) {
          exons.add(transcript.getExons());
        }
      }
      SAMUtils.sortMarkers(exons, samHeader.getSequenceDictionary());

      Markers regionTypes = new Markers();
      for (Map<String, Markers> types: parser.getType2marker().values()) {
        for(Markers markersPerType : types.values())
          regionTypes.add(markersPerType);
      }
      SAMUtils.sortMarkers(regionTypes, samHeader.getSequenceDictionary());

      if (bamPerType) {
        typeWriter = new ThreadSafeTypeWriter(samHeader, new File(output));
      } else {
        typeWriter = new TypeWriter() {
          @Override
          public void addRead(String type, SAMRecord record) {
            // Passthru
          }

          @Override
          public void close() throws IOException {
          }
        };
      }

      for (SAMSequenceRecord samSequenceRecord : reader.getFileHeader().getSequenceDictionary().getSequences()) {
        String samChromString = samSequenceRecord.getSequenceName();
        Chromosome chromosome = parser.getGenome().getChromosome(samChromString);
        if (chromosome != null) {
          CountCategories countCategories = new CountCategories(parser.getGenome(), null, samChromString, bam, useDups, typeWriter);
          counters.add(countCategories);
          futures.add(bamExecutor.submit(countCategories));
        }
      }
      reader.close();

      for (Future<Map<String, Integer>> future : futures) {
        Map<String, Integer> counts = future.get();

        for (String type : counts.keySet()) {
          if (!allCounts.containsKey(type)) {
            allCounts.put(type, new TypeCount(type, 0));
          }
          nbReads += counts.get(type);
          allCounts.get(type).add(counts.get(type));
        }
      }
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      bamExecutor.shutdownNow();
      try {
        if (typeWriter != null)
          typeWriter.close();
      } catch (IOException e) {
      }
    }

    plotCounts(nbReads, allCounts);
    return 0;
  }

  protected void plotCounts(long nbReads, Map<String, TypeCount> counts) {
    TypeCount typeCounts[] = counts.values().toArray(new TypeCount[0]);
    Arrays.sort(typeCounts, new Comparator<TypeCount>() {
      @Override
      public int compare(TypeCount o1, TypeCount o2) {
        return o2.count - o1.count;
      }
    });

    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    String seriesKey = "Types of hits";
    for (int idx = 0; idx < typeCounts.length; idx++) {
      dataset.addValue((double) typeCounts[idx].getCount() / (double) nbReads, seriesKey, typeCounts[idx].getType());
    }
    // free memory quickly

    File outputFile = new File(output + ".png");

    // create the chart...
    String title = "Types of hits\n(100% != all Reads)";

    JFreeChart chart = ChartFactory.createBarChart( // chart
        title, // title
        "Hit type", // domain axis label
        "rate of hits", // range axis label
        dataset, // data
        PlotOrientation.HORIZONTAL, // orientation
        true, // include legend
        true, false);
    chart.setBackgroundPaint(Color.white);
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    plot.setBackgroundPaint(Color.white);
    plot.setRangeGridlinePaint(Color.lightGray);
    plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
    BarRenderer renderer = (BarRenderer) plot.getRenderer();
    renderer.setBaseItemLabelsVisible(true);
    renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
    renderer.setBarPainter(new StandardBarPainter());
    CategoryAxis categoryAxis = plot.getDomainAxis();
    categoryAxis.setCategoryMargin(0.05);
    categoryAxis.setUpperMargin(0.05);
    categoryAxis.setLowerMargin(0.05);
    NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
    rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    DecimalFormat formatter = new DecimalFormat("#0.00");
    rangeAxis.setNumberFormatOverride(formatter);
    rangeAxis.setAutoRangeIncludesZero(true);
    rangeAxis.setUpperMargin(0.10);

    int additionalHeight = 30 * dataset.getColumnCount() - DEFAULT_HEIGHT;
    if (additionalHeight < 0)
      additionalHeight = 0;
    PrintWriter writer = null;
    try {
      ChartUtilities.saveChartAsPNG(outputFile, chart, DefaultQCPlugin.DEFAULT_WIDTH, DEFAULT_HEIGHT + additionalHeight);

      writer = new PrintWriter(output + ".csv");
      writer.print("Total nbHits,");
      writer.print(nbReads);
      writer.println(",1");

      // Sort them so output stays consistent
      Arrays.sort(typeCounts, 0, typeCounts.length, new Comparator<TypeCount>() {
        @Override
        public int compare(TypeCount o1, TypeCount o2) {
          return o1.getType().compareTo(o2.getType());
        }
      });

      for (TypeCount typeCount : typeCounts) {
        writer.print(typeCount.getType());
        writer.print(',');
        writer.print(typeCount.getCount());
        writer.print(',');
        writer.println(formatter.format((double) typeCount.getCount() / (double) nbReads));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      writer.close();
    }
  }

  static class TypeCount {
    private final String type;
    private int count;

    public TypeCount(String type, int count) {
      super();
      this.type = type;
      this.count = count;
    }

    public void add(int count) {
      this.count += count;
    }

    public void increment() {
      count++;
    }

    public String getType() {
      return type;
    }

    public int getCount() {
      return count;
    }

  }
}
