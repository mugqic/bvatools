/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.reference.IndexedFastaSequenceFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Deque;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.pileup.BamPileup;
import ca.mcgill.genome.bvatools.snpcall.TagDistribution;
import ca.mcgill.genome.bvatools.snpcall.TagSnpCallPileupHandler;
import ca.mcgill.genome.bvatools.snpcall.TagVariant;

public class TagSnpCall extends DefaultTool {
  private int minDepth = 10;
  private int minMappingQuality = 15;
  private int minBaseQuality = 15;
  private int filterTagsPerLociCount = 3;
  private boolean showTagDistribution = false;
  private int tagLength = 21;
  private double hetThreshold = 0.35;
  private IndexedFastaSequenceFile refSequence = null;
  private File bam = null;
  private String outputPrefix = null;

  @Override
  public String getCmdName() {
    return "tagsnpcall";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam                       Input bam");
    System.out.println("\t--minDepth                  Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minMappingQuality         Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
    System.out.println("\t--minBaseQuality            Only count bases with a base quality higher than this value. (default: " + minBaseQuality + ")");
    System.out.println("\t--filterTagsPerLociCount    Minimum (>=) observations required to keep a tag at a specific position. (default: " + filterTagsPerLociCount + ")");
    System.out.println("\t--showTagDistribution       Output tag distribution");
    System.out.println("\t--ref                       Indexed reference genome");
    System.out.println("\t--taglength                 Tag length taken from header. (default: " + tagLength + ")");
    System.out.println("\t--het                       At which threshold do we call a valid het. (default: " + hetThreshold + ")");
    System.out.println("\t--prefix                    Output prefix");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bam")) {
        bam = new File(arguments.pop());
      } else if (arg.equals("--het")) {
        hetThreshold = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--filterTagsPerLociCount")) {
        filterTagsPerLociCount = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--ref")) {
        try {
          refSequence = new IndexedFastaSequenceFile(new File(arguments.pop()));
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      } else if (arg.equals("--minMappingQuality")) {
        minMappingQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minBaseQuality")) {
        minBaseQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--prefix")) {
        outputPrefix = arguments.pop();
      } else if (arg.equals("--showTagDistribution")) {
        showTagDistribution = true;
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--taglength")) {
        tagLength = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bam == null) {
      printUsage("You need to pass --bam");
      return 1;
    }

    if (outputPrefix == null) {
      printUsage("prefix not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {

    try {
      callSnps();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return 0;
  }

  public void callSnps() throws IOException, UnsupportedEncodingException {
    TagSnpCallPileupHandler pileupHandler = new TagSnpCallPileupHandler(tagLength, refSequence, minMappingQuality, minBaseQuality);
    pileupHandler.setFilterTagCount(filterTagsPerLociCount);
    TagDistribution tagDistribution = new TagDistribution(tagLength);
    NumberFormat numberFormat = new DecimalFormat("#0.0000");
    BamPileup bamParser = new BamPileup(bam);
    bamParser.addReadHandler(tagDistribution);
    bamParser.readBam(pileupHandler);

    if (showTagDistribution) {
      tagDistribution.generateDistribution(outputPrefix);
      tagDistribution.generateContent(new File(outputPrefix + ".tagContent.png"));
    }

    final PrintWriter out = new PrintWriter(outputPrefix + ".all.tagCalls.tsv", "ASCII");
    try {
      String header = "Chr\tPos\tRefAllele\tAltAlleles\tRefReadCount\tAltReadCounts\tAltWithHighestPerTagCount";
      header       += "\tBestAltDepth+RefDepth\tBestAltFreq\tNbRefTags\tNbAltTags";
      header       += "\tHighestCountPerTagRef\tLowestCountPerTagRef\tAvgCountPerTagRef\tMedCountPerTagRef";
      header       += "\tHighestCountPerTagAlt\tLowestCountPerTagAlt\tAvgCountPerTagAlt\tMedCountPerTagAlt";
      header       += "\tNbDisagreeingTags";
      out.println(header);
      for (TagVariant variant : pileupHandler.getTagVariants()) {
        final StringBuilder line = new StringBuilder();

        line.append(variant.getChromosome());
        line.append('\t');
        line.append(variant.getPosition());
        line.append('\t');
        line.append((char) variant.getRef());
        line.append('\t');
        line.append((char) variant.getAlts()[0]);
        int topHighestCountPerTagAltIdx = 0;
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append((char) variant.getAlts()[idx]);
          if(variant.getHighestCountPerTagAlts()[topHighestCountPerTagAltIdx] < variant.getHighestCountPerTagAlts()[idx]) {
            topHighestCountPerTagAltIdx = idx;
          }
        }

        // RefReadCount
        line.append('\t');
        line.append(variant.getRefReadCount());
        
        // AltReadCounts
        line.append('\t');
        line.append(variant.getAltsReadCount()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getAltsReadCount()[idx]);
        }

        // AltWithHigestPerTagCount
        line.append('\t');
        line.append((char)variant.getAlts()[topHighestCountPerTagAltIdx]);

        // BestAltDepth+RefDepth
        line.append('\t');
        line.append(variant.getRefReadCount() + variant.getAltsReadCount()[topHighestCountPerTagAltIdx]);
        
        // BestAltFreq
        line.append('\t');
        line.append(numberFormat.format((double) variant.getAltsReadCount()[topHighestCountPerTagAltIdx]/(double)(variant.getRefReadCount() + variant.getAltsReadCount()[topHighestCountPerTagAltIdx])));

        // NbRefTags
        line.append('\t');
        line.append(variant.getRefTagCount());

        // NbAltTags
        line.append('\t');
        line.append(variant.getAltsTagCount()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getAltsTagCount()[idx]);
        }

        // HighestCountPerTagRef
        line.append('\t');
        line.append(variant.getHighestCountPerTagRef());
        // LowestCountPerTagRef
        line.append('\t');
        line.append(variant.getLowestCountPerTagRef());
        // AvgCountPerTagRef
        line.append('\t');
        line.append(variant.getAvgCountPerTagRef());
        // MedCountPerTagRef
        line.append('\t');
        line.append(variant.getMedCountPerTagRef());

        // HighestCountPerTagAlt
        line.append('\t');
        line.append(variant.getHighestCountPerTagAlts()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getHighestCountPerTagAlts()[idx]);
        }
        // LowestCountPerTagAlt
        line.append('\t');
        line.append(variant.getLowestCountPerTagAlts()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getLowestCountPerTagAlts()[idx]);
        }
        // AvgCountPerTagAlt
        line.append('\t');
        line.append(variant.getAvgCountPerTagAlts()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getAvgCountPerTagAlts()[idx]);
        }
        // MedCountPerTagAlt
        line.append('\t');
        line.append(variant.getMedCountPerTagAlts()[0]);
        for (int idx = 1; idx < variant.getAlts().length; idx++) {
          line.append(',');
          line.append(variant.getMedCountPerTagAlts()[idx]);
        }

        // NbDisagreeingTags
        line.append('\t');
        line.append(variant.getDisagreeCount());

        out.println(line);
      }
    } finally {
      out.close();
    }
  }
}
