/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Deque;
import java.util.zip.GZIPOutputStream;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.filterdups.DuplicateKmerBuilder;
import ca.mcgill.genome.bvatools.filterdups.SequenceLengthException;
import ca.mcgill.genome.bvatools.parsers.FASTQFileParser;
import ca.mcgill.genome.bvatools.parsers.FASTQPEFileParser;
import ca.mcgill.genome.bvatools.parsers.FASTQPEFileParser.FastqFormat;
import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.SequenceFileParser;

public class FilterDuplicates extends DefaultTool {
  private final DuplicateKmerBuilder duplicateKmerBuilder = new DuplicateKmerBuilder();
  private File read1 = null;
  private File read2 = null;
  private byte qualOffset = 33;
  private String prefix = null;
  private FastqFormat fastqFormat = FastqFormat.CASAVA1_8;
  private boolean onlyOutputReadNames = false;

  @Override
  public String getCmdName() {
    return "filterdups";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--read1             Fastq of read1");
    System.out.println("\t--read2             Fastq of read2 (optional)");
    System.out.println("\t--fastqFormat       Format of the fastq file. For paired reads only. (default: " + fastqFormat + ")");
    System.out.println("\t                    " + FastqFormat.CASAVA1_8 + " : Illumina casava >= 1.8");
    System.out.println("\t                    CASAVA 1.8:");
    System.out.println("\t                    @EAS139:136:FC706VJ:2:2104:15343:197393 1:Y:18:ATCACG");
    System.out.println("\t                    @EAS139:136:FC706VJ:2:2104:15343:197393 2:Y:18:ATCACG");
    System.out.println("\t                    " + FastqFormat.CASAVA1_6 + " : Illumina casava <= 1.6");
    System.out.println("\t                    CASAVA 1.6:");
    System.out.println("\t                    @HWUSI-EAS100R:6:73:941:1973#0/1");
    System.out.println("\t                    @HWUSI-EAS100R:6:73:941:1973#0/2");
    System.out.println("\t                    " + FastqFormat.NO_PAIR_ENCODING + " : No read encoding.");
    System.out.println("\t                    read1 header == read2 header");
    System.out.println("\t-Q                  Fastq Quality offset. (default: " + qualOffset + ")");
    System.out.println("\t--prefix            Output prefix (default original filename .dup.gz)");
    System.out.println("\t--readOutput        Only output duplicate read names");
    System.out.println("\t-k,--kmer           kmer size (default: " + duplicateKmerBuilder.getKmerSize() + ")");
    System.out.println("\t--estimatedNbReads  Estimated number of reads (for performance reasons) (default: " + duplicateKmerBuilder.getEstimatedMaxItems()
        + ")");
    System.out.println("\t-o,--offset         Read offset to get kmer (default: " + duplicateKmerBuilder.getOffset() + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--read1")) {
        read1 = new File(arguments.pop());
      } else if (arg.equals("--read2")) {
        read2 = new File(arguments.pop());
      } else if (arg.equals("-Q")) {
        qualOffset = Byte.parseByte(arguments.pop());
      } else if (arg.equals("--fastqFormat")) {
        fastqFormat = Enum.valueOf(FastqFormat.class, arguments.pop());
      } else if (arg.equals("--readOutput")) {
        onlyOutputReadNames = true;
      } else if (arg.equals("--prefix")) {
        prefix = arguments.pop();
      } else if (arg.equals("--estimatedNbReads")) {
        duplicateKmerBuilder.setEstimatedMaxItems(Integer.parseInt(arguments.pop()));
      } else if (arg.equals("--kmer") || arguments.pop().equals("-k")) {
        duplicateKmerBuilder.setKmerSize(Integer.parseInt(arguments.pop()));
      } else if (arg.equals("--offset") || arguments.pop().equals("-o")) {
        duplicateKmerBuilder.setOffset(Integer.parseInt(arguments.pop()));
      } else {
        unusedArgs.add(arg);
      }
    }
    if (read1 == null) {
      printUsage("read1 not set");
      return 1;
    }
    if (duplicateKmerBuilder.getEstimatedMaxItems() < 1) {
      printUsage("estimatedNbReads has to be > 0");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    filterDups();

    return 0;
  }

  public void filterDups() {
    BufferedWriter read1Writer = null;
    BufferedWriter read2Writer = null;
    try {
      String read1OutputFile;
      if (prefix != null) {
        read1OutputFile = prefix + ".read1.gz";
      } else {
        read1OutputFile = read1 + ".dup.read1.gz";
      }

      String read2OutputFile = null;
      SequenceFileParser fileParser;
      long totalFileSize = read1.length();

      if (read2 != null) {
        if (prefix != null) {
          read2OutputFile = prefix + ".read2.gz";
        } else {
          read2OutputFile = read2 + ".dup.read2.gz";
        }

        totalFileSize += read2OutputFile.length();
        fileParser = new FASTQPEFileParser(read1, read2, qualOffset, fastqFormat);
      } else {
        fileParser = new FASTQFileParser(read1, qualOffset);
      }

      long lastPercent = -1;
      System.out.println("Build kmer [" + duplicateKmerBuilder.getKmerSize() + "] Hash");
      while (true) {
        Fragment fragment = fileParser.nextSequence();
        if (fragment == null)
          break;

        duplicateKmerBuilder.processSequence(fragment);

        long percent = fileParser.getByteCount() * 100l / totalFileSize;
        if (percent != lastPercent) {
          lastPercent = percent;
          System.out.print("\rFile read: " + percent + "%");
        }
      }
      System.out.println();
      fileParser.close();

      double nbKmers = duplicateKmerBuilder.getNbKnownSequencesFound().size();
      double total = duplicateKmerBuilder.getTotalNbReads();
      System.out.println("Dup: " + (100.0 - (nbKmers * 100.0d / total)) + '%');

      // Second pass
      fileParser = new FASTQFileParser(read1, qualOffset);
      totalFileSize = read1.length();
      read1Writer = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(read1OutputFile)), Charset.forName("ASCII")));
      if (read2 != null) {
        FASTQFileParser filePEParser = new FASTQFileParser(read2, qualOffset);
        read2Writer = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(read2OutputFile)), Charset.forName("ASCII")));

        lastPercent = -1;
        System.out.println("Extract 'best' unique reads");
        while (true) {
          Fragment read1Sequence = fileParser.nextSequence();
          Fragment read2Sequence = filePEParser.nextSequence();
          if (read1Sequence == null)
            break;

          Fragment fragment = new Fragment(read1Sequence.getName(), null, null);
          fragment.insertRead(0, read1Sequence.getRead(0));
          fragment.insertRead(1, read2Sequence.getRead(0));
          if (!duplicateKmerBuilder.longEnoughReads(fragment)) {
            if (onlyOutputReadNames) {
              read1Writer.append(read1Sequence.getName());
              read1Writer.newLine();
              read2Writer.append(read2Sequence.getName());
              read2Writer.newLine();
            }
            continue;
          }

          boolean isWritable;
          try {
            isWritable = duplicateKmerBuilder.getKmerCountAndMark(fragment);
          } catch (SequenceLengthException e) {
            throw new RuntimeException("Kmer too long, or offset to big: " + read1Sequence.getName());
          }
          if (isWritable && !onlyOutputReadNames) {
            read1Writer.append('@').append(read1Sequence.getName());
            read1Writer.newLine();
            read1Writer.append(fragment.getRead(0).getSequence());
            read1Writer.newLine();
            read1Writer.append('+').append(read1Sequence.getName());
            read1Writer.newLine();
            for (int idx = 0; idx < fragment.getRead(0).getBaseQualities().length; idx++) {
              read1Writer.append((char) (fragment.getRead(0).getBaseQualities()[idx] + 33));
            }
            read1Writer.newLine();
            read2Writer.append('@').append(read2Sequence.getName());
            read2Writer.newLine();
            read2Writer.append(fragment.getRead(1).getSequence());
            read2Writer.newLine();
            read2Writer.append('+').append(read2Sequence.getName());
            read2Writer.newLine();
            for (int idx = 0; idx < fragment.getRead(1).getBaseQualities().length; idx++) {
              read2Writer.append((char) (fragment.getRead(1).getBaseQualities()[idx] + 33));
            }
            read2Writer.newLine();
          } else if (!isWritable && onlyOutputReadNames) {
            read1Writer.append(read1Sequence.getName());
            read1Writer.newLine();
            read2Writer.append(read2Sequence.getName());
            read2Writer.newLine();
          }

          long percent = fileParser.getByteCount() * 100l / totalFileSize;
          if (percent != lastPercent) {
            lastPercent = percent;
            System.out.print("\rFile read: " + percent + "%");
          }
        }
        System.out.println();
        read1Writer.close();
        read2Writer.close();
        fileParser.close();
        filePEParser.close();
      } else {
        lastPercent = -1;
        System.out.println("Extract 'best' unique reads");
        while (true) {
          Fragment fragment = fileParser.nextSequence();
          if (fragment == null)
            break;

          if (duplicateKmerBuilder.longEnoughReads(fragment)) {
            if (onlyOutputReadNames) {
              read1Writer.append(fragment.getName());
              read1Writer.newLine();
            }
            continue;
          }

          boolean isWritable;
          try {
            isWritable = duplicateKmerBuilder.getKmerCountAndMark(fragment);
          } catch (SequenceLengthException e) {
            throw new RuntimeException("Kmer too long, or offset to big: " + fragment.getName());
          }

          if (isWritable && !onlyOutputReadNames) {
            read1Writer.append('@').append(fragment.getName());
            read1Writer.newLine();
            read1Writer.append(fragment.getRead(0).getSequence());
            read1Writer.newLine();
            read1Writer.append('+').append(fragment.getName());
            read1Writer.newLine();
            for (int idx = 0; idx < fragment.getRead(0).getBaseQualities().length; idx++) {
              read1Writer.append((char) (fragment.getRead(0).getBaseQualities()[idx] + 33));
            }
            read1Writer.newLine();
          } else if (!isWritable && onlyOutputReadNames) {
            read1Writer.append(fragment.getName());
            read1Writer.newLine();
          }

          long percent = fileParser.getByteCount() * 100l / totalFileSize;
          if (percent != lastPercent) {
            lastPercent = percent;
            System.out.print("\rFile read: " + percent + "%");
          }
        }
        System.out.println();
        fileParser.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (read1Writer != null) {
        try {
          read1Writer.close();
        } catch (IOException e) {
          // oh well
        }
      }
      if (read2Writer != null) {
        try {
          read2Writer.close();
        } catch (IOException e) {
          // oh well
        }
      }
    }
  }

  public static void printGC() {
    runGC();
    System.out.println("Mem: " + (usedMemory() / 1024 / 1024));
  }

  private static void runGC() {
    // It helps to call Runtime.gc()
    // using several method calls:
    for (int r = 0; r < 4; ++r)
      _runGC();
  }

  private static void _runGC() {
    long usedMem1 = usedMemory(), usedMem2 = Long.MAX_VALUE;
    for (int i = 0; (usedMem1 < usedMem2) && (i < 500); ++i) {
      s_runtime.runFinalization();
      s_runtime.gc();
      Thread.yield();

      usedMem2 = usedMem1;
      usedMem1 = usedMemory();
    }
  }

  private static long usedMemory() {
    return s_runtime.totalMemory() - s_runtime.freeMemory();
  }

  private static final Runtime s_runtime = Runtime.getRuntime();
}
