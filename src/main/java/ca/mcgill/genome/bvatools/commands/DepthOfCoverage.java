/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.list.array.TIntArrayList;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SAMSequenceRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.tribble.bed.BEDCodec;
import htsjdk.tribble.bed.BEDFeature;
import htsjdk.tribble.readers.LineIterator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.depth.DepthComputer;
import ca.mcgill.genome.bvatools.depth.DepthInterval;
import ca.mcgill.genome.bvatools.parsers.CachedReferenceSequenceFile;
import ca.mcgill.mcb.pcingola.interval.ChromosomeSimpleName;

public class DepthOfCoverage extends DefaultTool {
  private File inputBAM;
  private int maxDepth = 1000;
  private boolean ommitN = false;
  private TIntArrayList summaryCoverageThresholds;
  private File refFasta;
  private int threads = 1;
  private int minMappingQuality = 0;
  private int minBaseQuality = 0;
  private List<DepthInterval> intervals;
  private boolean computeGC = false;
  private int binsize = 0;
  private boolean simplifyChrName = false;
  private boolean noRefCache = false;
  private File intervalFile = null;

  public DepthOfCoverage() {
    super();
    summaryCoverageThresholds = new TIntArrayList();
    summaryCoverageThresholds.add(10);
    summaryCoverageThresholds.add(100);
    intervals = new ArrayList<DepthInterval>();
  }

  @Override
  public String getCmdName() {
    return "depthofcoverage";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam                        Input BAM");
    System.out.println("\t--gc                         Compute GC content. (Needs --ref)");
    System.out.println("\t--maxDepth                   Maximum depth to compute. The higher the value, the more RAM is needed. (default: " + maxDepth + ")");
    System.out.println("\t--minMappingQuality          Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
    System.out.println("\t--minBaseQuality             Only count bases with a base quality higher than this value. (default: " + minBaseQuality + ")");
    System.out.println("\t--ommitN                     Don't coun't N bases. Needs reference.");
    System.out.println("\t--ref                        Indexed reference genome");
    System.out.println("\t--noRefCache                 Don't load all of the reference in RAM. Hits disk for every check. Saves RAM at the cost of performance");
    System.out.println("\t--simpleChrName              If the chromosome names in the bed file cannot be found in the BAM dict. remove 'chr' and try to find them: chr1 -> 1. (default: " + simplifyChrName + ")");
    System.out.print("\t--summaryCoverageThresholds  Compute percentage of bases covered at given values. (default: ");
    for (int idx = 0; idx < summaryCoverageThresholds.size(); idx++) {
      if (idx > 0) {
        System.out.print(',');
      }
      System.out.print(summaryCoverageThresholds.get(idx));
    }
    System.out.println(')');
    System.out.println("\t--threads                    Threads to use. (default: " + threads + ")");
    System.out.println("\t--intervals                  Intervals in bed format. (Optional)");
    System.out.println("\t--binsize                    Builds binsize intervals on each chromosome (Optional).");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bam")) {
        inputBAM = new File(arguments.pop());
      } else if (arg.equals("--gc")) {
        computeGC = true;
      } else if (arg.equals("--intervals")) {
        intervalFile = new File(arguments.pop());
      } else if (arg.equals("--maxDepth")) {
        maxDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minMappingQuality")) {
        minMappingQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minBaseQuality")) {
        minBaseQuality = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--ommitN")) {
        ommitN = true;
      } else if (arg.equals("--simpleChrName")) {
        simplifyChrName = true;
      } else if (arg.equals("--ref")) {
        refFasta = new File(arguments.pop());
      } else if (arg.equals("--noRefCache")) {
        noRefCache = true;
      } else if (arg.equals("--threads")) {
        threads = Byte.parseByte(arguments.pop());
      } else if (arg.equals("--binsize")) {
        binsize = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--summaryCoverageThresholds")) {
        String values[] = arguments.pop().split(",");
        for (String value : values) {
          if (!summaryCoverageThresholds.contains(Integer.parseInt(value))) {
            summaryCoverageThresholds.add(Integer.parseInt(value));
          }
        }
      } else {
        unusedArgs.add(arg);
      }
    }

    if (inputBAM == null) {
      printUsage("Missing inputBAM");
      return 1;
    }
    if (binsize > 0 && intervalFile != null) {
      printUsage("Only one of binsize or intervals can be used");
      return 1;
    }
    if (refFasta != null && !computeGC && !ommitN) {
      System.err.println("WARN: Turning ref off since neither gc or ommitN were passed");
      refFasta = null;
    }
    if (computeGC && refFasta == null) {
      printUsage("gc needs ref");
      return 1;
    }
    if (ommitN && refFasta == null) {
      printUsage("ommitN needs ref");
      return 1;
    }
    summaryCoverageThresholds.sort();

    if(maxDepth <= summaryCoverageThresholds.get(summaryCoverageThresholds.size()-1)) {
      printUsage("maxDepth has to be > than the biggest summaryCoverageThresholds given");
      return 1;
    }
    return 0;
  }

  @Override
  public int run() {
    if (intervalFile != null) {
      parseIntervals(intervalFile);
    } else {
      generateIntervals();
    }

    try {
      computeCoverage();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return 0;
  }

  private void generateIntervals() {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader reader = samReaderFactory.open(inputBAM);
    
    try {
      SAMFileHeader header = reader.getFileHeader();
      if (!header.getSortOrder().equals(SAMFileHeader.SortOrder.coordinate) || !reader.hasIndex()) {
        throw new RuntimeException("BAM must be coordinate sorted and be indexed");
      }

      SAMSequenceDictionary dictionary = header.getSequenceDictionary();
      for (SAMSequenceRecord record : dictionary.getSequences()) {
        if (binsize > 0) {
          for (int idx = 1; idx <= record.getSequenceLength(); idx += binsize) {
            int end = idx + binsize;
            if (end > record.getSequenceLength()) {
              end = record.getSequenceLength();
            }
            intervals.add(new DepthInterval(maxDepth, record.getSequenceName() + ':' + idx + '-' + end, record.getSequenceName(), idx, end));
          }
        } else {
          intervals.add(new DepthInterval(maxDepth, record.getSequenceName(), record.getSequenceName(), 1, record.getSequenceLength()));
        }
      }
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        System.err.println(e);
      }
    }
  }

  private void parseIntervals(File bedFile) {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader reader = samReaderFactory.open(inputBAM);
    
    SAMSequenceDictionary dictionary; 
    try {
      SAMFileHeader header = reader.getFileHeader();
      dictionary = header.getSequenceDictionary();
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        System.err.println(e);
      }
    }

    AbstractFeatureReader<BEDFeature, LineIterator> featureReader = null;
    intervals = new ArrayList<DepthInterval>();
    try {
      featureReader = AbstractFeatureReader.getFeatureReader(bedFile.getAbsolutePath(), new BEDCodec(), false);
      featureReader.close();
      CloseableIterator<BEDFeature> iter = featureReader.iterator();
      while(iter.hasNext()) {
        BEDFeature feature = iter.next();
        String name = feature.getChr();
        if(feature.getName() != null && feature.getName().length() > 0) {
          name = feature.getName();
        }
        String chr = feature.getChr();
        if(simplifyChrName) {
          if(dictionary.getSequence(chr) == null) {
            String tmpChr = ChromosomeSimpleName.get(chr);
            if(dictionary.getSequence(tmpChr) != null) {
              chr = tmpChr;
            }
          }
        }

        intervals.add(new DepthInterval(maxDepth, name, chr, feature.getStart(), feature.getEnd()));
      }
      iter.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (featureReader != null) {
        try {
          featureReader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  private void sortIntervals() {
    SamReader reader = null;
    try {
      
      SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
      reader = samReaderFactory.open(inputBAM);
      SAMFileHeader header = reader.getFileHeader();
      final SAMSequenceDictionary dictionary = header.getSequenceDictionary();

      // Change type for deque
      LinkedList<DepthInterval> tmpList = new LinkedList<DepthInterval>();
      for (DepthInterval interval : intervals) {
        int idx = dictionary.getSequenceIndex(interval.getChromosome());
        if (idx == -1)
          System.err.println("Couldn't find: " + interval.getChromosome() + " in bam");
        interval.setChromosomeIndex(idx);
        tmpList.add(interval);
      }
      intervals = tmpList;

      Collections.sort(intervals, new Comparator<DepthInterval>() {
        @Override
        public int compare(DepthInterval o1, DepthInterval o2) {
          int delta = o1.getChromosomeIndex() - o2.getChromosomeIndex();
          if (delta != 0)
            return delta;

          return o1.getStart() - o2.getStart();
        }
      });
    }
    finally {
      if(reader != null)
        try {
          reader.close();
        } catch (IOException e) {
          System.err.println(e);
        }
    }
  }

  public void computeCoverage() throws IOException {
    sortIntervals();
    
    if(intervals.size() < threads) {
      threads = intervals.size();
    }

    int parts = intervals.size() / threads;

    DepthComputer depthComputers[] = new DepthComputer[threads];
    int idx=0;
    ReferenceSequenceFile refSequences = null;
    if(refFasta != null && !noRefCache) {
      refSequences = new CachedReferenceSequenceFile(refFasta);
    }
    for(; idx < threads-1; idx++) {
      int offset = idx*parts;
      List<DepthInterval> newIntervals = intervals.subList(offset, offset+parts);
      if(refFasta != null && noRefCache) {
        refSequences = new IndexedFastaSequenceFile(refFasta);
      }
      depthComputers[idx] = new DepthComputer(inputBAM, refSequences, newIntervals, ommitN, computeGC, minMappingQuality, minBaseQuality);
    }
    int offset = idx*parts;
    List<DepthInterval> newIntervals = intervals.subList(offset, intervals.size());
    if(refFasta != null && noRefCache) {
      refSequences = new IndexedFastaSequenceFile(refFasta);
    }
    depthComputers[idx] = new DepthComputer(inputBAM, refSequences, newIntervals, ommitN, computeGC, minMappingQuality, minBaseQuality);
    
    Thread workers[] = new Thread[threads];
    ThreadGroup computerGrp = new ThreadGroup("DepthComputers");
    for (idx = 0; idx < threads; idx++) {
      workers[idx] = new Thread(computerGrp, depthComputers[idx]);
      workers[idx].setUncaughtExceptionHandler(depthComputers[idx]);
      workers[idx].setDaemon(true);
      workers[idx].start();
    }

    for (Thread worker : workers) {
      try {
        worker.join();
        for (DepthComputer depthComputer : depthComputers) {
          if(depthComputer.getCaughtException() != null) {
            throw new RuntimeException(depthComputer.getCaughtException());
          }
        }
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }

    DepthInterval globalInterval = new DepthInterval(maxDepth, "Total");
    for (DepthInterval interval : intervals) {
      globalInterval.add(interval);
    }

    globalInterval.printReportHeader(summaryCoverageThresholds, System.out);
    System.out.println();
    globalInterval.printReport(summaryCoverageThresholds, System.out);
    System.out.println();
    for (DepthInterval interval : intervals) {
      interval.printReport(summaryCoverageThresholds, System.out);
      System.out.println();
    }
    
    if(noRefCache) {
      for(DepthComputer depthComputer : depthComputers) {
        if(depthComputer.getRef() != null) {
          ((IndexedFastaSequenceFile)depthComputer.getRef()).close();
        }
      }
    }
    else {
      if(refSequences != null)
      refSequences.close();
    }
  }

  public static Map<String, File> parseSamplesFile(File samplesFile) {
    BufferedReader reader = null;
    Map<String, File> retVal = new HashMap<String, File>();
    try {
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(samplesFile), Charset.forName("ASCII")));
      while (true) {
        String line = reader.readLine();
        if (line == null)
          break;

        if (line.length() == 0 || line.startsWith("#")) {
          continue;
        }
        String values[] = line.split(",");
        retVal.put(values[0], new File(values[1]));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
    return retVal;
  }

}
