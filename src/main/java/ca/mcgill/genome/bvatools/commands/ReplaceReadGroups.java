/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMReadGroupRecord;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import ca.mcgill.genome.bvatools.DefaultTool;

public class ReplaceReadGroups extends DefaultTool {
  private File bam = null;
  private File outputBAM = null;
  private String rgid = null;
  private String rglb = null;
  private String rgpl = null;
  private String rgpu = null;
  private String rgsm = null;
  private String rgcn = null;
  private String rgds = null;

  @Override
  public String getCmdName() {
    return "replaceRG";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam     BAM file");
    System.out.println("\t--output  Output BAM");
    System.out.println("\t--rgid    Read Group ID");
    System.out.println("\t--rglb    Read Group Library");
    System.out.println("\t--rgpl    Read Group platform (e.g. illumina, solid)");
    System.out.println("\t--rgpu    Read Group platform unit (eg. run barcode)");
    System.out.println("\t--rgsm    Read Group sample name");
    System.out.println("\t--rgcn    Read Group sequencing center name");
    System.out.println("\t--rgds    Read Group description");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bam")) {
        bam = new File(arguments.pop());
      } else if (arg.equals("--output")) {
        outputBAM = new File(arguments.pop());
      } else if (arg.equals("--rgid")) {
        rgid = arguments.pop();
      } else if (arg.equals("--rglb")) {
        rglb = arguments.pop();
      } else if (arg.equals("--rgpl")) {
        rgpl = arguments.pop();
      } else if (arg.equals("--rgpu")) {
        rgpu = arguments.pop();
      } else if (arg.equals("--rgsm")) {
        rgsm = arguments.pop();
      } else if (arg.equals("--rgcn")) {
        rgcn = arguments.pop();
      } else if (arg.equals("--rgds")) {
        rgds = arguments.pop();
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bam == null && outputBAM == null && rgid == null && rgpl == null && rgsm == null && rgpu == null) {
      printUsage("bam, output and rgid not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader in = samReaderFactory.open(bam);

    // create the read group we'll be using
    SAMReadGroupRecord rg = new SAMReadGroupRecord(rgid);
    rg.setLibrary(rglb);
    rg.setPlatform(rgpl);
    rg.setSample(rgsm);
    rg.setPlatformUnit(rgpu);
    if (rgcn != null)
      rg.setSequencingCenter(rgcn);
    if (rgds != null)
      rg.setDescription(rgds);

    System.out.println(String.format("Created read group ID=%s PL=%s LB=%s SM=%s%n", rg.getId(), rg.getPlatform(), rg.getLibrary(), rg.getSample()));

    // create the new header and output file
    final SAMFileHeader inHeader = in.getFileHeader();
    final SAMFileHeader outHeader = inHeader.clone();
    List<SAMReadGroupRecord> samReadGroupRecords = inHeader.getReadGroups();
    boolean foundRG = false;
    List<SAMReadGroupRecord> newReadGroups = new ArrayList<SAMReadGroupRecord>(samReadGroupRecords.size());
    for (SAMReadGroupRecord samReadGroupRecord : samReadGroupRecords) {
      if (samReadGroupRecord.getId().equals(rg.getId())) {
        foundRG = true;
        newReadGroups.add(rg);
      } else {
        newReadGroups.add(samReadGroupRecord);
      }
    }

    if (!foundRG) {
      // throw new RuntimeException("Read group didn't exist: " + rgid);
      System.err.println(String.format("Read group didn't exist: %s", rgid));
    }

    outHeader.setReadGroups(newReadGroups);
    // if (SORT_ORDER != null) outHeader.setSortOrder(SORT_ORDER);

    final SAMFileWriter outWriter = new SAMFileWriterFactory().makeSAMOrBAMWriter(outHeader, outHeader.getSortOrder() == inHeader.getSortOrder(), outputBAM);

    for (final SAMRecord read : in) {
      outWriter.addAlignment(read);
    }

    // cleanup
    try {
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    outWriter.close();
    return 0;
  }
}
