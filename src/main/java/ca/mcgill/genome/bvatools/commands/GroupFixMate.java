/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamPairUtil;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.util.BlockCompressedOutputStream;

import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import ca.mcgill.genome.bvatools.DefaultTool;

public class GroupFixMate extends DefaultTool {
  private int compressionLevel = BlockCompressedOutputStream.getDefaultCompressionLevel();
  private File bamFile = null;
  private File output = null;

  @Override
  public String getCmdName() {
    return "groupfixmate";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("NOTE: This will only order paired Primary alignments together");
    System.out.println("Secondary or Supplementary will be written where they are found");
    System.out.println("Use samtools (>0.2.x) bamshuf to group them as well");
    System.out.println();
    System.out.println("\t--bam     BAM file");
    System.out.println("\t--level   Compression level. (default: "+compressionLevel+")");
    System.out.println("\t--out     Output file.");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--out")) {
        output = new File(arguments.pop());
      } else if (arg.equals("--bam")) {
        bamFile = new File(arguments.pop());
      } else if (arg.equals("--level")) {
        compressionLevel = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bamFile == null) {
      printUsage("bam not set");
      return 1;
    }
    if (output == null) {
      printUsage("out not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
    SamReader samReader = samReaderFactory.open(bamFile);
    
    SAMFileHeader header = samReader.getFileHeader().clone();
    
    // It is not queryname sorted...it's a hybrid
    header.setSortOrder(SortOrder.unsorted);
    SAMFileWriter writer = null;

    long maxCachedReads = 0;
    try {
      writer = new SAMFileWriterFactory().makeBAMWriter(header, true, output, compressionLevel);

      Map<String,SAMRecord> pairs = new HashMap<String,SAMRecord>();
      
      for (SAMRecord read : samReader) {
        if (read.isSecondaryOrSupplementary()) {
          writer.addAlignment(read);
        }
        else if(!read.getReadPairedFlag()) {
          writer.addAlignment(read);
        }
        else {
          String readName = read.getReadName();
          SAMRecord first = pairs.remove(readName);
          if(first == null) {
            pairs.put(readName, read);
            if(maxCachedReads < pairs.size()) 
              maxCachedReads = pairs.size();
          }
          else {
            final SAMRecord read1 = read.getFirstOfPairFlag() ? read : first;
            final SAMRecord read2 = read.getFirstOfPairFlag() ? first : read;
            SamPairUtil.setMateInfo(read1, read2, header);
            writer.addAlignment(read1);
            writer.addAlignment(read2);
          }
        }
      }
    }
    finally {
      try {
        samReader.close();
      } catch (IOException e) {
      }
      if(writer != null)
        writer.close();
    }

    System.err.println("Max cached reads: " + maxCachedReads);
    return 0;
  }
}
