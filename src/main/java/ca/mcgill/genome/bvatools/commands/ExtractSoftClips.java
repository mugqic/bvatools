/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import htsjdk.samtools.Defaults;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMUtils;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Deque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.io.input.CountingInputStream;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.extractSoftClip.PotentialSoftClip;
import ca.mcgill.genome.bvatools.extractSoftClip.ProcessSoftClip;
import ca.mcgill.genome.bvatools.extractSoftClip.SoftClipExtractor;
import ca.mcgill.genome.bvatools.extractSoftClip.SoftClippedRead;
import ca.mcgill.genome.bvatools.util.MemoryUtils;

/*
 * Dans l'id�ale :
 - ajouter un nombre minimum de nucl�otide pour valider le softclip
 - j'aimerai connaitre le nombre exact de softclip (avec le nombre minimum de nucl�otide et au total) pour chaque position
 avec le nom des reads (un peu comme crest, le format peut �tre diff�rent).
 - j'aimerai avoir en plus (en sortie) un bam file qui contient tous paired-end softclip�es ou a d�faut tous les reads softclip�es
 - Avoir une option pour activer (ou non) la s�lection des softclip uniquement au extr�mit� des reads.
 - Si on peut connaitre le nombre de paired-end "one anchor mappable" dans une fen�tre n bp autour du softclip,
 �a peut �tre int�ressant (sinon je le ferai apr�s).

 Si je vois autres choses je te pr�viens au plus vite


 La sortie crest :
 2 fichiers
 - pour chaque position le nombre de softclip et le cov
 Guillimin : /lb/project/mugqic/projects/DeNovoAssembly_PRJBFX_323/unmapAssembly/dev/DeNovo/HBV/75/SClipTum/T75.sorted.dup.bam.17.cover
 17    1    -    5    5
 17    928    +    1    33
 17    5324    +    1    22
 17    6666    +    1    26
 - pour chaque read softclip� la position et la s�quence softclip�
 guillimin : /lb/project/mugqic/projects/DeNovoAssembly_PRJBFX_323/unmapAssembly/dev/DeNovo/HBV/75/SClipTum/T75.sorted.dup.bam.17.sclip.txt
 17    1    -    ERR093664.19412552    GGAGAAGGGGACAAGAGGTCCCCAACTTCTTTGCA    FFF=FEEAEEDEDEEADD@DFFFEDFEEEEFBFDA
 17    1    -    ERR093664.89359331    GTCCCCAACTTCTTTGCA    :>B5=AA?A?7*68:D=:
 17    1    -    ERR093667.12999030    AGGGAGGTCATGTGCAGGCTGGAGAAGGGGACAAGAGGTCCCCAACTTCTTTGCA    HHHHHHHHHHHHHHHHHHHEHHAFFGDGIG?@B;EDDD4AEGF<FBCFFCGIGFB
 17    1    -    ERR093669.18492083    CCCAACTTCTTTGCA    HHHHHHHHHHHHHHH
 17    1    -    ERR093670.34220068    AGAGGTCCCCAACTTCTTTGCA    7@>77;>B=>><?>>AC?CAAD
 */
public class ExtractSoftClips extends DefaultTool {
  private boolean printElapsed = false;
  private boolean outputRemaining = false;
  private boolean outputTranslocated = false;
  // private IndexedFastaSequenceFile refSequence = null;
  private byte threads = 2;
  private int maxReadLength = 1000;
  private int minSCCount = 2;
  private int minSCLength = 5;
  private int minMappingQuality = 10;
  private int flank = 0;
  private File bamFile = null;
  private String prefix = null;

  @Override
  public String getCmdName() {
    return "extractsclip";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--bam                  BAM file");
    System.out.println("\t--prefix               Output prefix for files.");
    System.out.println("\t--outputRemaining      Output in a separate BAM reads that are not SoftClipped or aren't oneEndMap close to Soft clips.");
    System.out.println("\t--outputTranslocated   Output in a separate BAM paired-end mapped in 2 different chromosome and count him in scPositions file.");
    System.out.println("\t--threads              Threads to use. Has to be > 2. (default: " + threads + ")");
    System.out.println("\t--printElapsed         Prints elapsed percentage");
    // System.out.println("\t--ref           Reference to add refAllele");
    System.out.println("\t--flank                Peer into this distance in flanks for Unmapped");
    System.out.println("\t--maxReadLength        This value has to be equal or bigger than the longest read. (default: " + maxReadLength + ")");
    System.out.println("\t--minSCCount           Only output softclips with this many reads (default: " + minSCCount + ")");
    System.out.println("\t--minSCLength          Only use softclips at least this long (default: " + minSCCount + ")");
    System.out.println("\t--minMappingQuality    Only use reads with a mapping quality higher than this value (default: " + minMappingQuality + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--bam")) {
        bamFile = new File(arguments.pop());
      } else if (arg.equals("--threads")) {
        threads = Byte.parseByte(arguments.pop());
      } else if (arg.equals("--prefix")) {
        prefix = arguments.pop();
      } else if (arg.equals("--flank")) {
        flank = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--maxReadLength")) {
        maxReadLength = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--printElapsed")) {
        printElapsed = true;
      } else if (arg.equals("--outputRemaining")) {
        outputRemaining = true;
      } else if (arg.equals("--outputTranslocated")) {
        outputTranslocated = true;
      } else if (arg.equals("--minSCCount")) {
        minSCCount = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minSCLength")) {
        minSCLength = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minMappingQuality")) {
        minMappingQuality = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (bamFile == null) {
      printUsage("bam not set");
      return 1;
    }
    if (threads < 2) {
      printUsage("threads has to be >2. You gave " + threads);
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    extractSClip(bamFile, prefix);
    return 0;
  }

  private void extractSClip(File bam, String prefix) {
    ExecutorService sclipExecutor = Executors.newFixedThreadPool(threads - 1, new ThreadFactory() {
      @Override
      public Thread newThread(Runnable runnable) {
        Thread thread = Executors.defaultThreadFactory().newThread(runnable);
        thread.setDaemon(true);
        return thread;
      }
    });

    Queue<PotentialSoftClip> softClips = new LinkedBlockingQueue<PotentialSoftClip>();
    Queue<PotentialSoftClip> softClipsToProcess = new LinkedBlockingQueue<PotentialSoftClip>();
    SoftClipExtractor scExtractor = new SoftClipExtractor(softClipsToProcess, softClips, bam, minMappingQuality);
    scExtractor.setPrintElapsed(printElapsed);
    Thread mainThread = new Thread(new ThreadGroup("SCProcessor"), scExtractor, "SoftClipExtractor");
    mainThread.setDaemon(true);
    mainThread.start();

    ProcessSoftClip scProcessors[] = new ProcessSoftClip[threads - 1];
    Thread workers[] = new Thread[threads - 1];
    ThreadGroup scProcessorsGrp = new ThreadGroup("SoftClipProcessors");
    for (int idx = 0; idx < threads - 1; idx++) {
      scProcessors[idx] = new ProcessSoftClip(bam, softClipsToProcess, flank, minMappingQuality, maxReadLength);
      workers[idx] = new Thread(scProcessorsGrp, scProcessors[idx]);
      workers[idx].setDaemon(true);
      workers[idx].start();
    }

    PrintWriter scSeqwriter = null;
    PrintWriter scPositionWriter = null;
    try {
      scSeqwriter = new PrintWriter(new File(prefix + ".scSequences.txt"), "ASCII");
      scPositionWriter = new PrintWriter(new File(prefix + ".scPositions.txt"), "ASCII");
      printSoftClipDetailsHeader(scPositionWriter, scSeqwriter);
      Set<String> readsScToPrint = new HashSet<String>();
      Set<String> readsTransToPrint = new HashSet<String>();
      while (mainThread.isAlive()) {
        PotentialSoftClip processedSoftClip = softClips.poll();
        if (processedSoftClip != null) {
          while (!processedSoftClip.isProcessed()) {
            Thread.sleep(1000);
          }
          printSoftClipDetails(scPositionWriter, scSeqwriter, processedSoftClip);
          readsScToPrint.addAll(processedSoftClip.getOneEndAligneds());
          readsTransToPrint.addAll(processedSoftClip.getTranslocatedRead());
          for (SoftClippedRead reads : processedSoftClip.getReadsToOutput()) {
            readsScToPrint.add(reads.getReadName());
          }
        } else {
          Thread.sleep(1000);
        }
      }
      mainThread = null;

      // Finish up.
      long totalNbSC = scExtractor.getNbSoftClipsFound();
      scExtractor = null;
      long prevPCT = 0;
      if (printElapsed) {
        System.out.println("Processing remaining SC...");
      }
      while (softClips.size() > 0) {
        if (printElapsed) {
          long pct = (totalNbSC - softClips.size()) * 100l / totalNbSC;
          if (prevPCT != pct) {
            prevPCT = pct;
            System.out.print("\r" + prevPCT + "%");
          }
        }
        PotentialSoftClip potentialSoftClip = softClips.remove();
        while (!potentialSoftClip.isProcessed()) {
          Thread.sleep(1000);
        }
        printSoftClipDetails(scPositionWriter, scSeqwriter, potentialSoftClip);
        readsScToPrint.addAll(potentialSoftClip.getOneEndAligneds());
        readsTransToPrint.addAll(potentialSoftClip.getTranslocatedRead());
        for (SoftClippedRead reads : potentialSoftClip.getReadsToOutput()) {
          readsScToPrint.add(reads.getReadName());
        }
      }
      scSeqwriter.close();
      scPositionWriter.close();
      for (int idx = 0; idx < threads - 1; idx++) {
        scProcessors[idx].stopProcessing();
        scProcessors[idx] = null;
      }

      writeSCBam(bam, prefix, readsScToPrint, readsTransToPrint);
      if (printElapsed) {
        System.out.println("\nDone processing remaining SC.");
        System.out.println(MemoryUtils.printGC());
      }
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      sclipExecutor.shutdownNow();
    }
  }

  private void printSoftClipDetailsHeader(PrintWriter scPositionWriter, PrintWriter scSeqwriter) {
    scSeqwriter.println("Chr\tPos\tstrand\tReadName\tSoftClippedSequence\tSoftClippedQuality");
    if (outputTranslocated)
      scPositionWriter.println("Chr\tPos\tTotalDepth\tNbSoftClips\tNbPassedFilterSoftClips\tNbOneAnchoredEnd\tNbTranslocatedRead");
    else
      scPositionWriter.println("Chr\tPos\tTotalDepth\tNbSoftClips\tNbPassedFilterSoftClips\tNbOneAnchoredEnd");
  }

  private void printSoftClipDetails(PrintWriter scPositionWriter, PrintWriter scSeqwriter, PotentialSoftClip processedSoftClip) {
    if (processedSoftClip.getNbReads() < minSCCount)
      return;

    int passedFilter = 0;
    for (SoftClippedRead read : processedSoftClip.getReadsToOutput()) {
      if (read.getBases().length < minSCLength)
        continue;
      passedFilter++;
      scSeqwriter.print(processedSoftClip.getChr());
      scSeqwriter.print('\t');
      scSeqwriter.print(processedSoftClip.getPos());
      scSeqwriter.print('\t');
      if (read.getFirstOfPairFlag())
	scSeqwriter.print("+");
      else
	scSeqwriter.print("-");
      scSeqwriter.print('\t');
      scSeqwriter.print(read.getReadName());
      scSeqwriter.print('\t');
      for (byte b : read.getBases())
        scSeqwriter.print((char) b);
      scSeqwriter.print('\t');
      scSeqwriter.print(SAMUtils.phredToFastq(read.getQualities()));
      scSeqwriter.println();
    }

    scPositionWriter.print(processedSoftClip.getChr());
    scPositionWriter.print('\t');
    scPositionWriter.print(processedSoftClip.getPos());
    scPositionWriter.print('\t');
    scPositionWriter.print(processedSoftClip.getNbReads());
    scPositionWriter.print('\t');
    scPositionWriter.print(processedSoftClip.getReadsToOutput().size());
    scPositionWriter.print('\t');
    scPositionWriter.print(passedFilter);
    scPositionWriter.print('\t');
    if (outputTranslocated) {
      scPositionWriter.print(processedSoftClip.getNbOEA());
      scPositionWriter.print('\t');
      scPositionWriter.println(processedSoftClip.getNbTranslocatedRead());
    } else
      scPositionWriter.println(processedSoftClip.getNbOEA());
  }

  private void writeSCBam(File bam, String outputPrefix, Set<String> readsScToExtract, Set<String> readsTransToExtract) {
    SAMFileWriter writer = null;
    SAMFileWriter writerOther = null;
    SAMFileWriter writerTrans = null;
    SamReader reader = null;
    if (printElapsed)
      System.out.println("Writing outputs...");

    try {
      long fileSize = bam.length();
      CountingInputStream countingStream = new CountingInputStream(new FileInputStream(bam));
      
      SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
      reader = samReaderFactory.open(SamInputResource.of(new BufferedInputStream(countingStream, Defaults.BUFFER_SIZE)));
      SAMFileHeader header = reader.getFileHeader();

      writer = new SAMFileWriterFactory().makeSAMOrBAMWriter(header.clone(), true, new File(outputPrefix + ".sc.bam"));
      if (outputRemaining) {
        writerOther = new SAMFileWriterFactory().makeSAMOrBAMWriter(header.clone(), true, new File(outputPrefix + ".scOthers.bam"));
      }
      if (outputTranslocated) {
        writerTrans = new SAMFileWriterFactory().makeSAMOrBAMWriter(header.clone(), true, new File(outputPrefix + ".scTrans.bam"));
      }
      long prevPCT = 0;
      for (SAMRecord record : reader) {
        if (printElapsed) {
          long pct = countingStream.getByteCount() * 100l / fileSize;
          if (prevPCT != pct) {
            prevPCT = pct;
            System.out.print("\r" + prevPCT + "%");
          }
        }

        if (readsScToExtract.contains(record.getReadName())) {
          writer.addAlignment(record);
        } else {
          if (outputTranslocated && readsTransToExtract.contains(record.getReadName())) {
            writerTrans.addAlignment(record);
          } else if (writerOther != null) {
            writerOther.addAlignment(record);
          }
        }
      }
      if (printElapsed)
        System.out.println("\rDone Writing ourputs.");

    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } finally {
      if (writer != null) {
        writer.close();
      }
      if (writerOther != null) {
        writerOther.close();
      }
      if (writerTrans != null) {
        writerTrans.close();
      }
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
