/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import gnu.trove.map.hash.TObjectDoubleHashMap;
import htsjdk.samtools.Defaults;
import htsjdk.samtools.util.CloseableIterator;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.tribble.bed.BEDCodec;
import htsjdk.tribble.bed.BEDFeature;
import htsjdk.tribble.readers.LineIterator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.parsers.CachedReferenceSequenceFile;
import ca.mcgill.genome.bvatools.util.AlleleCounts;
import ca.mcgill.genome.bvatools.util.PairMappings;
import ca.mcgill.genome.bvatools.util.SampleAlleleCounts;
import ca.mcgill.genome.bvatools.util.SampleAlleleCounts.IndelAtPosition;
import ca.mcgill.genome.bvatools.util.Variant;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.interval.ChromosomeSimpleName;
import ca.mcgill.mcb.pcingola.interval.Genome;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.interval.Markers;
import ca.mcgill.mcb.pcingola.interval.tree.IntervalForest;
import ca.mcgill.mcb.pcingola.vcf.EffFormatVersion;
import ca.mcgill.mcb.pcingola.vcf.VcfEffect;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class VariantValidator extends DefaultTool {
  private double hetThreshold = 0.20;
  private int minDepth = 10;
  private double minQual = 0;
  private EffFormatVersion snpEffFormat = EffFormatVersion.FORMAT_EFF_3;
  private List<File> vcfs = new ArrayList<File>();
  private File vcfsFile = null;
  private File tsvFile = null;
  private File bed = null;
  private CachedReferenceSequenceFile fastaRef = null;
  private Map<String, File> validationSamplesBAM = null;
  private Map<String, File> referenceSamplesBAM = null;
  private PairMappings sampleMappings = null;
  private File loadVariants = null;
  private File saveVariants = null;

  @Override
  public String getCmdName() {
    return "validator";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcfsFile           File containing a list of VCF files to get positions to validate from");
    System.out.println("\t--vcf                VCF to get positions to validate from");
    System.out.println("\t--tsv                File containing sites to validate.");
    System.out.println("\t                     Format:");
    System.out.println("\t                     Sample<TAB>Chr<TAB>Pos<TAB>Ref<TAB>Alt<TAB>GeneName (optional)");
    System.out.println("\t--bed                BED file to filter reference VCFs");
    System.out.println("\t--ref                Indexed reference genome");
    System.out.println("\t--validationbams     Samples BAM from the validation");
    System.out.println("\t--referencebams      Samples BAM from another run");
    System.out.println("\t--pairs              Pairs file <needed with vtsv>");
    System.out.println("\t--het                At which threshold do we call a valid het. (default: " + hetThreshold + ")");
    System.out.println("\t--minDepth           Minimum depth on which to call a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minQual            Minimum variant quality to use to compute stats. (default: " + minQual + ")");
    System.out.println("\t--saveVariants       Cache usable variants for quicker loading.");
    System.out.println("\t--loadVariants       Load usable variants cache.");
    System.out.println("\t--snpEffFormat       VCF snpEff format. (default: " + snpEffFormat + ")");
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcfs.add(new File(arguments.pop()));
      } else if (arg.equals("--vcfsFile")) {
        vcfsFile = new File(arguments.pop());
      } else if (arg.equals("--tsv")) {
        tsvFile = new File(arguments.pop());
      } else if (arg.equals("--bed")) {
        bed = new File(arguments.pop());
      } else if (arg.equals("--ref")) {
        try {
          fastaRef = new CachedReferenceSequenceFile(new File(arguments.pop()));
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      } else if (arg.equals("--pairs")) {
        sampleMappings = PairMappings.parsePairsFile(new File(arguments.pop()));
      } else if (arg.equals("--validationbams")) {
        validationSamplesBAM = VariantAlleleFrequency.parseSamplesFile(new File(arguments.pop()));
      } else if (arg.equals("--referencebams")) {
        referenceSamplesBAM = VariantAlleleFrequency.parseSamplesFile(new File(arguments.pop()));
      } else if (arg.equals("--het")) {
        hetThreshold = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--snpEffFormat")) {
        snpEffFormat = Enum.valueOf(EffFormatVersion.class, arguments.pop());
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minQual")) {
        minQual = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--saveVariants")) {
        saveVariants = new File(arguments.pop());
      } else if (arg.equals("--loadVariants")) {
        loadVariants = new File(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    try {
      if (vcfsFile != null) {
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(vcfsFile)));
        while (true) {
          String line = reader.readLine();
          if (line == null)
            break;
          if (line.startsWith("#"))
            continue;
          vcfs.add(new File(line));
        }
        reader.close();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    if(tsvFile != null && vcfs.size() >0) {
      printUsage("VCFs and TSV cannot be used together.");
      return 1;
    }

    if(tsvFile == null && vcfs.size() == 0) {
      printUsage("Neither VCFs nor TSV were given");
      return 1;
    }

    if (validationSamplesBAM == null) {
      printUsage("bams not set");
      return 1;
    }

    return 0;
  }

  @Override
  public int run() {
    List<Variant> variants = null;
    Map<String, SampleAlleleCounts> samplesBAMAlleleCounter = new HashMap<String, SampleAlleleCounts>();

    if (validationSamplesBAM != null) {
      for (String sampleName : validationSamplesBAM.keySet()) {
        File refSampleBAM = null;
        if (referenceSamplesBAM != null)
          refSampleBAM = referenceSamplesBAM.get(sampleName);
        samplesBAMAlleleCounter.put(sampleName, new SampleAlleleCounts(fastaRef, validationSamplesBAM.get(sampleName), refSampleBAM, 0, 0));
      }
    }

    if (loadVariants != null) {
      variants = readVariants(loadVariants);
    } else if (vcfs.size() > 0) {
      variants = parseVariants(vcfs, bed, validationSamplesBAM.keySet());
    } else if (tsvFile != null) {
      variants = parseVariants(tsvFile, validationSamplesBAM.keySet());
    }

    if (saveVariants != null) {
      writeVariants(variants, saveVariants);
    }

    validateVariants(sampleMappings, hetThreshold, variants, samplesBAMAlleleCounter);
    for (SampleAlleleCounts counts : samplesBAMAlleleCounter.values()) {
      try {
        counts.close();
      } catch (IOException e) {
        // ignore
      }
    }
    return 0;
  }

  private void writeVariants(List<Variant> variants, File saveVariants) {
    PrintWriter writer;
    try {
      writer = new PrintWriter(saveVariants, "ASCII");

      for (Variant variant : variants) {
        String dump = variant.write();
        writer.println(dump);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    writer.close();
  }

  private List<Variant> readVariants(File loadVariants) {
    List<Variant> variants = new ArrayList<Variant>();
    BufferedReader reader = null;
    try {
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(loadVariants), "ASCII"));
      while (true) {
        String line = reader.readLine();
        if (line == null)
          break;
        if (line.startsWith("#"))
          continue;

        variants.add(Variant.read(line));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        e.printStackTrace();
        // Do nothing
      }
    }
    return variants;
  }

  private IndelAtPosition getIndelAtPositionType(Variant variant) {
    IndelAtPosition indelAtPos = IndelAtPosition.NONE;
    int indelLength = variant.getRefAllele().length();
    // If it is an indel give the ref allele length
    for (String altAllele : variant.getAltAlleles()) {
      if (altAllele.length() > indelLength) {
        if (indelAtPos == IndelAtPosition.NONE) {
          indelAtPos = IndelAtPosition.INS;
        } else if (indelAtPos == IndelAtPosition.DEL) {
          indelAtPos = IndelAtPosition.BOTH;
        }
      } else if (altAllele.length() < indelLength) {
        if (indelAtPos == IndelAtPosition.NONE) {
          indelAtPos = IndelAtPosition.DEL;
        } else if (indelAtPos == IndelAtPosition.INS) {
          indelAtPos = IndelAtPosition.BOTH;
        }
      } else if (indelLength > 1 && altAllele.length() == indelLength) {
        throw new RuntimeException("MNPs are not handled");
      }
    }
    return indelAtPos;
  }

  private void validateVariants(PairMappings sampleMapping, double hetThreshold, List<Variant> variants, Map<String, SampleAlleleCounts> samplesBAMAlleleCounter) {
    long totalTests = 0;
    long totalSampleVariantTests = 0;
    long totalPassed = 0;
    long totalSampleVariantPassed = 0;
    long totalNonIndelTests = 0;
    long totalNonIndelPassed = 0;
    long totalFP = 0;
    long totalFN = 0;
    CountedData countedData = new CountedData();

    System.out
        .println("Sample\tChr\tPos\tGene\tVariant\tSampleAlleles\tExtraFields\tReferenceBAMAlleleCounts\tAlleleCounts\tReferenceAlleleFractions\tAlleleFractions\tTruth\tVerdict\tMessage");
    for (Variant variant : variants) {
      for (Variant.SampleGenotype sampleGenotype : variant.getSampleGenotypes()) {
        SampleAlleleCounts sampleAlleleCounts = samplesBAMAlleleCounter.get(sampleGenotype.getSampleName());
        if (sampleAlleleCounts == null)
          continue;

        IndelAtPosition indelAtPos = getIndelAtPositionType(variant);
        int indelLength = variant.getRefAllele().length();

        Callable<AlleleCounts> callableCount = sampleAlleleCounts.getCallableAlleleCountsComputer(variant.getChromosome(), variant.getPosition(), indelAtPos,
            indelLength);
        AlleleCounts baseCounts;
        try {
          baseCounts = callableCount.call();
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
        StringBuilder valSB = baseCounts.format(false);
        StringBuilder refSB;

        TObjectDoubleHashMap<String> valFraction = AlleleCounts.computeAlleleFractions(baseCounts.getBamCounts());
        StringBuilder valFracSB = AlleleCounts.formatFractionCounts(valFraction);
        StringBuilder refFracSB;

        if (baseCounts.getReferenceBAMCounts() != null) {
          refSB = baseCounts.format(true);
          TObjectDoubleHashMap<String> refFraction = AlleleCounts.computeAlleleFractions(baseCounts.getReferenceBAMCounts());
          refFracSB = AlleleCounts.formatFractionCounts(refFraction);
        } else {
          refSB = new StringBuilder();
          refFracSB = new StringBuilder();
        }

        int depth = baseCounts.getDepth();

        if (sampleMapping != null) {
          String sampleName = sampleMapping.getSampleFromNormal(sampleGenotype.getSampleName());
          if (sampleName != null) {
            System.out.print(sampleName + "-N");
          } else {
            sampleName = sampleMapping.getSampleFromTumor(sampleGenotype.getSampleName());
            if (sampleName != null) {
              System.out.print(sampleName + "-T");
            } else {
              System.out.print(sampleGenotype.getSampleName());
            }
          }
        } else {
          System.out.print(sampleGenotype.getSampleName());
        }
        System.out.print('\t');
        System.out.print(variant.getChromosome());
        System.out.print('\t');
        System.out.print(variant.getPosition());
        System.out.print('\t');
        System.out.print(variant.getGene());
        System.out.print('\t');
        System.out.print(variant.getRefAllele());
        System.out.print(" / ");
        System.out.print(StringUtils.join(variant.getAltAlleles(), ','));
        System.out.print('\t');

        if (sampleGenotype.getAlleleA() == null) {
          System.out.print("NA");
        } else {
          System.out.print(sampleGenotype.getAlleleA());
          System.out.print(" / ");
          System.out.print(sampleGenotype.getAlleleB());
        }
        System.out.print('\t');
        System.out.print(sampleGenotype.getExtra());
        System.out.print('\t');
        System.out.print(refSB);
        System.out.print('\t');
        System.out.print(valSB);
        System.out.print('\t');
        System.out.print(refFracSB);
        System.out.print('\t');
        System.out.print(valFracSB);
        System.out.print('\t');

        if (depth < minDepth) {
          countedData.nUncovered++;
          System.out.print('\t');
          System.out.print(TestResult.TestOutcome.NA);
          System.out.print("IGNORED");
          System.out.print('\t');
          System.out.print("Depth too low");
        }
        else {
          if (sampleGenotype.getAlleleA() == null) {
            System.out.print('\t');
            System.out.print(TestResult.TestOutcome.NA);
            TestResult tstResult = testVariant(hetThreshold, variant, variant.getRefAllele(), variant.getAltAlleles().get(0), valFraction);
            if (tstResult.getOutcome() == TestResult.TestOutcome.TP) {
              countedData.nNoStatusCalledAlt++;
              System.out.print("Would have a HET variant");
            } else {
              tstResult = testVariant(hetThreshold, variant, variant.getAltAlleles().get(0), variant.getAltAlleles().get(0), valFraction);
              if (tstResult.getOutcome() == TestResult.TestOutcome.TP) {
                countedData.nNoStatusCalledAlt++;
                System.out.print("Would have a HOM variant");
              } else {
                tstResult = testVariant(hetThreshold, variant, variant.getRefAllele(), variant.getRefAllele(), valFraction);
                if (tstResult.getOutcome() == TestResult.TestOutcome.TN) {
                  countedData.nNoStatusCalledRef++;
                  System.out.print("Would have a HOM ref");
                }
                else {
                  countedData.nNoStatusNotCalled++;
                  System.out.print("Might not have a variant");
                }
              }
            }
            System.out.print('\t');
            System.out.print("Not previously sequenced");
          } else {
            totalTests++;
            if (!variant.isIndel()) {
              totalNonIndelTests++;
            }
  
            if (sampleGenotype.isVariant(variant.getRefAllele())) {
              totalSampleVariantTests++;
            }
  
            TestResult tstResult = testVariant(hetThreshold, variant, sampleGenotype.getAlleleA(), sampleGenotype.getAlleleB(), valFraction);
            System.out.print(tstResult.getOutcome());
            System.out.print('\t');
            if (tstResult.getOutcome() == TestResult.TestOutcome.TP || tstResult.getOutcome() == TestResult.TestOutcome.TN) {
              if(tstResult.getOutcome() == TestResult.TestOutcome.TP)
                countedData.nAltCalledAlt++;
              else
                countedData.nRefCalledRef++;
              System.out.print("PASSED");
              System.out.print('\t');
              totalPassed++;
              if (!variant.isIndel()) {
                totalNonIndelPassed++;
              }
              if (sampleGenotype.isVariant(variant.getRefAllele())) {
                totalSampleVariantPassed++;
              }
            } else if (tstResult.getOutcome() == TestResult.TestOutcome.FP) {
              countedData.nAltCalledRef++;
              System.out.print("FAILED-FP");
              System.out.print('\t');
              System.out.print(tstResult.getMessage());
              if (!variant.isIndel()) {
                totalFP++;
              }
            } else if (tstResult.getOutcome() == TestResult.TestOutcome.FN) {
              countedData.nRefCalledAlt++;
              System.out.print("FAILED-FN");
              System.out.print('\t');
              System.out.print(tstResult.getMessage());
              if (!variant.isIndel()) {
                totalFN++;
              }
            } else if (tstResult.getOutcome() == TestResult.TestOutcome.NA) {
              countedData.nNotConfidentCalls++;
              System.out.print("Unknown outcome");
              System.out.print('\t');
              System.out.print(tstResult.getMessage());
            } else {
              throw new RuntimeException("Unknown outcome: " + tstResult.getOutcome());
            }
          }
        }
        System.out.println();
      }
    }
    DecimalFormat formatter = new DecimalFormat("#0.00");
    System.out.println("Sample Variant Passed:  " + formatter.format(totalSampleVariantPassed * 100d / totalSampleVariantTests) + "%");
    System.out.println("Passed:                 " + formatter.format(totalPassed * 100d / totalTests) + "%");
    System.out.println("Non-indel Passed:       " + formatter.format(totalNonIndelPassed * 100d / totalNonIndelTests) + "%");
    System.out.println("FP percent:             " + formatter.format(totalFP * 100d / totalNonIndelTests) + "%");
    System.out.println("FN percent:             " + formatter.format(totalFN * 100d / totalNonIndelTests) + "%");
    
    double ppv = 100 * ((double) countedData.nAltCalledAlt /( countedData.nAltCalledAlt + countedData.nRefCalledAlt));
    double npv = 100 * ((double) countedData.nRefCalledRef /( countedData.nRefCalledRef + countedData.nAltCalledRef));
    double sensitivity = 100 * ((double) countedData.nAltCalledAlt /( countedData.nAltCalledAlt + countedData.nAltCalledRef));
    double specificity = (countedData.nRefCalledRef + countedData.nRefCalledAlt > 0) ? 100 * ((double) countedData.nRefCalledRef /( countedData.nRefCalledRef + countedData.nRefCalledAlt)) : 100;
    System.out.println(
                String.format("Resulting Truth Table Output\n\n" +
                              "------------------------------------------------------------------\n" +
                              "\t\t|\tALT\t|\tREF\t|\tNo Status\n"  +
                              "------------------------------------------------------------------\n" +
                              "called alt\t|\t%d\t|\t%d\t|\t%d\n" +
                              "called ref\t|\t%d\t|\t%d\t|\t%d\n" +
                              "not called\t|\t%d\t|\t%d\t|\t%d\n" +
                              "------------------------------------------------------------------\n" +
                              "positive predictive value: %f%%\n" +
                              "negative predictive value: %f%%\n" +
                              "------------------------------------------------------------------\n" +
                              "sensitivity: %f%%\n" +
                              "specificity: %f%%\n" +
                              "------------------------------------------------------------------\n" +
                              "not confident: %d\n" +
                              "not covered: %d\n" +
                              "------------------------------------------------------------------\n",
                              countedData.nAltCalledAlt, countedData.nRefCalledAlt, countedData.nNoStatusCalledAlt, countedData.nAltCalledRef, countedData.nRefCalledRef, countedData.nNoStatusCalledRef, countedData.nAltNotCalled, countedData.nRefNotCalled, countedData.nNoStatusNotCalled, ppv, npv, sensitivity, specificity, countedData.nNotConfidentCalls, countedData.nUncovered));
    
    
  }

  private TestResult testVariant(double hetThreshold, Variant variant, String alleleA, String alleleB, TObjectDoubleHashMap<String> baseFraction) {
    TestResult.TestOutcome badOutcome;
    TestResult result = new TestResult();
    if (variant.getRefAllele().equals(alleleA) && variant.getRefAllele().equals(alleleB)) {
      badOutcome = TestResult.TestOutcome.FN;
      result.setOutcome(TestResult.TestOutcome.TN);
    } else {
      badOutcome = TestResult.TestOutcome.FP;
      result.setOutcome(TestResult.TestOutcome.TP);
    }

    List<String> keptBases = new ArrayList<String>();
    for (String base : baseFraction.keySet()) {
      if (baseFraction.get(base) > hetThreshold) {
        if (keptBases.size() == 2) {
          result.setMessage("Too many bases above het threshold [" + hetThreshold + "]");
          result.setOutcome(TestResult.TestOutcome.NA);
        } else {
          keptBases.add(base.toString());
        }
      }
    }

    if (!keptBases.contains(alleleA)) {
      result.setMessage("Missing allele '" + alleleA + "'");
      result.setOutcome(badOutcome);
    }
    if (!keptBases.contains(alleleB)) {
      result.setMessage("Missing allele '" + alleleB + "'");
      result.setOutcome(badOutcome);
    }

    keptBases.remove(alleleA);
    keptBases.remove(alleleB);

    if (keptBases.size() != 0) {
      result.setMessage("Spurrious allele " + StringUtils.join(keptBases, '/'));
      result.setOutcome(badOutcome);
    }

    return result;
  }


  public IntervalForest parseBED(File bed) {
    IntervalForest forest = new IntervalForest();

    AbstractFeatureReader<BEDFeature, LineIterator> featureReader = null;
    try {
      featureReader = AbstractFeatureReader.getFeatureReader(bed.getAbsolutePath(), new BEDCodec(), false);
      CloseableIterator<BEDFeature> iter = featureReader.iterator();
      Genome genome = new Genome();
      while(iter.hasNext()) {
        BEDFeature feature = iter.next();
        String chr = feature.getChr();
        chr = ChromosomeSimpleName.get(chr);
  
        forest.add(new Marker(genome.getOrCreateChromosome(chr), feature.getStart(), feature.getEnd(), false, ""));
      }
      iter.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      if(featureReader != null)
        try {
          featureReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
    }
    forest.build();
    return forest;
  }

  private List<Variant> parseVariants(List<File> inputVCFs, File bed, Set<String> samplesWithBAM) {
    Map<String, Variant> retVal = new HashMap<String, Variant>();

    IntervalForest forestOfRegions = parseBED(bed);

    Set<String> samplesWithGT = new HashSet<String>();
    try {
      for (File vcf : inputVCFs) {
        System.err.println("Reading: " + vcf);
        String vcfName = vcf.toString();
        BufferedReader in;
        if (vcfName.endsWith(".gz")) {
          in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(vcf), Defaults.NON_ZERO_BUFFER_SIZE), "ASCII"));
        } else {
          in = new BufferedReader(new InputStreamReader(new FileInputStream(vcf), "ASCII"), Defaults.NON_ZERO_BUFFER_SIZE);
        }
        VcfFileIterator vcfFile = new VcfFileIterator(in);
        VcfHeader header = vcfFile.readHeader();
        samplesWithGT.add(header.getSampleNames().get(0));
        samplesWithGT.add(header.getSampleNames().get(1));
        for (VcfEntry vcfEntry : vcfFile) {
          if(vcfEntry.getQuality() < minQual)
            continue;
          String key = vcfEntry.getChromosomeName() + ':' + (vcfEntry.getStart() + 1);
          String extra = "QUAL=" +vcfEntry.getQuality()+ ":CLR=" + vcfEntry.getInfo("CLR");

          if (retVal.containsKey(key)) {
            Variant variant = retVal.get(key);
            if (!variant.getAltAlleles().get(0).equals(vcfEntry.getAlts()[0])) {
              System.err.println("Different alt alleles: " + key + " alt1: " + variant.getAltAlleles().get(0) + ' ' + vcfEntry.getAltsStr());
            }
            VcfGenotype normGT = vcfEntry.getVcfGenotypes().get(0);
            VcfGenotype tumorGT = vcfEntry.getVcfGenotypes().get(1);

            try {
              variant.addSampleGenotype(header.getSampleNames().get(0), normGT.getGenotype(0), normGT.getGenotype(1), extra);
              variant.addSampleGenotype(header.getSampleNames().get(1), tumorGT.getGenotype(0), tumorGT.getGenotype(1), extra);
            }
            catch(RuntimeException e) {
              System.err.println("Variant from: " + key + " ignored since it's added twice");
            }
          } else {
            Markers hits = forestOfRegions.query(vcfEntry);
            if (hits.size() > 0) {
              List<VcfEffect> effects = vcfEntry.parseEffects(snpEffFormat);
              String gene = null;
              for (VcfEffect effect : effects) {
                if (effect.getGeneName() != null) {
                  gene = effect.getGeneName();
                  break;
                }
              }
              
              Variant variant = new Variant(vcfEntry.getChromosomeName(), vcfEntry.getStart() + 1, vcfEntry.getRef(), vcfEntry.getAltsStr(), gene);
              variant.simplifyIndels();
              retVal.put(key, variant);

              VcfGenotype normGT = vcfEntry.getVcfGenotypes().get(0);
              VcfGenotype tumorGT = vcfEntry.getVcfGenotypes().get(1);
              variant.addSampleGenotype(header.getSampleNames().get(0), normGT.getGenotype(0), normGT.getGenotype(1), extra);
              variant.addSampleGenotype(header.getSampleNames().get(1), tumorGT.getGenotype(0), tumorGT.getGenotype(1), extra);
            }
          }
        }
        vcfFile.close();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    List<String> notPrevCalled = new ArrayList<String>();
    for (String sampleWithBAM : samplesWithBAM) {
      if (!samplesWithGT.contains(sampleWithBAM)) {
        notPrevCalled.add(sampleWithBAM);
      }
    }

    List<Variant> variants = new ArrayList<Variant>(retVal.values());
    for (Variant variant : variants) {
      for (String sample : samplesWithGT) {
        if (variant.getSampleToGenotype().get(sample) == null) {
          variant.addSampleGenotype(sample, variant.getRefAllele(), variant.getRefAllele(), "");
        }
      }
      for (String sample : notPrevCalled) {
        variant.addSampleGenotype(sample, null, null, "");
      }
    }

    return variants;
  }

  private List<Variant> parseVariants(File inputTSV, Set<String> samplesWithBAM) {
    Map<String, Variant> retVal = new HashMap<String, Variant>();

    IntervalForest forestOfRegions = parseBED(bed);

    Set<String> samplesWithGT = new HashSet<String>();
    try {
      System.err.println("Reading: " + inputTSV);
      String tsvName = inputTSV.toString();
      BufferedReader in;
      if (tsvName.endsWith(".gz")) {
        in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(inputTSV), Defaults.NON_ZERO_BUFFER_SIZE), "ASCII"));
      } else {
        in = new BufferedReader(new InputStreamReader(new FileInputStream(inputTSV), "ASCII"), Defaults.NON_ZERO_BUFFER_SIZE);
      }

      String header = in.readLine();
      String values[] = header.split("\t");
      int sampleIdx=-1;
      int chrIdx=-1;
      int posIdx=-1;
      int refIdx=-1;
      int altIdx=-1;
      int geneIdx=-1;
      for(int i=0; i < values.length; i++) {
        if(values[i].equalsIgnoreCase("sample")) {
          sampleIdx = i;
        }
        else if(values[i].equalsIgnoreCase("chr")) {
          chrIdx = i;
        }
        else if(values[i].equalsIgnoreCase("pos")) {
          posIdx = i;
        }
        else if(values[i].equalsIgnoreCase("ref")) {
          refIdx = i;
        }
        else if(values[i].equalsIgnoreCase("alt")) {
          altIdx = i;
        }
        else if(values[i].equalsIgnoreCase("gene")) {
          geneIdx = i;
        }

      }
      
      Genome genome = new Genome();
      String line;
      while (true) {
        line = in.readLine();
        if(line == null)
          break;
        values = line.split("\t");
        
        String sample = values[sampleIdx];
        String chr = values[chrIdx];
        int pos = Integer.parseInt(values[posIdx]);
        String ref = values[refIdx];
        String alt = values[altIdx];
        String gene = null;
        if(geneIdx != -1)
          gene = values[geneIdx];

        samplesWithGT.add(sample);
        
        String key = chr + ':' + pos;
        if (retVal.containsKey(key)) {
          Variant variant = retVal.get(key);
          if (!variant.getAltAlleles().get(0).equals(alt)) {
            System.err.println("Different alt alleles: " + key + " alt1: " + variant.getAltAlleles().get(0) + ' ' + alt);
          }

          try {
            variant.addSampleGenotype(sample, ref, alt, "");
          }
          catch(RuntimeException e) {
            System.err.println("Variant from: " + key + " ignored since it's added twice");
          }
        } else {
          Marker marker = new Marker(genome.getOrCreateChromosome(chr), pos, pos, false, key); 
          Markers hits = forestOfRegions.query(marker);
          if (hits.size() > 0) {
            Variant variant = new Variant(chr, pos, ref, alt, gene);
            variant.simplifyIndels();
            retVal.put(key, variant);

            variant.addSampleGenotype(sample, ref, alt, "");
          }
        }
      }
      in.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    List<String> notPrevCalled = new ArrayList<String>();
    for (String sampleWithBAM : samplesWithBAM) {
      if (!samplesWithGT.contains(sampleWithBAM)) {
        notPrevCalled.add(sampleWithBAM);
      }
    }

    List<Variant> variants = new ArrayList<Variant>(retVal.values());
    for (Variant variant : variants) {
      for (String sample : samplesWithGT) {
        if (variant.getSampleToGenotype().get(sample) == null) {
          variant.addSampleGenotype(sample, variant.getRefAllele(), variant.getRefAllele(), "");
        }
      }
      for (String sample : notPrevCalled) {
        variant.addSampleGenotype(sample, null, null, "");
      }
    }

    return variants;
  }
  
  private static class TestResult {
    public enum TestOutcome {
      FP, FN, TP, TN, NA
    };

    private TestOutcome outcome;
    private String message;

    public TestOutcome getOutcome() {
      return outcome;
    }

    public void setOutcome(TestOutcome outcome) {
      this.outcome = outcome;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }
  }
  
  public static class CountedData {
    private long nAltCalledAlt = 0L;
    private long nAltCalledRef = 0L;
    private long nAltNotCalled = 0L;
    private long nRefCalledAlt = 0L;
    private long nRefCalledRef = 0L;
    private long nRefNotCalled = 0L;
    private long nNoStatusCalledAlt = 0L;
    private long nNoStatusCalledRef = 0L;
    private long nNoStatusNotCalled = 0L;
    private long nNotConfidentCalls = 0L;
    private long nUncovered = 0L;
  }
}
