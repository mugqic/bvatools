/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.commands;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

import ca.mcgill.genome.bvatools.DefaultTool;
import ca.mcgill.genome.bvatools.maf.MAFWriter;
import ca.mcgill.genome.bvatools.parsers.GeneMapper;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.EffFormatVersion;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class VCF2MAF extends DefaultTool {
  public static final int NORMAL_VCF_IDX = 0;
  public static final int TUMOR_VCF_IDX = 1;

  private File vcfFile;
  private PrintStream output;
  private GeneMapper geneMapper;
  private int minGenotypeQual = 0;
  private int minDepth = 10;
  private double minQual = 10;
  private int minCLR = 45;
  private String tumorName = null;
  private EffFormatVersion snpEffFormat = EffFormatVersion.FORMAT_EFF_4;

  @Override
  public String getCmdName() {
    return "vcf2maf";
  }

  @Override
  public String getCmdUsage() {
    return super.getCmdUsage();
  }

  public void printUsage(String errMsg) {
    System.out.println(errMsg);
    printUsageHeader();
    System.out.println("\t--vcf             VCFs to load");
    System.out.println("\t--minDepth        Minimum depth on which to use a Variant. (default: " + minDepth + ")");
    System.out.println("\t--minGQ           Minimum tumor genotype quality. (default: " + minGenotypeQual + ")");
    System.out.println("\t--minQual         Minimum variant quality. (default: " + minQual + ")");
    System.out.println("\t--minCLR          Minimum CLR to call somatic. (default: " + minCLR + ")");
    System.out.println("\t--mapper          Genes to keep file. Use to filter for refseq");
    System.out.println("\t--output          Output file");
    System.out.println("\t--forceTumorName  Force Tumor sample name to this value.");
    System.out.println("\t--snpEffFormat    VCF snpEff format. (default: " + snpEffFormat + ")");
    System.out.println("\t                  Possible values: "+ Arrays.toString(EffFormatVersion.values()));
  }

  @Override
  public int parseArgs(Deque<String> arguments, Deque<String> unusedArgs) {
    while (!arguments.isEmpty()) {
      String arg = arguments.pop();
      if (arg.equals("--vcf")) {
        vcfFile = new File(arguments.pop());
      } else if (arg.equals("--output")) {
        if (arg.equals("-"))
          output = System.out;
        else
          try {
            output = new PrintStream(arguments.pop(), "ASCII");
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
      } else if (arg.equals("--mapper")) {
        geneMapper = new GeneMapper(new File(arguments.pop()));
      } else if (arg.equals("--forceTumorName")) {
        tumorName = arguments.pop();
      } else if (arg.equals("--snpEffFormat")) {
        snpEffFormat = Enum.valueOf(EffFormatVersion.class, arguments.pop());
      } else if (arg.equals("--minGQ")) {
        minGenotypeQual = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minQual")) {
        minQual = Double.parseDouble(arguments.pop());
      } else if (arg.equals("--minDepth")) {
        minDepth = Integer.parseInt(arguments.pop());
      } else if (arg.equals("--minCLR")) {
        minCLR = Integer.parseInt(arguments.pop());
      } else {
        unusedArgs.add(arg);
      }
    }

    if (vcfFile == null) {
      printUsage("You need to pass --vcf");
      return 1;
    }
    if (output == null) {
      printUsage("You need to pass --output");
      return 1;
    }
    
    return 0;
  }

  @Override
  public int run() {
    vcf2maf();

    return 0;
  }

  public void vcf2maf() {
    VcfFileIterator vcfParser = new VcfFileIterator(vcfFile.toString());
    VcfHeader header = vcfParser.readHeader();
    List<String> sampleNames = header.getSampleNames();
    String normalSample = sampleNames.get(NORMAL_VCF_IDX);
    String tumorSample = sampleNames.get(TUMOR_VCF_IDX);
    MAFWriter writer = null;
    try {
      writer = new MAFWriter(snpEffFormat, output);
      writer.setNormal(normalSample);
      if (tumorName != null)
        writer.setTumor(tumorName);
      else
        writer.setTumor(tumorSample);
      writer.setMapper(geneMapper);
      writer.writeHeader();
      for (VcfEntry vcfEntry : vcfParser) {
        VcfGenotype tumorGenotype = vcfEntry.getVcfGenotype(TUMOR_VCF_IDX);
        if (Gpr.parseIntSafe(tumorGenotype.get("GQ")) >= minGenotypeQual && tumorGenotype.getGenotypeCode() != 0 && vcfEntry.getQuality() >= minQual
            && Gpr.parseIntSafe(tumorGenotype.get("DP")) >= minDepth && vcfEntry.getInfoInt("CLR") >= minCLR)
          writer.write(vcfEntry);
      }
    } finally {
      if (writer != null) {
        try {
          writer.close();
        } catch (IOException e) {
          // Oh Well
          e.printStackTrace();
        }
      }
    }
  }

}
