/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.snpcall;


public class TagVariant {
  private final String chromosome;
  private final int position;
  private final byte ref;
  private final byte alts[];
  private int refReadCount = 0;
  private final int altsReadCount[];
  private int refTagCount = 0;
  private int highestCountPerTagRef = 0;
  private int lowestCountPerTagRef = 0;
  private int avgCountPerTagRef = 0;
  private int medCountPerTagRef = 0;
  private final int altsTagCount[];
  private final int highestCountPerTagAlts[];
  private final int lowestCountPerTagAlts[];
  private final int avgCountPerTagAlts[];
  private final int medCountPerTagAlts[];

  private int disagreeCount = 0;

  public TagVariant(String chromosome, int position, byte ref, byte[] alts) {
    super();
    this.chromosome = chromosome;
    this.position = position;
    this.ref = ref;
    this.alts = alts;

    altsReadCount = new int[alts.length];
    altsTagCount = new int[alts.length];
    highestCountPerTagAlts = new int[alts.length];
    lowestCountPerTagAlts = new int[alts.length];
    avgCountPerTagAlts = new int[alts.length];
    medCountPerTagAlts = new int[alts.length];
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getPosition() {
    return position;
  }

  public byte getRef() {
    return ref;
  }

  public byte[] getAlts() {
    return alts;
  }

  public int getRefReadCount() {
    return refReadCount;
  }

  public void setRefReadCount(int refReadCount) {
    this.refReadCount = refReadCount;
  }

  public int getRefTagCount() {
    return refTagCount;
  }

  public void setRefTagCount(int refTagCount) {
    this.refTagCount = refTagCount;
  }

  public int getHighestCountPerTagRef() {
    return highestCountPerTagRef;
  }

  public void setHighestCountPerTagRef(int highestCountPerTagRef) {
    this.highestCountPerTagRef = highestCountPerTagRef;
  }

  public int getLowestCountPerTagRef() {
    return lowestCountPerTagRef;
  }

  public void setLowestCountPerTagRef(int lowestCountPerTagRef) {
    this.lowestCountPerTagRef = lowestCountPerTagRef;
  }

  public int getAvgCountPerTagRef() {
    return avgCountPerTagRef;
  }

  public void setAvgCountPerTagRef(int avgCountPerTagRef) {
    this.avgCountPerTagRef = avgCountPerTagRef;
  }

  public int getMedCountPerTagRef() {
    return medCountPerTagRef;
  }

  public void setMedCountPerTagRef(int medCountPerTagRef) {
    this.medCountPerTagRef = medCountPerTagRef;
  }

  public int getDisagreeCount() {
    return disagreeCount;
  }

  public void setDisagreeCount(int disagreeCount) {
    this.disagreeCount = disagreeCount;
  }

  public int[] getAltsReadCount() {
    return altsReadCount;
  }

  public int[] getAltsTagCount() {
    return altsTagCount;
  }

  public int[] getHighestCountPerTagAlts() {
    return highestCountPerTagAlts;
  }

  public int[] getLowestCountPerTagAlts() {
    return lowestCountPerTagAlts;
  }

  public int[] getAvgCountPerTagAlts() {
    return avgCountPerTagAlts;
  }

  public int[] getMedCountPerTagAlts() {
    return medCountPerTagAlts;
  }

}
