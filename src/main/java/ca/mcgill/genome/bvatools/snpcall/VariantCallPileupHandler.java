/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.snpcall;

import gnu.trove.iterator.TByteIntIterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.SAMRecord;

import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.GenotypeBuilder;
import htsjdk.variant.variantcontext.VariantContextBuilder;
import htsjdk.variant.variantcontext.writer.VariantContextWriter;
import htsjdk.variant.vcf.VCFConstants;
import htsjdk.variant.vcf.VCFFormatHeaderLine;
import htsjdk.variant.vcf.VCFHeader;
import htsjdk.variant.vcf.VCFHeaderLine;
import htsjdk.variant.vcf.VCFHeaderLineCount;
import htsjdk.variant.vcf.VCFHeaderLineType;
import htsjdk.variant.vcf.VCFInfoHeaderLine;

import ca.mcgill.genome.bvatools.pileup.Pileup;
import ca.mcgill.genome.bvatools.pileup.PileupHandler;

public class VariantCallPileupHandler implements PileupHandler {
  public static String GENOTYPE_TOP_ALLELE_FRACTION = "TAF";
  
  private final IndexedFastaSequenceFile refSequence;
  private final VariantContextWriter writer;
  private int minMappingQuality;
  private int minBaseQuality;
  private final String sample;
  private final float hetThreshold;
  private final float emitAt;
  //private List<VariantInfo> variants;

  public VariantCallPileupHandler(String sample, VariantContextWriter writer, IndexedFastaSequenceFile refSequence, int minMappingQuality, int minBaseQuality, float hetThreshold, float emitAt) {
    this.writer = writer;
    this.refSequence = refSequence;
    this.minMappingQuality = minMappingQuality;
    this.minBaseQuality = minBaseQuality;
    this.sample = sample;
    this.hetThreshold = hetThreshold;
    this.emitAt = emitAt;
    //this.variants = new ArrayList<VariantInfo>();
    writeHeader(sample);
  }

  private void writeHeader(String sample) {
    List<String> genotypeSampleNames = Collections.singletonList(sample);
    Set<VCFHeaderLine> metaData = new HashSet<VCFHeaderLine>();
    metaData.add(new VCFInfoHeaderLine(VCFConstants.DEPTH_KEY, 1, VCFHeaderLineType.Integer, "Approximate read depth; some reads may have been filtered"));

    metaData.add(new VCFFormatHeaderLine(VCFConstants.GENOTYPE_KEY, 1, VCFHeaderLineType.String, "Genotype"));
    metaData.add(new VCFFormatHeaderLine(VCFConstants.GENOTYPE_ALLELE_DEPTHS, VCFHeaderLineCount.UNBOUNDED, VCFHeaderLineType.Integer, "Allelic depths for the ref and alt alleles in the order listed"));    
    metaData.add(new VCFFormatHeaderLine(GENOTYPE_TOP_ALLELE_FRACTION, 1, VCFHeaderLineType.Float, "Fraction of the top allele (allele with most reads) vs Ref count. Top/(Top+Ref) "));    
    VCFHeader header = new VCFHeader(metaData, genotypeSampleNames);
    writer.writeHeader(header);
  }
//  public List<VariantInfo> getVariants() {
//    return variants;
//  }
//
//  public void setVariants(List<VariantInfo> variants) {
//    this.variants = variants;
//  }

  @Override
  public void handle(Pileup pileup) {
    ReferenceSequence base = refSequence.getSubsequenceAt(pileup.getChromosome(), pileup.getPosition(), pileup.getPosition());
    final byte refBase = base.getBases()[0];

    final VariantInfo variantInfo = new VariantInfo();
    variantInfo.getBaseCounts().put(refBase, 0);
    for (int idx = 0; idx < pileup.getNbReads(); idx++) {
      int basePosition = pileup.getReadBasePositions().get(idx) - 1;
      SAMRecord read = pileup.getReads().get(idx);

      if (read.getMappingQuality() < minMappingQuality)
        continue;

      byte readBase = read.getReadBases()[basePosition];
      byte quality = read.getBaseQualities()[basePosition];
      if (quality < minBaseQuality)
        continue;

      variantInfo.getBaseCounts().adjustOrPutValue(readBase, 1, 1);
      if (readBase != refBase) {
        variantInfo.setMutant(true);
      }
    }

    if (variantInfo.isMutant()) {
      AlleleCount counts[] = new AlleleCount[variantInfo.getBaseCounts().size()];
      
      TByteIntIterator iter = variantInfo.getBaseCounts().iterator();
      for(int idx=0; iter.hasNext(); idx++) {
        iter.advance();
        counts[idx] = new AlleleCount(iter.key(), iter.value());
      }
      Arrays.sort(counts, new Comparator<AlleleCount>() {
//        @Override
//        public int compare(AlleleCount o1, AlleleCount o2) {
//          if(o1.getBase() == o2.getBase())
//            return 0;
//          else if(o1.getBase() == refBase)
//            return -1;
//          else if(o2.getBase() == refBase)
//            return 1;
//          else {
//            return o2.getCount() - o1.getCount();
//          }
//        }
//      });
    @Override
    public int compare(AlleleCount o1, AlleleCount o2) {
      return o2.getCount() - o1.getCount();
    }
  });
    List<Allele> variantAlleles = new ArrayList<Allele>();
    int ad[] = new int[counts.length];
    ad[0] = variantInfo.getBaseCounts().get(refBase);
    int actualIdx = 1;
    int topAllele = -1;
    for(int idx=0; idx < counts.length; idx++) {
      
      if(counts[idx].getBase() == refBase) {
        variantAlleles.add(Allele.create(counts[idx].getBase(), true));
        continue;
      }
      if(topAllele == -1)
        topAllele = counts[idx].getCount();
      variantAlleles.add(Allele.create(counts[idx].getBase()));
      ad[actualIdx] = counts[idx].getCount();
      actualIdx++;
    }
      
    float taf = (float)topAllele / (float)(topAllele+ad[0]);
    if(taf >= emitAt) {
      List<Allele> genotypeAlleles = new ArrayList<Allele>();
      int dp = 0;
      if(counts.length == 1) {
        dp = counts[0].getCount();
        if(counts[0].getBase() == refBase) {
          genotypeAlleles.add(Allele.create(counts[0].getBase(), true));
          genotypeAlleles.add(Allele.create(counts[0].getBase(), true));
        }
        else {
          genotypeAlleles.add(Allele.create(counts[0].getBase()));
          genotypeAlleles.add(Allele.create(counts[0].getBase()));
        }
      }
      else {
        float frac = (float)counts[1].getCount()/(float)(counts[0].getCount()+counts[1].getCount());
        if(frac >= hetThreshold) {
          dp = counts[0].getCount() + counts[1].getCount();
          if(counts[0].getBase() == refBase) {
            genotypeAlleles.add(Allele.create(counts[0].getBase(), true));
          }
          else {
            genotypeAlleles.add(Allele.create(counts[0].getBase()));
          }
          if(counts[1].getBase() == refBase) {
            genotypeAlleles.add(Allele.create(counts[1].getBase(), true));
          }
          else {
            genotypeAlleles.add(Allele.create(counts[1].getBase()));
          }
        }
        else {
          dp = counts[0].getCount();
          if(counts[0].getBase() == refBase) {
            genotypeAlleles.add(Allele.create(counts[0].getBase(), true));
            genotypeAlleles.add(Allele.create(counts[0].getBase(), true));
          }
          else {
            genotypeAlleles.add(Allele.create(counts[0].getBase()));
            genotypeAlleles.add(Allele.create(counts[0].getBase()));
          }
        }
      }
  //      //variants.add(variantInfo);
  //      List<Allele> variantAlleles = new ArrayList<Allele>();
  //      List<Allele> genotypeAlleles = new ArrayList<Allele>();
  //      
  //      ad[0] = counts[0].getCount();
  //      Allele refAllele = Allele.create(counts[0].getBase(), true);
  //      variantAlleles.add(refAllele);
  //      if(ad[0] > 0) 
  //        genotypeAlleles.add(refAllele);
  //      
  //      int topAllele = -1;
  //      for(int idx=1; idx < counts.length; idx++) {
  //        Allele altAllele = Allele.create(counts[idx].getBase());
  //        variantAlleles.add(altAllele);
  //        ad[idx] = counts[idx].getCount();
  //        if(ad[idx] > 0) {
  //          if(topAllele == -1)
  //            topAllele = ad[idx];
  //          genotypeAlleles.add(altAllele);
  //        }
  //      }
  
        GenotypeBuilder gb = new GenotypeBuilder(sample, genotypeAlleles);
        gb.AD(ad);
        gb.attribute(GENOTYPE_TOP_ALLELE_FRACTION,  taf);
        
        VariantContextBuilder builder = new VariantContextBuilder("dude", pileup.getChromosome(), pileup.getPosition(), pileup.getPosition(), variantAlleles);
        builder.genotypes(Collections.singleton(gb.make()));
        builder.unfiltered();
        builder.attribute(VCFConstants.DEPTH_KEY, dp);
  
        writer.add(builder.make());
      }
    }
  }
  
  private static class AlleleCount {
    private final byte base;
    private final int count;
    public AlleleCount(byte base, int count) {
      super();
      this.base = base;
      this.count = count;
    }
    public byte getBase() {
      return base;
    }
    public int getCount() {
      return count;
    }
  }
}
