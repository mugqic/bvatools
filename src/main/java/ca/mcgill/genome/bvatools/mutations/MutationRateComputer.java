/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.mutations;

import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import gnu.trove.procedure.TObjectIntProcedure;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ca.mcgill.genome.bvatools.experimental.SamtoolsPairFilter;
import ca.mcgill.mcb.pcingola.fileIterator.VcfFileIterator;
import ca.mcgill.mcb.pcingola.interval.Marker;
import ca.mcgill.mcb.pcingola.util.Gpr;
import ca.mcgill.mcb.pcingola.vcf.VcfEntry;
import ca.mcgill.mcb.pcingola.vcf.VcfGenotype;
import ca.mcgill.mcb.pcingola.vcf.VcfHeader;

public class MutationRateComputer implements Runnable {
  private Map<String, Region> regions;
  private final VcfFileInfo vcfFileInfo;
  private final double minQual;
  private final int minGenotypeQual;
  private final String filter;
  private final Set<String> toExclude;
  private final MutationRateTarget somaticMutRateTarget = new MutationRateTarget();
  private final MutationRateTarget tumorMutRateTarget = new MutationRateTarget();
  private final MutationRateTarget normalMutRateTarget = new MutationRateTarget();

  public MutationRateComputer(VcfFileInfo vcfFileInfo, Map<String, Region> regions, Set<String> toExclude, double minQual, int minGenotypeQual, String filter) {
    super();
    this.vcfFileInfo = vcfFileInfo;
    this.regions = regions;
    this.toExclude = toExclude;
    this.minQual = minQual;
    this.minGenotypeQual = minGenotypeQual;
    this.filter = filter;
  }

  public MutationRateTarget getSomaticMutRateTarget() {
    return somaticMutRateTarget;
  }

  public MutationRateTarget getTumorMutRateTarget() {
    return tumorMutRateTarget;
  }

  public MutationRateTarget getNormalMutRateTarget() {
    return normalMutRateTarget;
  }

  @Override
  public void run() {
    String sampleName = vcfFileInfo.getVcffile().getName().substring(0, vcfFileInfo.getVcffile().getName().indexOf('.'));
    System.out.println("Processing " + sampleName + "...");

    somaticMutRateTarget.getCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());
    somaticMutRateTarget.getCountPerTypePerSample().get(sampleName).put("total", 0l);
    tumorMutRateTarget.getCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());
    tumorMutRateTarget.getCountPerTypePerSample().get(sampleName).put("total", 0l);
    normalMutRateTarget.getCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());
    normalMutRateTarget.getCountPerTypePerSample().get(sampleName).put("total", 0l);

    VcfFileIterator vcfParser = new VcfFileIterator(vcfFileInfo.getVcffile().toString());
    int normalIdx = 0;
    int tumorIdx = 1;
    if(vcfFileInfo.getNormalName() != null) {
      normalIdx = -1;
      tumorIdx = -1;
      VcfHeader header = vcfParser.readHeader();
      int idx=0;
      for(String sample : header.getSampleNames()) {
        if(sample.equals(vcfFileInfo.getNormalName())) {
          normalIdx = idx;
        }
        else if (sample.equals(vcfFileInfo.getTumorName())) {
          tumorIdx = idx;
        }
        idx++;
      }
      if(normalIdx == -1 || tumorIdx == -1) {
        throw new RuntimeException("Can't find normal and/or tumor sample names in VCF header");
      }
    }
    
    TObjectLongHashMap<String> somaticSubstitutionCounts = new TObjectLongHashMap<String>();
    TObjectLongHashMap<String> tumorSubstitutionCounts = new TObjectLongHashMap<String>();
    TObjectLongHashMap<String> normalSubstitutionCounts = new TObjectLongHashMap<String>();
    for (String var : MutationRateTarget.substitutionOrder) {
      somaticSubstitutionCounts.put(var, 0);
      tumorSubstitutionCounts.put(var, 0);
      normalSubstitutionCounts.put(var, 0);
    }
    somaticMutRateTarget.getSubstitutionCountPerSample().put(sampleName, somaticSubstitutionCounts);
    tumorMutRateTarget.getSubstitutionCountPerSample().put(sampleName, tumorSubstitutionCounts);
    normalMutRateTarget.getSubstitutionCountPerSample().put(sampleName, normalSubstitutionCounts);
    somaticMutRateTarget.getSubstitutionCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());
    tumorMutRateTarget.getSubstitutionCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());
    normalMutRateTarget.getSubstitutionCountPerTypePerSample().put(sampleName, new TObjectLongHashMap<String>());

    try {
      TObjectIntMap<String> geneCnt = new TObjectIntHashMap<String>();
      for (VcfEntry vcfEntry : vcfParser) {
        if (vcfEntry.getQuality() < minQual
        || toExclude.contains(vcfEntry.getChromosomeName())
        || vcfEntry.getRef().indexOf('N') != -1
        || (filter != null && vcfEntry.getFilterPass().indexOf(filter) == -1)) {
          continue;
        }

        VcfGenotype normalGenotype = vcfEntry.getVcfGenotype(normalIdx);
        VcfGenotype tumorGenotype = vcfEntry.getVcfGenotype(tumorIdx);
        boolean isSomatic = vcfEntry.getInfoFlag(SamtoolsPairFilter.SOMATIC_FLAG);
        if (vcfEntry.isSnp()) {
          String substKey = vcfEntry.getRef() + '>' + vcfEntry.getAlts()[0];

          boolean normalHasVariant = false;
          if (Gpr.parseIntSafe(normalGenotype.get("GQ")) >= minGenotypeQual && normalGenotype.getGenotypeCode() != 0) {
            normalHasVariant = true;
            if (normalMutRateTarget.getSubstitutionCountPerSample().get(sampleName).increment(substKey) == false) {
              throw new RuntimeException("Missing titv" + vcfEntry);
            }
            normalMutRateTarget.getCountPerTypePerSample().get(sampleName).increment("total");
          }

          boolean tumorHasVariant = false;
          if (Gpr.parseIntSafe(tumorGenotype.get("GQ")) >= minGenotypeQual && tumorGenotype.getGenotypeCode() != 0) {
            tumorHasVariant = true;
            if (tumorMutRateTarget.getSubstitutionCountPerSample().get(sampleName).increment(substKey) == false) {
              throw new RuntimeException("Missing titv" + vcfEntry);
            }
            tumorMutRateTarget.getCountPerTypePerSample().get(sampleName).increment("total");
          } else {
            isSomatic = false;
          }

          if (isSomatic) {
            if (somaticMutRateTarget.getSubstitutionCountPerSample().get(sampleName).increment(substKey) == false) {
              throw new RuntimeException("Missing titv" + vcfEntry);
            }
            somaticMutRateTarget.getCountPerTypePerSample().get(sampleName).increment("total");
          }

          Set<String> types = getHits(vcfEntry);
          if (types.size() == 0) {
            //System.err.println("Strange all positions should have hits: " + vcfEntry);
          }

          for (String typeName : types) {
            String regionSubstKey = substKey + '_' + typeName;

            if (isSomatic) {
              somaticMutRateTarget.getCountPerTypePerSample().get(sampleName).adjustOrPutValue(typeName, 1, 1);
              somaticMutRateTarget.getSubstitutionCountPerTypePerSample().get(sampleName).adjustOrPutValue(regionSubstKey, 1, 1);
            }
            if (tumorHasVariant) {
              tumorMutRateTarget.getCountPerTypePerSample().get(sampleName).adjustOrPutValue(typeName, 1, 1);
              tumorMutRateTarget.getSubstitutionCountPerTypePerSample().get(sampleName).adjustOrPutValue(regionSubstKey, 1, 1);
            }
            if (normalHasVariant) {
              normalMutRateTarget.getCountPerTypePerSample().get(sampleName).adjustOrPutValue(typeName, 1, 1);
              normalMutRateTarget.getSubstitutionCountPerTypePerSample().get(sampleName).adjustOrPutValue(regionSubstKey, 1, 1);
            }
          }
        }
      }

      geneCnt.forEachEntry(new TObjectIntProcedure<String>() {

        @Override
        public boolean execute(String a, int b) {
          System.err.println(a + "\t" + b);
          return true;
        }

      });

    } finally {
      vcfParser.close();
    }
  }// for samples

  public Set<String> getHits(Marker marker) {
    Set<String> hits = new HashSet<String>();
    for(String name : regions.keySet()) {
      Region region = regions.get(name);
      
      if(region.intersects(marker)) {
        hits.add(name);
      }
    }

    return hits;
  }
  
  public static class VcfFileInfo {
    private final File vcffile;
    private final String normalName;
    private final String tumorName;

    public VcfFileInfo(File vcffile) {
      this.vcffile = vcffile;
      this.normalName = null;
      this.tumorName = null;
    }

    public VcfFileInfo(File vcffile, String normalName, String tumorName) {
      this.vcffile = vcffile;
      this.normalName = normalName;
      this.tumorName = tumorName;
    }

    public File getVcffile() {
      return vcffile;
    }

    public String getNormalName() {
      return normalName;
    }

    public String getTumorName() {
      return tumorName;
    }
    
  }
}
