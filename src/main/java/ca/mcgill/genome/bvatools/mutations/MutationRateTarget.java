/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.mutations;

import gnu.trove.map.hash.TObjectLongHashMap;

import java.util.HashMap;
import java.util.Map;

public class MutationRateTarget {
  public static final String substitutionOrder[] = { "A>T", "A>C", "A>G", "C>A", "C>G", "C>T", "G>A", "G>C", "G>T", "T>A", "T>C", "T>G" };

  private final Map<String, TObjectLongHashMap<String>> countPerTypePerSample = new HashMap<String, TObjectLongHashMap<String>>();
  private final Map<String, TObjectLongHashMap<String>> substitutionCountPerSample = new HashMap<String, TObjectLongHashMap<String>>();
  private final Map<String, TObjectLongHashMap<String>> substitutionCountPerTypePerSample = new HashMap<String, TObjectLongHashMap<String>>();

  public void add(MutationRateTarget src) {
    for (String sample : src.countPerTypePerSample.keySet()) {
      if (countPerTypePerSample.put(sample, src.countPerTypePerSample.get(sample)) != null) {
        throw new RuntimeException("Sample already added, countPerTypePerSample: " + sample);
      }
    }
    for (String sample : src.substitutionCountPerSample.keySet()) {
      if (substitutionCountPerSample.put(sample, src.substitutionCountPerSample.get(sample)) != null) {
        throw new RuntimeException("Sample already added, substitutionCountPerSample: " + sample);
      }
    }
    for (String sample : src.substitutionCountPerTypePerSample.keySet()) {
      if (substitutionCountPerTypePerSample.put(sample, src.substitutionCountPerTypePerSample.get(sample)) != null) {
        throw new RuntimeException("Sample already added, substitutionCountPerTypePerSample: " + sample);
      }
    }
  }

  public Map<String, TObjectLongHashMap<String>> getCountPerTypePerSample() {
    return countPerTypePerSample;
  }

  public Map<String, TObjectLongHashMap<String>> getSubstitutionCountPerSample() {
    return substitutionCountPerSample;
  }

  public Map<String, TObjectLongHashMap<String>> getSubstitutionCountPerTypePerSample() {
    return substitutionCountPerTypePerSample;
  }

}
