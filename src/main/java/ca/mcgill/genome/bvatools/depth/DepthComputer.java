/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.depth;

import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.reference.ReferenceSequenceFile;
import htsjdk.samtools.util.CloseableIterator;

import java.io.File;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ca.mcgill.genome.bvatools.util.SAMUtils;

public class DepthComputer implements Runnable, UncaughtExceptionHandler {
  private final File bam;
  private final ReferenceSequenceFile ref;
  private final List<DepthInterval> intervals;
  private final boolean ommitN;
  private final boolean computeGC;
  private final int minMappingQuality;
  private final int minBaseQuality;
  private Throwable caughtException;
  
  /**
   * 
   * @param bam
   * @param ref Needs to be a new instance per DepthComputer
   * @param intervals
   * @param ommitN
   * @param computeGC
   * @param minMappingQuality
   * @param minBaseQuality
   */
  public DepthComputer(File bam, ReferenceSequenceFile ref, List<DepthInterval> intervals, boolean ommitN, boolean computeGC, int minMappingQuality, int minBaseQuality) {
    super();
    this.bam = bam;
    this.ref = ref;
    this.intervals = intervals;
    this.ommitN = ommitN;
    this.computeGC = computeGC;
    this.minMappingQuality = minMappingQuality;
    this.minBaseQuality = minBaseQuality;
    this.caughtException = null;
  }

  public ReferenceSequenceFile getRef() {
    return ref;
  }

  @Override
  public void uncaughtException(Thread t, Throwable e) {
    caughtException = e;
    System.err.println("UncaughtException on thread ["+t.getName()+": "+t.getId()+"]");
  }

  public Throwable getCaughtException() {
    return caughtException;
  }

  @Override
  public void run() {
    CloseableIterator<SAMRecord> recIter = null;
    SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT).enable(SamReaderFactory.Option.CACHE_FILE_BASED_INDEXES);

    try (SamReader reader = samReaderFactory.open(bam)){
      List<ReadDepthHandler> listCompute = new ArrayList<ReadDepthHandler>();
      for (DepthInterval interval : intervals) {
        ReadDepthHandler depthCounter = new ReadDepthHandler(interval, ommitN, computeGC, minMappingQuality, minBaseQuality);
        listCompute.add(depthCounter);
      }

      // jump to the first interval
      DepthInterval firstInterval = intervals.get(0);

      recIter = SAMUtils.jumpToBAMLocation(reader, firstInterval.getChromosomeIndex(), firstInterval.getStart());
      while (recIter.hasNext()) {
        SAMRecord record = recIter.next();
        if (record.getNotPrimaryAlignmentFlag() || record.getReadUnmappedFlag() || record.getDuplicateReadFlag() || record.getMappingQuality() < minMappingQuality)
          continue;

        Iterator<ReadDepthHandler> iter = listCompute.iterator();
        while (iter.hasNext()) {
          ReadDepthHandler computeDepth = iter.next();
          DepthInterval interval = computeDepth.getInterval();
          if (interval.getChromosomeIndex() < record.getReferenceIndex().intValue()) {
            // Past this chromosome already
            computeDepth.finalizeInterval();
            iter.remove();
          } else if (interval.getChromosomeIndex() > record.getReferenceIndex().intValue()) {
            // Interval to come, not processed yet. Stop here
            break;
          } else if (record.getAlignmentStart() > interval.getEnd()) {
            // Past this start coordinate already
            computeDepth.finalizeInterval();
            iter.remove();
          } else if (record.getAlignmentEnd() >= interval.getStart()) {
            // Intersecting
            computeDepth.updateDepth(record, ref);
          }
        }

        // No new intervals to check.
        if(listCompute.isEmpty())
          break;
      }
      
      // Finalize remaining
      for(ReadDepthHandler computeDepth : listCompute) {
        computeDepth.finalizeInterval();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if(recIter != null)
        recIter.close();
    }
  }
}
