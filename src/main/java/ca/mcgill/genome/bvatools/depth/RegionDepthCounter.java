/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.depth;

import gnu.trove.list.linked.TLinkedList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import htsjdk.samtools.reference.ReferenceSequence;
import htsjdk.samtools.reference.ReferenceSequenceFile;

public class RegionDepthCounter {
  private static final byte n_VALUE = (byte) 'n';
  private static final byte N_VALUE = (byte) 'N';
  private final TLinkedList<LinkableBasePairCount> regionWindow;
  private final String sequenceName;
  private int regionStartCoordinate;
  private final ReferenceSequenceFile fastaRef;
  private final boolean ommitN;

  public RegionDepthCounter(ReferenceSequenceFile fastaRef, String sequenceName, int startPosition, boolean ommitN) {
    this.fastaRef = fastaRef;
    this.regionWindow = new TLinkedList<LinkableBasePairCount>();
    this.sequenceName = sequenceName;
    this.regionStartCoordinate = startPosition;
    this.ommitN = ommitN;

    byte base = BasePairCount.MISSING_BASE;
    if (fastaRef != null) {
      ReferenceSequence sequence = fastaRef.getSubsequenceAt(sequenceName, startPosition, startPosition);
      base = sequence.getBases()[0];
    }

    if (ommitN) {
      if (base == N_VALUE || base == n_VALUE) {
        regionWindow.add(new LinkableBasePairCount(BasePairCount.NO_VALUE, base));
      } else {
        regionWindow.add(new LinkableBasePairCount(0, base));
      }
    } else {
      regionWindow.add(new LinkableBasePairCount(0, base));
    }
  }

  public List<BasePairCount> adjustToPosition(int position) {
    List<BasePairCount> retVal = new ArrayList<BasePairCount>();
    int nbToRemove = position - regionStartCoordinate;
    regionStartCoordinate = position;
    if (nbToRemove > 0) {
      for (int idx = 0; idx < nbToRemove; idx++) {
        retVal.add(regionWindow.removeFirst());
      }
    }
    return retVal;
  }

  public byte[] bases(int start, int stop) {
    ReferenceSequence sequence = fastaRef.getSubsequenceAt(sequenceName, start, stop);
    byte bases[] = sequence.getBases();
    return bases;
  }

  public boolean ommit(byte base) {
    return base == n_VALUE || base == N_VALUE;
  }

  public void resizeTo(int position) {
    int offset = position - regionStartCoordinate;
    int nbToAdd = offset - regionWindow.size() + 1;
    if (nbToAdd > 0) {
      byte bases[];
      if (fastaRef != null) {
        bases = bases(regionStartCoordinate + regionWindow.size() - 1, position);
      } else {
        bases = new byte[nbToAdd];
        Arrays.fill(bases, BasePairCount.MISSING_BASE);
      }

      for (int idx = 0; idx < nbToAdd; idx++) {
        regionWindow.add(new LinkableBasePairCount(ommitN && ommit(bases[idx]) ? BasePairCount.NO_VALUE : 0, bases[idx]));
      }
    }
  }

  public void incrementPosition(int position) {
    int offset = position - regionStartCoordinate;
    regionWindow.get(offset).incrementValid();
  }

  public void incrementRange(int start, int stop) {
    int offsetStart = start - regionStartCoordinate;
    int offsetStop = stop - regionStartCoordinate;

    LinkableBasePairCount tmp = regionWindow.get(offsetStart);
    for (int i = offsetStart; i <= offsetStop; i++) {
      tmp.incrementValid();
      tmp = tmp.getNext();
    }
  }

  public void incrementRangeFiltered(int start, int stop, RangeFilter filter) {
    int offsetStart = start - regionStartCoordinate;
    int offsetStop = stop - regionStartCoordinate;

    LinkableBasePairCount tmp = regionWindow.get(offsetStart);
    for (int i = offsetStart, idx = 0; i <= offsetStop; i++, idx++) {
      if (filter != null && filter.isValidToIncrement(idx, tmp))
        tmp.incrementValid();
      tmp = tmp.getNext();
    }
  }

  /**
   * 
   * @param position
   * @return List of passed position values
   */
  public void ignorePosition(int position) {
    int offset = position - regionStartCoordinate;
    regionWindow.get(offset).setBpCount(BasePairCount.NO_VALUE);
  }

  public static interface RangeFilter {
    public boolean isValidToIncrement(int rangeIndex, LinkableBasePairCount basePairCount);
  }
}
