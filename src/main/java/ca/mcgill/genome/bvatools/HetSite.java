/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools;

public class HetSite {

  private final String chromosome;
  private final int position;
  private final char allele1;
  private final char allele2;

  public HetSite(String chromosome, int position, char allele1, char allele2) {
    super();
    this.chromosome = chromosome;
    this.position = position;
    this.allele1 = allele1;
    this.allele2 = allele2;
  }

  public String getChromosome() {
    return chromosome;
  }

  public int getPosition() {
    return position;
  }

  public char getAllele1() {
    return allele1;
  }

  public char getAllele2() {
    return allele2;
  }

}
