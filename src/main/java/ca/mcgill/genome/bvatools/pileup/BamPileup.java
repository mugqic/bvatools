/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.pileup;

import htsjdk.samtools.AlignmentBlock;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BamPileup {
  private final File bam;
  private LinkedList<Pileup> pileupQueue;
  private int pileupQueueStart;
  private String pileupQueueChromosome;
  private List<ReadHandler> readHandler;
  private String chr;
  private int start;
  private int end;

  public BamPileup(File bam) {
    this(bam, null, -1, -1);
  }

  public BamPileup(File bam, String chr, int start, int end) {
    this.bam = bam;
    this.readHandler = new ArrayList<ReadHandler>();
    this.chr = chr;
    this.start = start;
    this.end = end;
  }

  
  public void addReadHandler(ReadHandler handler) {
    readHandler.add(handler);
  }

  public void readBam(PileupHandler consumer) {
    SamReader reader = null; 
    try {
      SamReaderFactory samReaderFactory = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
      reader = samReaderFactory.open(bam);
      SAMFileHeader header = reader.getFileHeader();
      if (header.getSortOrder() != SortOrder.coordinate) {
        throw new RuntimeException("BAM has to be coordinate sorted");
      }

      pileupQueue = new LinkedList<Pileup>();
      pileupQueueChromosome = "";
      pileupQueueStart = -1;

      SAMRecordIterator iterator;
      if(chr != null) {
        iterator = reader.query(chr, start, end, false);
      }
      else {
        iterator = reader.iterator();
      }
      while (iterator.hasNext()) {
        SAMRecord read = iterator.next();
        if (read.getReadUnmappedFlag() || read.getNotPrimaryAlignmentFlag())
          continue;
        cleanPileups(read, consumer);
        extendPileups(read);

        for (ReadHandler handler : readHandler) {
          handler.readToPileup(read);
        }

//        Cigar cigar = read.getCigar();
//        int refOffset = 0;
//        for (CigarElement cigarElement : cigar.getCigarElements()) {
//          if (cigarElement.getOperator().equals(CigarOperator.D)) {
//            cigarElement.getLength();
//          } else if (cigarElement.getOperator().equals(CigarOperator.I)) {
//            cigarElement.getLength();
//          } else {
//            if (cigarElement.getOperator().consumesReferenceBases()) {
//              refOffset += cigarElement.getLength();
//            }
//          }
//        }        

        for (AlignmentBlock block : read.getAlignmentBlocks()) {
          for (int idx = 0; idx < block.getLength(); idx++) {
            addReadToPileup(read, block.getReadStart() + idx, block.getReferenceStart() + idx);
          }
        }
      }
      flushPileups(consumer);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          // Ignored
          e.printStackTrace();
        }
      }
    }
  }

  private void addReadToPileup(SAMRecord read, int readOffset, int position) {
    pileupQueue.get(position - pileupQueueStart).add(read, readOffset);
  }

  private void extendPileups(SAMRecord read) {
    if (pileupQueueChromosome.equals("")) {
      pileupQueueChromosome = read.getReferenceName();
      pileupQueueStart = read.getAlignmentStart();
      System.err.println("Changing chromosome: " + pileupQueueChromosome);
    }

    if (!read.getReferenceName().equals(pileupQueueChromosome)) {
      return;
    }

    if (pileupQueueStart == -1) {
      pileupQueueStart = read.getAlignmentStart();
    }

    int endPosition = read.getAlignmentEnd();
    while ((pileupQueueStart + pileupQueue.size() - 1) < endPosition) {
      pileupQueue.add(new Pileup(pileupQueueChromosome, pileupQueueStart + pileupQueue.size()));
    }
  }

  private void flushPileups(PileupHandler consumer) {
    while (pileupQueue.size() > 0) {
      Pileup pileup = pileupQueue.removeFirst();
      consumer.handle(pileup);
    }
    pileupQueueChromosome = "";
    pileupQueueStart = -1;
  }

  private void cleanPileups(SAMRecord read, PileupHandler consumer) {
    if (!read.getReferenceName().equals(pileupQueueChromosome)) {
      while (pileupQueue.size() > 0) {
        Pileup pileup = pileupQueue.removeFirst();
        consumer.handle(pileup);
      }
      pileupQueueChromosome = "";
      pileupQueueStart = -1;
    } else {
      while (pileupQueueStart != -1 && read.getAlignmentStart() > pileupQueueStart) {
        Pileup pileup = pileupQueue.removeFirst();
        consumer.handle(pileup);
        if (pileupQueue.size() == 0)
          pileupQueueStart = -1;
        else
          pileupQueueStart = pileupQueue.getFirst().getPosition();
      }
    }
  }
}
