/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.pwm;

import java.io.IOException;

import ca.mcgill.genome.bvatools.parsers.CISBPParser;

public class RnaBindingProteinInfo {
  private final CISBPParser parser;
  private final String geneName;
  private final String geneId;
  private final String cisbpRnaId;
  private final String motifId;
  private final String studyId;
  private PositionWeightMatrix pwm = null;

  public RnaBindingProteinInfo(CISBPParser parser, String cisbpRnaId, String geneName, String geneId, String motifId, String studyId) {
    this.parser = parser;
    this.cisbpRnaId = cisbpRnaId;
    this.geneName = geneName;
    this.geneId = geneId;
    this.motifId = motifId;
    this.studyId = studyId;
  }

  public CISBPParser getParser() {
    return parser;
  }

  public String getGeneName() {
    return geneName;
  }

  public String getCISBPRNAId() {
    return cisbpRnaId;
  }

  public String getGeneId() {
    return geneId;
  }

  public String getMotifId() {
    return motifId;
  }

  public String getStudyId() {
    return studyId;
  }

  public PositionWeightMatrix getPwm() {
    if (pwm == null) {
      try {
        pwm = parser.readPWM(motifId);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
    return pwm;
  }

}
