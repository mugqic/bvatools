/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools;

public class Allele {

  private String order;
  private String strand;
  private Character base;
  public int firstForwardCs = 0;
  public int firstForwardTs = 0;
  public int secondForwardCs = 0;
  public int secondForwardTs = 0;
  public int firstReverseCs = 0;
  public int firstReverseTs = 0;
  public int secondReverseCs = 0;
  public int secondReverseTs = 0;

  public Allele() {

    // TODO Auto-generated constructor stub
  }

  public void setOrder(String order) {
    this.order = order;
  }

  public void setStrand(String strand) {
    this.strand = strand;
  }

  public String getOrder() {
    return this.order;
  }

  public String getStrand() {
    return this.strand;
  }

  public void setBase(Character a) {
    this.base = a;
  }

  public Character getBase() {
    return this.base;
  }

}
