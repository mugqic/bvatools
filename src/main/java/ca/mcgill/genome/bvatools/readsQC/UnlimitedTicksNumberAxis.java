/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTick;
import org.jfree.chart.axis.Tick;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

public class UnlimitedTicksNumberAxis extends NumberAxis {
  private static final long serialVersionUID = 7825770677843627382L;

  public UnlimitedTicksNumberAxis() {
    super();
  }

  public UnlimitedTicksNumberAxis(String label) {
    super(label);
  }

  @Override
  protected List<Tick> refreshTicksHorizontal(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {

    List<Tick> result = new ArrayList<Tick>();

    Font tickLabelFont = getTickLabelFont();
    g2.setFont(tickLabelFont);

    if (isAutoTickUnitSelection()) {
      selectAutoTickUnit(g2, dataArea, edge);
    }

    double size = getTickUnit().getSize();
    int count = calculateVisibleTickCount();
    double lowestTickValue = calculateLowestVisibleTickValue();

    for (int i = 0; i < count; i++) {
      double currentTickValue = lowestTickValue + (i * size);
      String tickLabel;
      NumberFormat formatter = getNumberFormatOverride();
      if (formatter != null) {
        tickLabel = formatter.format(currentTickValue);
      } else {
        tickLabel = getTickUnit().valueToString(currentTickValue);
      }
      TextAnchor anchor = null;
      TextAnchor rotationAnchor = null;
      double angle = 0.0;
      if (isVerticalTickLabels()) {
        anchor = TextAnchor.CENTER_RIGHT;
        rotationAnchor = TextAnchor.CENTER_RIGHT;
        if (edge == RectangleEdge.TOP) {
          angle = Math.PI / 2.0;
        } else {
          angle = -Math.PI / 2.0;
        }
      } else {
        if (edge == RectangleEdge.TOP) {
          anchor = TextAnchor.BOTTOM_CENTER;
          rotationAnchor = TextAnchor.BOTTOM_CENTER;
        } else {
          anchor = TextAnchor.TOP_CENTER;
          rotationAnchor = TextAnchor.TOP_CENTER;
        }
      }

      Tick tick = new NumberTick(new Double(currentTickValue), tickLabel, anchor, rotationAnchor, angle);
      result.add(tick);
    }
    return result;

  }

  /**
   * Calculates the positions of the tick labels for the axis, storing the results in the tick label list (ready for
   * drawing).
   * 
   * @param g2
   *          the graphics device.
   * @param dataArea
   *          the area in which the plot should be drawn.
   * @param edge
   *          the location of the axis.
   * 
   * @return A list of ticks.
   */
  @Override
  protected List<Tick> refreshTicksVertical(Graphics2D g2, Rectangle2D dataArea, RectangleEdge edge) {

    List<Tick> result = new ArrayList<Tick>();

    Font tickLabelFont = getTickLabelFont();
    g2.setFont(tickLabelFont);
    if (isAutoTickUnitSelection()) {
      selectAutoTickUnit(g2, dataArea, edge);
    }

    double size = getTickUnit().getSize();
    int count = calculateVisibleTickCount();
    double lowestTickValue = calculateLowestVisibleTickValue();

    for (int i = 0; i < count; i++) {
      double currentTickValue = lowestTickValue + (i * size);
      String tickLabel;
      NumberFormat formatter = getNumberFormatOverride();
      if (formatter != null) {
        tickLabel = formatter.format(currentTickValue);
      } else {
        tickLabel = getTickUnit().valueToString(currentTickValue);
      }

      TextAnchor anchor = null;
      TextAnchor rotationAnchor = null;
      double angle = 0.0;
      if (isVerticalTickLabels()) {
        if (edge == RectangleEdge.LEFT) {
          anchor = TextAnchor.BOTTOM_CENTER;
          rotationAnchor = TextAnchor.BOTTOM_CENTER;
          angle = -Math.PI / 2.0;
        } else {
          anchor = TextAnchor.BOTTOM_CENTER;
          rotationAnchor = TextAnchor.BOTTOM_CENTER;
          angle = Math.PI / 2.0;
        }
      } else {
        if (edge == RectangleEdge.LEFT) {
          anchor = TextAnchor.CENTER_RIGHT;
          rotationAnchor = TextAnchor.CENTER_RIGHT;
        } else {
          anchor = TextAnchor.CENTER_LEFT;
          rotationAnchor = TextAnchor.CENTER_LEFT;
        }
      }

      Tick tick = new NumberTick(new Double(currentTickValue), tickLabel, anchor, rotationAnchor, angle);
      result.add(tick);
    }
    return result;
  }

}
