/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.list.array.TLongArrayList;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBoxAndWhiskerRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.statistics.BoxAndWhiskerItem;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;
import ca.mcgill.genome.bvatools.util.GrowingLongArrayList;

public class SequenceQualityPlugin extends DefaultQCPlugin implements IReadSetMedian {
  private final List<List<GrowingLongArrayList>> readsPerBaseQualitiesBins = new ArrayList<List<GrowingLongArrayList>>();
  private final List<TLongArrayList> readsSumOfBaseQualitiesPerCycle = new ArrayList<TLongArrayList>();
  private final List<TLongArrayList> readsNbReadsPerCycle = new ArrayList<TLongArrayList>();
  private final List<GrowingLongArrayList> readsOverallQualityBins = new ArrayList<GrowingLongArrayList>();
  private long totalNbBases = 0;
  private long totalQuality = 0;
  private int overallMedian = -1;
  private float overallArithmeticMean = -1.0f;
  private int maxCycle = 0;
  private int maxQuality = 0;
  private BinnedQualityBoxAndWhiskerXYDataset qualityDataset = null;

  @Override
  public void processSequence(Fragment fragment) {
    while(readsPerBaseQualitiesBins.size() < fragment.size()) {
      readsPerBaseQualitiesBins.add(new ArrayList<GrowingLongArrayList>());
      readsSumOfBaseQualitiesPerCycle.add(new TLongArrayList());
      readsNbReadsPerCycle.add(new TLongArrayList());
      readsOverallQualityBins.add(new GrowingLongArrayList());
    }
    
    for(int idx=0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if(read == null)
        continue;

      List<GrowingLongArrayList> perBaseQualitiesBins = readsPerBaseQualitiesBins.get(idx);
      TLongArrayList sumOfBaseQualitiesPerCycle = readsSumOfBaseQualitiesPerCycle.get(idx);
      TLongArrayList nbReadsPerCycle = readsNbReadsPerCycle.get(idx);
      GrowingLongArrayList overallQualityBins = readsOverallQualityBins.get(idx);
      
      for (int i = perBaseQualitiesBins.size(); i < read.getBaseQualities().length; i++) {
        perBaseQualitiesBins.add(new GrowingLongArrayList());
        sumOfBaseQualitiesPerCycle.add(0l);
        nbReadsPerCycle.add(0l);
      }
  
      for (int i = 0; i < read.getBaseQualities().length; i++) {
        totalNbBases++;
        byte quality = read.getBaseQualities()[i];

        overallQualityBins.set(quality, overallQualityBins.get(quality) + 1);
        perBaseQualitiesBins.get(i).set(quality, perBaseQualitiesBins.get(i).get(quality) + 1);
        sumOfBaseQualitiesPerCycle.set(i, sumOfBaseQualitiesPerCycle.get(i) + quality);
        nbReadsPerCycle.set(i, nbReadsPerCycle.get(i) + 1l);
      }
    }
  }

  public long getTotalNbBases() {
    return totalNbBases;
  }

  public long getTotalQuality() {
    return totalQuality;
  }

  @Override
  public int getMedian() {
    computeBinDataset();
    return overallMedian;
  }

  @Override
  public float getArithmeticMean() {
    computeBinDataset();
    return overallArithmeticMean;
  }

  private void computeBinDataset() {
    if (qualityDataset != null)
      return;
    // Merge them all
    List<GrowingLongArrayList> perBaseQualitiesBins = new ArrayList<GrowingLongArrayList>();
    TLongArrayList sumOfBaseQualitiesPerCycle = new TLongArrayList();;
    TLongArrayList nbReadsPerCycle = new TLongArrayList();
    GrowingLongArrayList overallQualityBins = new GrowingLongArrayList();
    
    for(int idx=0; idx < readsPerBaseQualitiesBins.size(); idx++) {
      perBaseQualitiesBins.addAll(readsPerBaseQualitiesBins.get(idx));
      sumOfBaseQualitiesPerCycle.addAll(readsSumOfBaseQualitiesPerCycle.get(idx));
      nbReadsPerCycle.addAll(readsNbReadsPerCycle.get(idx));
      for(int i=0; i < readsOverallQualityBins.get(idx).size(); i++)
        overallQualityBins.set(i, overallQualityBins.get(i) + readsOverallQualityBins.get(idx).get(i));
    }
    maxQuality = overallQualityBins.size();
    maxCycle = perBaseQualitiesBins.size();

    qualityDataset = new BinnedQualityBoxAndWhiskerXYDataset(overallQualityBins.size(), "Arithmetic Mean");
    long halfMark = (long) Math.ceil(totalNbBases / 2.0d);
    long sum = 0;
    overallMedian = 0;
    overallArithmeticMean = 0;
    totalQuality = 0;
    for (int i = 0; i < overallQualityBins.size(); i++) {
      sum += overallQualityBins.get(i);
      if (sum <= halfMark) {
        overallMedian = i;
      }

      totalQuality += i * overallQualityBins.get(i);
    }
    overallArithmeticMean = totalQuality / (float) totalNbBases;

    for (int i = 0; i < perBaseQualitiesBins.size(); i++) {
      long nbReadsForCycle = nbReadsPerCycle.get(i);
      double mean = 0;
      double median = -1;
      double q1 = -1;
      double q3 = -1;

      if (nbReadsForCycle > 0)
        mean = sumOfBaseQualitiesPerCycle.get(i) / nbReadsForCycle;

      long nbReadsQ1 = nbReadsForCycle / 4;
      long nbReadsQ2 = nbReadsForCycle / 2;
      long nbReadsQ3 = nbReadsForCycle / 4 * 3;

      long passedNbReads = -1l;
      GrowingLongArrayList bin = perBaseQualitiesBins.get(i);
      for (int binIdx = 0; binIdx < bin.size(); binIdx++) {
        passedNbReads += bin.get(binIdx);

        if (passedNbReads >= nbReadsQ1 && q1 < 0) {
          q1 = binIdx;
        }

        if (passedNbReads >= nbReadsQ2 && median < 0) {
          median = binIdx;
        }

        if (passedNbReads >= nbReadsQ3 && q3 < 0) {
          q3 = binIdx;
        }
      }

      double interQuartileRange = q3 - q1;

      /**
       * The coefficient used to calculate outliers. Tukey's default value is 1.5 (see EDA) Any value which is greater
       * than Q3 + (interquartile range * outlier coefficient) is considered to be an outlier. Can be altered if the
       * data is particularly skewed.
       */
      double upperOutlierThreshold = q3 + (interQuartileRange * 1.5);
      double lowerOutlierThreshold = q1 - (interQuartileRange * 1.5);

      /**
       * The coefficient used to calculate farouts. Tukey's default value is 2 (see EDA) Any value which is greater than
       * Q3 + (interquartile range * farout coefficient) is considered to be a farout. Can be altered if the data is
       * particularly skewed.
       */
      double upperFaroutThreshold = q3 + (interQuartileRange * 2.0);
      double lowerFaroutThreshold = q1 - (interQuartileRange * 2.0);

      double minRegularValue = Double.POSITIVE_INFINITY;
      double maxRegularValue = Double.NEGATIVE_INFINITY;
      double minOutlier = Double.POSITIVE_INFINITY;
      double maxOutlier = Double.NEGATIVE_INFINITY;
      List<Integer> outliers = new ArrayList<Integer>();

      for (int binIdx = 0; binIdx < bin.size(); binIdx++) {
        // test for empty bin
        if (bin.get(binIdx) == 0l)
          continue;

        if (binIdx > upperOutlierThreshold) {
          outliers.add(binIdx);
          if (binIdx > maxOutlier && binIdx <= upperFaroutThreshold) {
            maxOutlier = binIdx;
          }
        } else if (binIdx < lowerOutlierThreshold) {
          outliers.add(binIdx);
          if (binIdx < minOutlier && binIdx >= lowerFaroutThreshold) {
            minOutlier = binIdx;
          }
        } else {
          minRegularValue = Math.min(minRegularValue, binIdx);
          maxRegularValue = Math.max(maxRegularValue, binIdx);
        }
        minOutlier = Math.min(minOutlier, minRegularValue);
        maxOutlier = Math.max(maxOutlier, maxRegularValue);
      }

      BoxAndWhiskerItem item = new BoxAndWhiskerItem(mean, median, q1, q3, minRegularValue, maxRegularValue, minRegularValue, maxRegularValue, outliers);
      qualityDataset.add(item);
    }
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    computeBinDataset();

    XYBoxAndWhiskerRenderer boxRenderer = new XYBoxAndWhiskerRenderer();
    NumberAxis xAxis = new UnlimitedTicksNumberAxis("Cycles");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setFixedAutoRange(maxCycle);
    xAxis.setLowerBound(0);
    xAxis.setUpperBound(maxCycle);

    NumberAxis yAxis = new UnlimitedTicksNumberAxis("Quality");
    yAxis.setLowerBound(0);
    yAxis.setUpperBound(maxQuality);
    yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

    XYPlot plot = new XYPlot(qualityDataset, xAxis, yAxis, boxRenderer);

    boxRenderer.setFillBox(true);
    boxRenderer.setBoxPaint(Color.YELLOW);
    boxRenderer.setBasePaint(Color.BLACK);
    boxRenderer.setArtifactPaint(Color.RED);
    boxRenderer.setSeriesPaint(0, Color.BLACK);
    // Supposed to be in 1.0.13 of jfree
    // boxRenderer.setMeanVisible(false);
    XYItemRenderer meanRenderer = new XYLineAndShapeRenderer(true, false);
    plot.setDataset(1, qualityDataset);
    plot.setRenderer(1, meanRenderer);

    // Q20-30 markers
    final Marker q20Marker = new ValueMarker(20);
    q20Marker.setPaint(Color.GREEN);
    plot.addRangeMarker(q20Marker);
    final Marker q30Marker = new ValueMarker(30);
    q30Marker.setPaint(Color.BLUE);
    plot.addRangeMarker(q30Marker);

    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

    JFreeChart chart = new JFreeChart("Per Base Qualities", JFreeChart.DEFAULT_TITLE_FONT, plot, false);
    chart.removeLegend();
    chart.setBackgroundPaint(Color.white);
    plot.setBackgroundPaint(Color.white);

    File outputFile = new File(outputDirectory, generateGraphFileName("01sequenceQualityChart", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, 60 + 15 * maxCycle, 600);
  }
}
