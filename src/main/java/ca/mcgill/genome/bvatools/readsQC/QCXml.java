/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.list.array.TFloatArrayList;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashMap;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import ca.mcgill.genome.bvatools.readsQC.SequenceContentPlugin.Bases;
import ca.mcgill.mcb.pcingola.util.Gpr;

public class QCXml {
  private static final char FIELD_SEP = ',';
  private static final String QC_SUMMARY_ELEMENT = "qcsummary";

  private static final String NB_READS_ATTRIBUTE = "nbReads";
  private static final String NB_BASES_ATTRIBUTE = "nbBases";
  private static final String AVERAGE_QUALITY_ATTRIBUTE = "avgQual";
  private static final String DUPLICATE_RATE_ATTRIBUTE = "duplicateRate";
  private static final String OPTICAL_DUPLICATE_RATE_ATTRIBUTE = "opticalDuplicateRate";
  private static final String ATCGN_PERCENTAGE_FOR_EACH_CYCLE_ATTRIBUTE = "atcgnPercentageForEachCycle";
  private static final String MEDIAN_RAW_LENGTH_ATTRIBUTE = "medianRawLength";
  private static final String AVG_RAW_LENGTH_ATTRIBUTE = "avgRawLength";
  private static final String STDD_RAW_LENGTH_ATTRIBUTE = "stddRawLength";
  private static final String MEDIAN_TRIM_LENGTH_ATTRIBUTE = "medianTrimLength";
  private static final String AVG_TRIM_LENGTH_ATTRIBUTE = "avgTrimLength";
  private static final String STDD_TRIM_LENGTH_ATTRIBUTE = "stddTrimLength";
  private static final String MEDIAN_FINAL_LENGTH_ATTRIBUTE = "medianFinalLength";
  private static final String AVG_FINAL_LENGTH_ATTRIBUTE = "avgFinalLength";
  private static final String STDD_FINAL_LENGTH_ATTRIBUTE = "stddFinalLength";

  private long nbReads;
  private long nbBases;
  private float avgQual;
  private float duplicateRate;
  private float opticalDuplicateRate = -1;
  private Integer medianRawReadLength;
  private Float avgRawReadLength;
  private Float stdRawReadLength;
  private Integer medianTrimReadLength;
  private Float avgTrimReadLength;
  private Float stdTrimReadLength;
  private Integer medianFinalReadLength;
  private Float avgFinalReadLength;
  private Float stdFinalReadLength;
  private LinkedHashMap<Bases, TFloatArrayList> atcgnPercentageForEachCycle;

  public QCXml() {
    super();
  }

  public void exportJob(File outputJobFile) throws IOException {
    OutputStream outputJob = new BufferedOutputStream(new FileOutputStream(outputJobFile));
    XMLOutputFactory output = XMLOutputFactory.newInstance();
    try {
      XMLStreamWriter writer = output.createXMLStreamWriter(outputJob);
      writer.writeStartDocument();
      writer.writeStartElement(QC_SUMMARY_ELEMENT);
      writer.writeAttribute(NB_READS_ATTRIBUTE, String.valueOf(nbReads));
      writer.writeAttribute(NB_BASES_ATTRIBUTE, String.valueOf(nbBases));
      writer.writeAttribute(AVERAGE_QUALITY_ATTRIBUTE, String.valueOf(avgQual));
      writer.writeAttribute(DUPLICATE_RATE_ATTRIBUTE, String.valueOf(duplicateRate));
      if(opticalDuplicateRate > -1)
        writer.writeAttribute(OPTICAL_DUPLICATE_RATE_ATTRIBUTE, String.valueOf(opticalDuplicateRate));
      writer.writeAttribute(ATCGN_PERCENTAGE_FOR_EACH_CYCLE_ATTRIBUTE, transformATCGNPercentageForEachCycle());
      if (medianRawReadLength != null) {
        writer.writeAttribute(MEDIAN_RAW_LENGTH_ATTRIBUTE, String.valueOf(medianRawReadLength));
      }
      if (avgRawReadLength != null) {
        writer.writeAttribute(AVG_RAW_LENGTH_ATTRIBUTE, String.valueOf(avgRawReadLength));
      }
      if (stdRawReadLength != null) {
        writer.writeAttribute(STDD_RAW_LENGTH_ATTRIBUTE, String.valueOf(stdRawReadLength));
      }
      if (medianTrimReadLength != null) {
        writer.writeAttribute(MEDIAN_TRIM_LENGTH_ATTRIBUTE, String.valueOf(medianTrimReadLength));
      }
      if (avgTrimReadLength != null) {
        writer.writeAttribute(AVG_TRIM_LENGTH_ATTRIBUTE, String.valueOf(avgTrimReadLength));
      }
      if (stdTrimReadLength != null) {
        writer.writeAttribute(STDD_TRIM_LENGTH_ATTRIBUTE, String.valueOf(stdTrimReadLength));
      }
      if (medianFinalReadLength != null) {
        writer.writeAttribute(MEDIAN_FINAL_LENGTH_ATTRIBUTE, String.valueOf(medianFinalReadLength));
      }
      if (avgFinalReadLength != null) {
        writer.writeAttribute(AVG_FINAL_LENGTH_ATTRIBUTE, String.valueOf(avgFinalReadLength));
      }
      if (stdFinalReadLength != null) {
        writer.writeAttribute(STDD_FINAL_LENGTH_ATTRIBUTE, String.valueOf(stdFinalReadLength));
      }
      writer.writeEndElement();
      writer.flush();
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    }
  }

  private String transformATCGNPercentageForEachCycle() {
    StringBuilder sb = new StringBuilder();
    
    int itemCount = atcgnPercentageForEachCycle.get(Bases.A).size();
    for (int i = 0; i < itemCount; i++) {

      if(i != 0)
        sb.append(FIELD_SEP);
        
      sb.append(atcgnPercentageForEachCycle.get(Bases.A).get(i));
      sb.append(FIELD_SEP);
      sb.append(atcgnPercentageForEachCycle.get(Bases.T).get(i));
      sb.append(FIELD_SEP);
      sb.append(atcgnPercentageForEachCycle.get(Bases.C).get(i));
      sb.append(FIELD_SEP);
      sb.append(atcgnPercentageForEachCycle.get(Bases.G).get(i));
      sb.append(FIELD_SEP);
      sb.append(atcgnPercentageForEachCycle.get(Bases.N).get(i));
      sb.append(FIELD_SEP);
    }
    return sb.toString();
  }

  private static LinkedHashMap<Bases, TFloatArrayList> transformATCGNPercentageForEachCycle(String atcgnPercentPerCycle) {
    LinkedHashMap<Bases, TFloatArrayList> retVal = new LinkedHashMap<Bases, TFloatArrayList>();
    
    for(Bases base : Bases.values()) {
      retVal.put(base, new TFloatArrayList());
    }
    
    String basePercents[] = Gpr.split(atcgnPercentPerCycle, FIELD_SEP);
    for(int idx=0; idx < basePercents.length; idx++) {
      retVal.get(Bases.A).add(Float.parseFloat(basePercents[idx]));
      idx++;
      retVal.get(Bases.T).add(Float.parseFloat(basePercents[idx]));
      idx++;
      retVal.get(Bases.C).add(Float.parseFloat(basePercents[idx]));
      idx++;
      retVal.get(Bases.G).add(Float.parseFloat(basePercents[idx]));
      idx++;
      retVal.get(Bases.N).add(Float.parseFloat(basePercents[idx]));
    }
    return retVal;
  }

  public static QCXml importJob(File jobFile) throws IOException {
    BufferedInputStream input = new BufferedInputStream(new FileInputStream(jobFile));

    QCXml retVal = null;
    long nbReads = -1l;
    long nbBases = -1l;
    float avgQual = -1.0f;
    float duplicateRate = -1.0f;
    Integer medianRawReadLength = -1;
    Float avgRawReadLength = -1.0f;
    Float stdRawReadLength = -1.0f;
    Integer medianTrimReadLength = -1;
    Float avgTrimReadLength = -1.0f;
    Float stdTrimReadLength = -1.0f;
    Integer medianFinalReadLength = -1;
    Float avgFinalReadLength = -1.0f;
    Float stdFinalReadLength = -1.0f;
    LinkedHashMap<Bases, TFloatArrayList> atcgnPercentageForEachCycle = null;

    try {
      XMLInputFactory xif = XMLInputFactory.newInstance();
      XMLStreamReader xmlReader = xif.createXMLStreamReader(input, "UTF-8");
      while (xmlReader.hasNext()) {
        switch (xmlReader.next()) {
        case XMLStreamReader.START_ELEMENT:
          if (xmlReader.getName().getLocalPart().equals(QC_SUMMARY_ELEMENT)) {
            nbReads = Long.parseLong(xmlReader.getAttributeValue(null, NB_READS_ATTRIBUTE));
            nbBases = Long.parseLong(xmlReader.getAttributeValue(null, NB_BASES_ATTRIBUTE));
            avgQual = Float.parseFloat(xmlReader.getAttributeValue(null, AVERAGE_QUALITY_ATTRIBUTE));
            duplicateRate = Float.parseFloat(xmlReader.getAttributeValue(null, DUPLICATE_RATE_ATTRIBUTE));
            if (xmlReader.getAttributeValue(null, MEDIAN_RAW_LENGTH_ATTRIBUTE) != null) {
              medianRawReadLength = Integer.parseInt(xmlReader.getAttributeValue(null, MEDIAN_RAW_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, AVG_RAW_LENGTH_ATTRIBUTE) != null) {
              avgRawReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, AVG_RAW_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, STDD_RAW_LENGTH_ATTRIBUTE) != null) {
              stdRawReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, STDD_RAW_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, MEDIAN_TRIM_LENGTH_ATTRIBUTE) != null) {
              medianTrimReadLength = Integer.parseInt(xmlReader.getAttributeValue(null, MEDIAN_TRIM_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, AVG_TRIM_LENGTH_ATTRIBUTE) != null) {
              avgTrimReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, AVG_TRIM_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, STDD_TRIM_LENGTH_ATTRIBUTE) != null) {
              stdTrimReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, STDD_TRIM_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, MEDIAN_FINAL_LENGTH_ATTRIBUTE) != null) {
              medianFinalReadLength = Integer.parseInt(xmlReader.getAttributeValue(null, MEDIAN_FINAL_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, AVG_FINAL_LENGTH_ATTRIBUTE) != null) {
              avgFinalReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, AVG_FINAL_LENGTH_ATTRIBUTE));
            }

            if (xmlReader.getAttributeValue(null, STDD_FINAL_LENGTH_ATTRIBUTE) != null) {
              stdFinalReadLength = Float.parseFloat(xmlReader.getAttributeValue(null, STDD_FINAL_LENGTH_ATTRIBUTE));
            }

            atcgnPercentageForEachCycle = transformATCGNPercentageForEachCycle(xmlReader.getAttributeValue(null, ATCGN_PERCENTAGE_FOR_EACH_CYCLE_ATTRIBUTE));
          }
          break;

        default:
          break;
        }
      }
      xmlReader.close();

      retVal = new QCXml();
      retVal.setNbReads(nbReads);
      retVal.setNbBases(nbBases);
      retVal.setAvgQual(avgQual);
      retVal.setDuplicateRate(duplicateRate);
      retVal.setMedianRawReadLength(medianRawReadLength);
      retVal.setAvgRawReadLength(avgRawReadLength);
      retVal.setStdRawReadLength(stdRawReadLength);
      retVal.setMedianTrimReadLength(medianTrimReadLength);
      retVal.setAvgTrimReadLength(avgTrimReadLength);
      retVal.setStdTrimReadLength(stdTrimReadLength);
      retVal.setMedianFinalReadLength(medianFinalReadLength);
      retVal.setAvgFinalReadLength(avgFinalReadLength);
      retVal.setStdFinalReadLength(stdFinalReadLength);

      retVal.setAtcgnPercentageForEachCycle(atcgnPercentageForEachCycle);
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    } finally {
      input.close();
    }

    return retVal;
  }

  public float getAvgQual() {
    return avgQual;
  }

  public void setAvgQual(float avgQual) {
    this.avgQual = avgQual;
  }

  public float getDuplicateRate() {
    return duplicateRate;
  }

  public void setDuplicateRate(float duplicateRate) {
    this.duplicateRate = duplicateRate;
  }

  public float getOpticalDuplicateRate() {
    return opticalDuplicateRate;
  }

  public void setOpticalDuplicateRate(float opticalDuplicateRate) {
    this.opticalDuplicateRate = opticalDuplicateRate;
  }

  public long getNbBases() {
    return nbBases;
  }

  public void setNbBases(long nbBases) {
    this.nbBases = nbBases;
  }

  public long getNbReads() {
    return nbReads;
  }

  public void setNbReads(long nbReads) {
    this.nbReads = nbReads;
  }

  public LinkedHashMap<Bases, TFloatArrayList> getAtcgnPercentageForEachCycle() {
    return atcgnPercentageForEachCycle;
  }

  public void setAtcgnPercentageForEachCycle(LinkedHashMap<Bases, TFloatArrayList> atcgnPercentageForEachCycle) {
    this.atcgnPercentageForEachCycle = atcgnPercentageForEachCycle;
  }

  public Integer getMedianRawReadLength() {
    return medianRawReadLength;
  }

  public void setMedianRawReadLength(Integer medianRawReadLength) {
    this.medianRawReadLength = medianRawReadLength;
  }

  public Float getAvgRawReadLength() {
    return avgRawReadLength;
  }

  public void setAvgRawReadLength(Float avgRawReadLength) {
    this.avgRawReadLength = avgRawReadLength;
  }

  public Integer getMedianTrimReadLength() {
    return medianTrimReadLength;
  }

  public void setMedianTrimReadLength(Integer medianTrimReadLength) {
    this.medianTrimReadLength = medianTrimReadLength;
  }

  public Float getStdRawReadLength() {
    return stdRawReadLength;
  }

  public void setStdRawReadLength(Float stdRawReadLength) {
    this.stdRawReadLength = stdRawReadLength;
  }

  public Float getAvgTrimReadLength() {
    return avgTrimReadLength;
  }

  public void setAvgTrimReadLength(Float avgTrimReadLength) {
    this.avgTrimReadLength = avgTrimReadLength;
  }

  public Float getStdTrimReadLength() {
    return stdTrimReadLength;
  }

  public void setStdTrimReadLength(Float stdTrimReadLength) {
    this.stdTrimReadLength = stdTrimReadLength;
  }

  public Integer getMedianFinalReadLength() {
    return medianFinalReadLength;
  }

  public void setMedianFinalReadLength(Integer medianFinalReadLength) {
    this.medianFinalReadLength = medianFinalReadLength;
  }

  public Float getAvgFinalReadLength() {
    return avgFinalReadLength;
  }

  public void setAvgFinalReadLength(Float avgFinalReadLength) {
    this.avgFinalReadLength = avgFinalReadLength;
  }

  public Float getStdFinalReadLength() {
    return stdFinalReadLength;
  }

  public void setStdFinalReadLength(Float stdFinalReadLength) {
    this.stdFinalReadLength = stdFinalReadLength;
  }

}
