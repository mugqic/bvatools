/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.list.array.TLongArrayList;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

public class SequenceLengthDistributionPlugin extends DefaultQCPlugin {
  private final List<TLongArrayList> readLengthBins = new ArrayList<TLongArrayList>();

  @Override
  public void processSequence(Fragment fragment) {
    int longestRead = 0;
    for(Read read : fragment) {
      if(read != null) {
        if(read.length() > longestRead)
          longestRead = read.length();
      }
    }

    while(readLengthBins.size() < fragment.size()) {
      readLengthBins.add(new TLongArrayList());
    }
    
    for(TLongArrayList lengthBins : readLengthBins) {
      for (int i = lengthBins.size(); i <= longestRead; i++) {
        lengthBins.add(0l);
      }
    }
    for(int idx=0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if(read == null)
        continue;

      TLongArrayList lengthBins = readLengthBins.get(idx);
      lengthBins.set(read.length()-1, lengthBins.get(read.length()-1) + 1);
    }
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    TLongArrayList lengthBins = new TLongArrayList();
    for(TLongArrayList readBins : readLengthBins) {
      lengthBins.addAll(readBins);
    }
    
    XYSeries lengthSerie = new XYSeries("Sequence Length");

    for (int i = 0; i < lengthBins.size(); i++) {
      lengthSerie.add(i, (double) lengthBins.get(i));
    }

    XYSeriesCollection lengthCollection = new XYSeriesCollection();
    lengthCollection.addSeries(lengthSerie);

    NumberAxis xAxis = new UnlimitedTicksNumberAxis("Cycles");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setFixedAutoRange(lengthBins.size());
    xAxis.setLowerBound(0);
    xAxis.setUpperBound(lengthBins.size());

    NumberAxis yAxis = new UnlimitedTicksNumberAxis("Nb Sequences");
    yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

    XYLineAndShapeRenderer lengthRenderer = new XYLineAndShapeRenderer(true, false);
    XYPlot lengthPlot = new XYPlot(lengthCollection, xAxis, yAxis, lengthRenderer);
    JFreeChart chart = new JFreeChart("Sequence Length Distribution", JFreeChart.DEFAULT_TITLE_FONT, lengthPlot, true);
    chart.setBackgroundPaint(Color.white);
    lengthPlot.setBackgroundPaint(Color.white);
    File outputFile = new File(outputDirectory, generateGraphFileName("06sequenceLengthDistribution", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, 60 + 15 * lengthBins.size(), 600);
  }
}
