/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TObjectIntProcedure;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import htsjdk.samtools.util.StringUtil;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.obiba.bitwise.BitVector;

import ca.mcgill.genome.bvatools.filterdups.DuplicateKmerBuilder;
import ca.mcgill.genome.bvatools.filterdups.SequenceLengthException;
import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;

public class DuplicateKMerPercentPlugin extends DefaultQCPlugin {
  /**
   * Small interface that provides access to the physical location information about a cluster.
   * All values should be defaulted to -1 if unavailable.  ReadGroup and Tile should only allow
   * non-zero positive integers, x and y coordinates may be negative.
   */
  public static interface PhysicalLocation {
      short getReadGroup();
      void  setReadGroup(short rg);
      short  getTile();
      void  setTile(short tile);
      short getX();
      void  setX(short x);
      short getY();
      void  setY(short y);
  }
  
  private static final String DEFAULT_READ_NAME_REGEX = "[a-zA-Z0-9]+:[0-9]:([0-9]+):([0-9]+):([0-9]+).*".intern();
  private static final int DEFAULT_HEIGHT = 200;
  private static final int ESTIMATED_MAX_ITEMS = 45000000; // 1 lane of GA
  private static final int MAX_DISPLAYED_DUPLICATES = 10;
  private transient TObjectIntHashMap<BitVector> nbKnownSequencesFound;
  private transient HashMap<BitVector, List<PhysicalLocation>> duplicateClusterLocations = null;
  private int kmerSize = 20;
  private int offset = 15;
  private long totalNbReads = 0;
  private int totalNbDuplicatedReads = 0;
  private double percentDup = -1;
  private boolean containsMultiReadFragment = false;
  private String readNameRegEx = DEFAULT_READ_NAME_REGEX;
  private Pattern readNamePattern = null;
  private int opticalDuplicateDistance = 100;
  private final boolean testOpticalDuplicates;
  private double percentOpticalDup = -1;

  public DuplicateKMerPercentPlugin() {
    this(false);
  }

  public DuplicateKMerPercentPlugin(boolean testOpticalDuplicates) {
    this.testOpticalDuplicates = testOpticalDuplicates;
  }

  public boolean longEnoughReads(Fragment fragment) {
    int kmerBasesPerRead = kmerSize / fragment.size();
    // All reads need to be > than kmer + offset
    for (int idx = 0; idx < fragment.size(); idx++) {
      Read read = fragment.getRead(idx);
      if (read != null) {
        if (read.getBaseQualities().length < (kmerBasesPerRead + offset)) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean isTestOpticalDuplicates() {
    return testOpticalDuplicates;
  }

  @Override
  public void processSequence(Fragment fragment) {
    if (!longEnoughReads(fragment)) {
      return;
    }
    if (fragment.size() > 1)
      containsMultiReadFragment = true;

    int estimatedItems = (int) (ESTIMATED_MAX_ITEMS / 0.75 + 1.0);
    if (nbKnownSequencesFound == null)
      nbKnownSequencesFound = new TObjectIntHashMap<BitVector>(estimatedItems, 0.75f);
    if (duplicateClusterLocations == null && testOpticalDuplicates)
      duplicateClusterLocations = new HashMap<BitVector, List<PhysicalLocation>>(estimatedItems, 0.75f);

    totalNbReads++;
    BitVector twoBitSequence;
    try {
      twoBitSequence = DuplicateKmerBuilder.computeKmer(offset, kmerSize, fragment);
    } catch (SequenceLengthException e) {
      throw new RuntimeException("Kmer too long, or offset to big: " + fragment.getName());
    }

    // Keep this for debugging purposes
    // if(pair1.toString().equals(pair2.toString())) {
    // System.err.println("They are equal: " + sequence.getHeader());
    // }
    if (!nbKnownSequencesFound.containsKey(twoBitSequence)) {
      nbKnownSequencesFound.put(twoBitSequence, 1);
      if (duplicateClusterLocations != null) {
        PhysicalLocation loc = new ClusterLocation();
        addLocationInformation(fragment.getName(), loc);
        List<PhysicalLocation> newList = new ArrayList<PhysicalLocation>(1);
        newList.add(loc);
        duplicateClusterLocations.put(twoBitSequence, newList);
      }
    } else {
      int value = nbKnownSequencesFound.get(twoBitSequence);
      value++;
      if (value == 2) {
        totalNbDuplicatedReads++;
      }
      nbKnownSequencesFound.put(twoBitSequence, value);

      if (duplicateClusterLocations != null) {
        PhysicalLocation loc = new ClusterLocation();
        addLocationInformation(fragment.getName(), loc);
        duplicateClusterLocations.get(twoBitSequence).add(loc);
      }
    }
  }

  public double getPercentDuplication() {
    computeDuplication();
    return percentDup;
  }

  public double getPercentOpticalDuplication() {
    computeDuplication();
    return percentOpticalDup;
  }

  public void computeDuplication() {
    if (percentDup > -1)
      return;
    if (nbKnownSequencesFound == null) {
      percentDup = 0;
    } else {
      double nbKmers = nbKnownSequencesFound.size();
      double total = totalNbReads;
      double nbDuplicateReads = total - nbKmers;
      percentDup = nbDuplicateReads * 100.0 / total;

      double nbOpticalDuplicates = countOpticalDuplicates();
      if (nbOpticalDuplicates != -1) {
        percentOpticalDup = nbOpticalDuplicates * 100.0 / nbDuplicateReads;
      }
    }
  }

  private long countOpticalDuplicates() {
    if (duplicateClusterLocations == null)
      return -1;

    long nbOpticalDups = 0;
    for (List<PhysicalLocation> list : duplicateClusterLocations.values()) {
      Collections.sort(list, new Comparator<PhysicalLocation>() {
        public int compare(final PhysicalLocation lhs, final PhysicalLocation rhs) {
          int retval = lhs.getReadGroup() - rhs.getReadGroup();
          if (retval == 0)
            retval = lhs.getTile() - rhs.getTile();
          if (retval == 0)
            retval = lhs.getX() - rhs.getX();
          if (retval == 0)
            retval = lhs.getY() - rhs.getY();
          return retval;
        }
      });

      int length = list.size();
      long countPerList = 0;
      boolean optDups[] = new boolean[length];
      for (int i = 0; i < length; ++i) {
        PhysicalLocation lhs = list.get(i);
        if (lhs.getTile() < 0 || optDups[i] == true)
          continue;

        for (int j = i + 1; j < length; ++j) {
          PhysicalLocation rhs = list.get(j);

          if (lhs.getReadGroup() != rhs.getReadGroup())
            break;
          if (lhs.getTile() != rhs.getTile())
            break;
          if (rhs.getX() > lhs.getX() + opticalDuplicateDistance)
            break;

          // Not true distance, but close enough and faster to compute
          if (Math.abs(lhs.getY() - rhs.getY()) <= opticalDuplicateDistance) {
            countPerList++;
            optDups[i] = true;
            if (optDups[j] == false) {
              countPerList++;
              optDups[j] = true;
            }
            break;
          }
        }
      }

      // All can't be opt dup
      if (countPerList == length)
        countPerList--;
      nbOpticalDups += countPerList;
    }
    return nbOpticalDups;
  }

  public int getKmerSize() {
    return kmerSize;
  }

  public void setKmerSize(int kmerSize) {
    this.kmerSize = kmerSize;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    File outputFile = new File(outputDirectory, generateGraphFileName("08abundantDuplicates", ".png"));
    computeDuplication();

    if (nbKnownSequencesFound == null) {
      final int height = 100;
      final String text = "No usable reads found for Dup. computation";
      BufferedImage image = new BufferedImage(DEFAULT_WIDTH, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = image.createGraphics();
      Font font = new Font(Font.DIALOG, Font.BOLD, 26);
      g.setFont(font);
      g.setPaint(Color.RED);

      FontMetrics fm = g.getFontMetrics(font);
      java.awt.geom.Rectangle2D rect = fm.getStringBounds(text, g);

      int textHeight = (int) (rect.getHeight());
      int textWidth = (int) (rect.getWidth());

      // Center text horizontally and vertically
      int x = (DEFAULT_WIDTH - textWidth) / 2;
      int y = (height - textHeight) / 2 + fm.getAscent();
      g.drawString(text, x, y); // Draw the string.
      g.dispose();
      ImageIO.write(image, "png", outputFile);
    } else {
      FilterDuplicates filterDuplicates = new FilterDuplicates();
      nbKnownSequencesFound.forEachEntry(filterDuplicates);
      // free memory quickly
      nbKnownSequencesFound = null;

      DuplicateRead[] duplicateReads = filterDuplicates.getDuplicateReads();
      // free memory quickly
      filterDuplicates = null;
      if (duplicateReads.length > 0) {
        Arrays.sort(duplicateReads, Collections.reverseOrder());

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String seriesKey = "Most abundant duplicated reads";
        for (int idx = 0; idx < duplicateReads.length && idx < MAX_DISPLAYED_DUPLICATES; idx++) {
          dataset.addValue(duplicateReads[idx].getCount(), seriesKey, duplicateReads[idx].getSequenceString());
        }
        // free memory quickly
        duplicateReads = null;

        // create the chart...
        DecimalFormat formatter = new DecimalFormat("#0.00");
        String title = "";
        if (containsMultiReadFragment) {
          title = "Multi-Read ";
        }
        title += "Abundant Duplicates (" + formatter.format(getPercentDuplication()) + "%)";

        JFreeChart chart = ChartFactory.createBarChart( // chart
            title, // title
            "Sequences", // domain axis label
            "Number of Reads", // range axis label
            dataset, // data
            PlotOrientation.HORIZONTAL, // orientation
            true, // include legend
            true, false);
        chart.setBackgroundPaint(Color.white);
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.white);
        plot.setRangeGridlinePaint(Color.lightGray);
        plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBarPainter(new StandardBarPainter());
        CategoryAxis categoryAxis = plot.getDomainAxis();
        categoryAxis.setCategoryMargin(0.05);
        categoryAxis.setUpperMargin(0.05);
        categoryAxis.setLowerMargin(0.05);
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setAutoRangeIncludesZero(true);
        rangeAxis.setUpperMargin(0.10);

        int additionalHeight = 30 * dataset.getColumnCount() - DEFAULT_HEIGHT;
        if (additionalHeight < 0)
          additionalHeight = 0;
        ChartUtilities.saveChartAsPNG(outputFile, chart, DEFAULT_WIDTH, DEFAULT_HEIGHT + additionalHeight);
      }
    }
  }

  private class FilterDuplicates implements TObjectIntProcedure<BitVector> {
    private final DuplicateRead duplicateReads[] = new DuplicateRead[totalNbDuplicatedReads];
    private int idx = 0;

    @Override
    public boolean execute(BitVector a, int b) {
      if (b > 1) {
        duplicateReads[idx] = new DuplicateRead(a, b);
        idx++;
      }

      return true;
    }

    public DuplicateRead[] getDuplicateReads() {
      return duplicateReads;
    }

  }

  private static class DuplicateRead implements Comparable<DuplicateRead> {
    private final BitVector sequence;
    private final int count;

    public DuplicateRead(BitVector sequence, int count) {
      this.sequence = sequence;
      this.count = count;
    }

    public int getCount() {
      return count;
    }

    public String getSequenceString() {
      char nucleotides[] = new char[sequence.size() / 3];

      for (int idx = 0; idx < sequence.size(); idx += 3) {
        byte value = 0;
        if (sequence.get(idx)) {
          value += 4;
        }
        if (sequence.get(idx + 1)) {
          value += 2;
        }
        if (sequence.get(idx + 2)) {
          value += 1;
        }

        switch (value) {
        case 0:
          // 000
          nucleotides[idx / 3] = 'A';
          break;
        case 1:
          // 001
          nucleotides[idx / 3] = 'C';
          break;
        case 2:
          // 010
          nucleotides[idx / 3] = 'G';
          break;
        case 3:
          // 011
          nucleotides[idx / 3] = 'T';
          break;
        case 4:
          // 100
          nucleotides[idx / 3] = 'N';
          break;
        default:
          throw new RuntimeException("Unknown value: " + value);
        }
      }
      return String.valueOf(nucleotides);
    }

    @Override
    public int compareTo(DuplicateRead o) {
      return count - o.count;
    }

    @Override
    public int hashCode() {
      return sequence.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      DuplicateRead other = (DuplicateRead) obj;
      return sequence.equals(other.sequence);
    }
  }

  private final String[] tmpLocationFields = new String[10];

  private boolean addLocationInformation(final String readName, final PhysicalLocation loc) {
    // Optimized version if using the default read name regex (== used on purpose):
    if (readNameRegEx == DEFAULT_READ_NAME_REGEX) {
      final int fields = StringUtil.split(readName, tmpLocationFields, ':');
      if (fields < 5) {
        throw new RuntimeException(String.format("Default READ_NAME_REGEX '%s' did not match read name '%s'.  "
            + "You may need to specify a READ_NAME_REGEX in order to correctly identify optical duplicates.  "
            + "Note that this message will not be emitted again even if other read names do not match the regex.", readNameRegEx, readName));
      }

      loc.setTile((short) rapidParseInt(tmpLocationFields[2]));
      loc.setX((short) rapidParseInt(tmpLocationFields[3]));
      loc.setY((short) rapidParseInt(tmpLocationFields[4]));
      return true;
    } else if (readNameRegEx == null) {
      return false;
    } else {
      // Standard version that will use the regex
      if (readNamePattern == null)
        readNamePattern = Pattern.compile(readNameRegEx);

      final Matcher m = readNamePattern.matcher(readName);
      if (m.matches()) {
        loc.setTile((short) Integer.parseInt(m.group(1)));
        loc.setX((short) Integer.parseInt(m.group(2)));
        loc.setY((short) Integer.parseInt(m.group(3)));
        return true;
      } else {
        throw new RuntimeException(String.format("READ_NAME_REGEX '%s' did not match read name '%s'.  Your regex may not be correct.  "
            + "Note that this message will not be emitted again even if other read names do not match the regex.", readNameRegEx, readName));
      }
    }
  }

  /**
   * Very specialized method to rapidly parse a sequence of digits from a String up until the first non-digit character.
   * Does not handle negative numbers.
   */
  private final int rapidParseInt(final String input) {
    final int len = input.length();
    int val = 0;

    for (int i = 0; i < len; ++i) {
      final char ch = input.charAt(i);
      if (Character.isDigit(ch)) {
        val = (val * 10) + (ch - 48);
      }
    }

    return val;
  }

  private static class ClusterLocation implements PhysicalLocation {
    private short readGroup = -1;
    private short tile = -1;
    private short x = -1, y = -1;

    @Override
    public short getReadGroup() {
      return this.readGroup;
    }

    @Override
    public void setReadGroup(final short readGroup) {
      this.readGroup = readGroup;
    }

    @Override
    public short getTile() {
      return this.tile;
    }

    @Override
    public void setTile(final short tile) {
      this.tile = tile;
    }

    @Override
    public short getX() {
      return this.x;
    }

    @Override
    public void setX(final short x) {
      this.x = x;
    }

    @Override
    public short getY() {
      return this.y;
    }

    @Override
    public void setY(final short y) {
      this.y = y;
    }
  }
}
