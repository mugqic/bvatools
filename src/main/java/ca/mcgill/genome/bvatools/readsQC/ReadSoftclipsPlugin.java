/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.readsQC;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import htsjdk.samtools.CigarElement;
import htsjdk.samtools.CigarOperator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ca.mcgill.genome.bvatools.parsers.BAMRead;
import ca.mcgill.genome.bvatools.parsers.Fragment;
import ca.mcgill.genome.bvatools.parsers.Read;
import ca.mcgill.genome.bvatools.util.GrowingLongArrayList;

public class ReadSoftclipsPlugin extends DefaultQCPlugin {
  private int nbReads = 0;
  private final GrowingLongArrayList startSoftlipLength = new GrowingLongArrayList();
  private final GrowingLongArrayList endSoftlipLength = new GrowingLongArrayList();

  @Override
  public void processSequence(Fragment fragment) {
    for (Read read : fragment) {
      BAMRead bamRead = (BAMRead)read;;

      // Unmapped
      if(bamRead.getCigar() == null) {
        continue;
      }

      nbReads++;
      CigarElement first = bamRead.getCigar().getCigarElement(0);
      CigarElement last = bamRead.getCigar().getCigarElement(bamRead.getCigar().numCigarElements()-1);
      
      if(first.getOperator() == CigarOperator.S) {
        startSoftlipLength.set(first.getLength(), startSoftlipLength.get(first.getLength())+1);
      }
      else {
        startSoftlipLength.set(0, startSoftlipLength.get(0)+1);
      }

      if(last.getOperator() == CigarOperator.S) {
        endSoftlipLength.set(last.getLength(), endSoftlipLength.get(last.getLength())+1);
      }
      else {
        endSoftlipLength.set(0, endSoftlipLength.get(0)+1);
      }
    }
  }

  @Override
  public void generateGraph(File outputDirectory) throws IOException {
    int longestSClip = startSoftlipLength.size();
    if(longestSClip < endSoftlipLength.size())
      longestSClip = endSoftlipLength.size();
    
    final double filterLevel = 0.005;
    // End graph at the first sclipLength that contains more than 0.5% of the reads
    for(; longestSClip > -1; longestSClip--) {
      if((double)startSoftlipLength.get(longestSClip) / (double)nbReads > filterLevel) {
        break;
      }
      if((double)endSoftlipLength.get(longestSClip) / (double)nbReads > filterLevel) {
        break;
      }
    }

    XYSeries startSeries = new XYSeries("Beginning");
    XYSeries endSeries = new XYSeries("Ending");
    for(int i=0; i < longestSClip;i++) {
      startSeries.add(i, (double)startSoftlipLength.get(i)*100.0/(double)nbReads);
      endSeries.add(i, (double)endSoftlipLength.get(i)*100.0/(double)nbReads);
    }
    XYSeriesCollection collection = new XYSeriesCollection();
    collection.addSeries(startSeries);
    collection.addSeries(endSeries);
    
    JFreeChart chart = ChartFactory.createXYBarChart("Read Softclips\n(Longest softclip filtered to contain > "+filterLevel*100+"% of the reads)", "SoftClip Size", false, "Percent", collection, PlotOrientation.VERTICAL, true, false, false);
    chart.setBackgroundPaint(Color.white);
    
    ClusteredXYBarRenderer renderer = new ClusteredXYBarRenderer(0,false);

    XYPlot plot = chart.getXYPlot();
    plot.setBackgroundPaint(Color.white);
    plot.setRenderer(renderer);

    NumberAxis xAxis = new UnlimitedTicksNumberAxis("Bases");
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setRange(-0.9,longestSClip+1);
    plot.setDomainAxis(xAxis);
//    plot.getDomainAxis().setAutoRange(false);

    plot.setRangeGridlinesVisible(true);
    plot.setRangeGridlinePaint(Color.black);
    

    int width = 60 + 35*longestSClip;
    if (width < DEFAULT_WIDTH) {
      width = DEFAULT_WIDTH;
    }

    File outputFile = new File(outputDirectory, generateGraphFileName("09readSoftclipsChart", ".png"));
    ChartUtilities.saveChartAsPNG(outputFile, chart, width, 600);
  }

}
