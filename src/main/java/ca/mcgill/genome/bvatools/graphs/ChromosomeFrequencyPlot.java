/**
 * This file is part of BVATools.
 *
 * BVATools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BVATools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with BVATools.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.mcgill.genome.bvatools.graphs;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.ui.RectangleAnchor;

import ca.mcgill.mcb.pcingola.interval.Chromosome;
import ca.mcgill.mcb.pcingola.interval.Genome;

public class ChromosomeFrequencyPlot {
  private final Genome genome;
  private final int width;
  private final int height;
  private final PointDataset allDataset = new PointDataset();
  private final Map<String, PointDataset> chr2Dataset = new HashMap<String, PointDataset>();
  private final TObjectLongMap<String> chrOffset = new TObjectLongHashMap<String>();

  public ChromosomeFrequencyPlot(Genome genome, int width, int height) {
    this.genome = genome;
    this.width = width;
    this.height = height;

    init();
  }

  public void init() {
    long offset = 0;
    // The order is important
    for (String chrName : genome.getChromosomeNames()) {
      Chromosome chr = genome.getChromosome(chrName);
      chrOffset.put(chr.getChromosomeName(), offset);
      offset += chr.getEnd();

      chr2Dataset.put(chr.getChromosomeName(), new PointDataset());
    }
  }

  public void addPoint(String chr, int position, double value) {
    if (chr2Dataset.get(chr) == null)
      return;

    chr2Dataset.get(chr).add(position, value);
    allDataset.add(position + chrOffset.get(chr), value);
  }

  public void write(String outputPrefix, boolean drawPerChromosome) throws IOException {
    chart2Image(new File(outputPrefix + ".png"), allDataset, true);

    if (drawPerChromosome) {
      for (String chr : chr2Dataset.keySet()) {
        if (chr2Dataset.get(chr).getItemCount(0) == 0)
          continue;

        chart2Image(new File(outputPrefix + '.' + chr + ".png"), allDataset, false);
      }
    }
  }

  private void chart2Image(File output, PointDataset dataset, boolean drawChromosomeSeperators) throws IOException {

    JFreeChart chart = ChartFactory.createScatterPlot("Ratios", "bp", "ratio", dataset, PlotOrientation.VERTICAL, false, false, false);
    XYPlot plot = chart.getXYPlot();
    plot.setRenderer(new XYDotRenderer());
    NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
    domainAxis.setAutoRangeIncludesZero(true);
    // plot.getRangeAxis().setRange(-2, 2);
    plot.getRangeAxis().setAutoRange(true);

    if (drawChromosomeSeperators) {
      for (String chr : chrOffset.keySet()) {
        IntervalMarker marker = new IntervalMarker(chrOffset.get(chr), chrOffset.get(chr) + genome.getChromosome(chr).size(), new Color(255, 255, 255, 0),
            new BasicStroke(2.0f), Color.BLACK, new BasicStroke(0), 1f);
        marker.setLabel(chr);
        marker.setLabelAnchor(RectangleAnchor.CENTER);
        marker.setLabelPaint(Color.BLACK);
        plot.addDomainMarker(marker);
      }
    }

    ChartUtilities.saveChartAsPNG(output, chart, width, height);
  }
}
